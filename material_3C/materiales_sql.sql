-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-11-2023 a las 23:48:11
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `materiales.sql`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen`
--

CREATE TABLE `almacen` (
  `codigo` int(11) NOT NULL,
  `nomCalle` varchar(50) NOT NULL,
  `num_calle` varchar(30) NOT NULL,
  `colonia` varchar(30) NOT NULL,
  `codigo_postal` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `almacen`
--

INSERT INTO `almacen` (`codigo`, `nomCalle`, `num_calle`, `colonia`, `codigo_postal`) VALUES
(1, 'Almacén A', '789 Industrial Blvd', 'Área Industrial', '54321'),
(2, 'Centro de Almacenamiento', '456 Storage Ave', 'Distrito de Almacenamiento', '98765'),
(3, 'Almacén Principal', '101 Storage Lane', 'Storageville', '23456'),
(4, 'Almacén Central', '202 Warehouse St', 'Distrito de Almacenes', '67890'),
(5, 'Depósito de Mercancías', '303 Goods Blvd', 'Área de Mercancías', '12345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambios`
--

CREATE TABLE `cambios` (
  `codigo` int(11) NOT NULL,
  `FECHA` date DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `pedido` int(11) NOT NULL,
  `almacen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cambios`
--

INSERT INTO `cambios` (`codigo`, `FECHA`, `descripcion`, `pedido`, `almacen`) VALUES
(1, '2023-11-21', 'Cambio de productos', 1, 2),
(2, '2023-11-21', 'Cambio de productos', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `camb_material`
--

CREATE TABLE `camb_material` (
  `cambio` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `cantidad_devuelta` int(11) DEFAULT NULL,
  `cantidad_cambiada` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `camb_material`
--

INSERT INTO `camb_material` (`cambio`, `material`, `cantidad_devuelta`, `cantidad_cambiada`) VALUES
(2, 1, 5, 0),
(1, 1, 10, 0),
(1, 1, 15, 15),
(1, 1, 10, 0),
(1, 1, 0, 15),
(1, 1, 11, 0),
(1, 1, 0, 11);

--
-- Disparadores `camb_material`
--
DELIMITER $$
CREATE TRIGGER `calcular_stock_despues_de_cambio` AFTER INSERT ON `camb_material` FOR EACH ROW BEGIN
    DECLARE material_stock INT;
    DECLARE importe_pedido DECIMAL(10, 2);
     DECLARE importe_devuelto DECIMAL(10, 2);
    DECLARE importe_cambiado DECIMAL(10, 2);
    
    SELECT stock INTO material_stock
    FROM mat_alma
    WHERE almacen = (SELECT almacen FROM pedido WHERE codigo = NEW.cambio)
      AND material = NEW.material;

    
    SELECT subtotal INTO importe_pedido
    FROM pedido
    WHERE codigo = NEW.cambio;

    
   
    SELECT precioCompra * NEW.cantidad_devuelta INTO importe_devuelto
    FROM Material
    WHERE Codigo = NEW.material;

    
    
    SELECT precioCompra * NEW.cantidad_cambiada INTO importe_cambiado
    FROM Material
    WHERE Codigo = NEW.material;

    
    IF (importe_devuelto + importe_cambiado) >= importe_pedido THEN
        
        UPDATE mat_alma
        SET stock = material_stock + NEW.cantidad_devuelta - NEW.cantidad_cambiada
        WHERE almacen = (SELECT almacen FROM pedido WHERE codigo = NEW.cambio)
          AND material = NEW.material;
    ELSE
        
        
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'El importe devuelto y cambiado no es suficiente para realizar el cambio.';
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `codigo` int(11) NOT NULL,
  `Nombre` varchar(15) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `correo` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`codigo`, `Nombre`, `telefono`, `correo`) VALUES
(1, 'Electronics Cor', '123-456-7890', 'info@abc-electronics.com'),
(2, 'Furniture Ltd.', '987-654-3210', 'sales@xyz-furniture.com'),
(3, 'Tech Innovators', '555-123-4567', 'info@tech-innovators.com'),
(4, 'Global Books LL', '777-888-9999', 'orders@global-books.com'),
(5, 'Tool Solutions ', '444-555-6666', 'info@tool-solutions.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `Codigo` int(11) NOT NULL,
  `Descripcion` varchar(50) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `prodTotal` int(11) DEFAULT NULL,
  `Subtotal` decimal(10,2) DEFAULT NULL,
  `Iva` decimal(5,2) DEFAULT NULL,
  `Total` decimal(10,2) DEFAULT NULL,
  `Proveedor` int(11) NOT NULL,
  `almacen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`Codigo`, `Descripcion`, `Fecha`, `prodTotal`, `Subtotal`, `Iva`, `Total`, `Proveedor`, `almacen`) VALUES
(1, 'Compra de electrónicos', '2023-11-20', 5, 1500.00, 240.00, 1740.00, 1, 1),
(2, 'Compra de muebles', '2023-11-18', 4, 8000.00, 999.99, 9200.00, 2, 2),
(3, 'Compra de ropa', '2023-11-15', 5, 3000.00, 450.00, 3450.00, 3, 3),
(4, 'Compra de libros', '2023-11-12', 6, 2000.00, 300.00, 2300.00, 4, 4),
(5, 'Compra de herramientas', '2023-11-10', 7, 4000.00, 600.00, 4600.00, 5, 5),
(6, 'Compra de prueba', '2023-11-21', 5, 1500.00, 240.00, 1740.00, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar`
--

CREATE TABLE `lugar` (
  `codigo` int(11) NOT NULL,
  `num_calle` varchar(30) NOT NULL,
  `colonia` varchar(30) NOT NULL,
  `codigo_postal` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `lugar`
--

INSERT INTO `lugar` (`codigo`, `num_calle`, `colonia`, `codigo_postal`) VALUES
(1, '123 Main St', 'Centro', '12345'),
(2, '456 Oak St', 'Suburbio', '67890'),
(3, '789 Pine St', 'Área Rural', '54321'),
(4, '101 Elm St', 'Centro de la Ciudad', '98765'),
(5, '202 Maple St', 'Colina', '23456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE `material` (
  `Codigo` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Descripcion` text DEFAULT NULL,
  `precioCompra` decimal(10,2) DEFAULT NULL,
  `PrecioVenta` decimal(10,2) DEFAULT NULL,
  `Tipo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`Codigo`, `Nombre`, `Descripcion`, `precioCompra`, `PrecioVenta`, `Tipo`) VALUES
(1, 'Smartphone', 'Teléfono inteligente de última generación', 300.00, 500.00, 1),
(2, 'Sofá', 'Sofá cómodo y elegante', 600.00, 900.00, 2),
(3, 'Camisa', 'Camisa de algodón de alta calidad', 20.00, 40.00, 3),
(4, 'Libro', 'Novela best-seller', 10.00, 20.00, 4),
(5, 'Destornillador', 'Destornillador eléctrico recargable', 50.00, 80.00, 5);

--
-- Disparadores `material`
--
DELIMITER $$
CREATE TRIGGER `before_insert_material` BEFORE INSERT ON `material` FOR EACH ROW BEGIN
    
    SET NEW.PrecioVenta = NEW.precioCompra * (1 + 0.10); 
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material_compra`
--

CREATE TABLE `material_compra` (
  `material` int(11) NOT NULL,
  `compra` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `material_compra`
--

INSERT INTO `material_compra` (`material`, `compra`, `cantidad`, `importe`) VALUES
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(4, 4, NULL, NULL),
(5, 5, NULL, NULL),
(1, 2, 2, 600.00),
(1, 6, 5, 1500.00),
(1, 1, 5, 1500.00);

--
-- Disparadores `material_compra`
--
DELIMITER $$
CREATE TRIGGER `actualizar_stock_en_compra_almacen` AFTER INSERT ON `material_compra` FOR EACH ROW BEGIN
    DECLARE material_stock INT;
    DECLARE material_cantidad INT;

    
    SELECT stock INTO material_stock
    FROM mat_alma
    WHERE almacen = (SELECT almacen FROM Compra WHERE Codigo = NEW.compra)
      AND material = NEW.material;

    
    SELECT cantidad INTO material_cantidad
    FROM material_compra
    WHERE compra = NEW.compra AND material = NEW.material;

    
    UPDATE mat_alma
    SET stock = material_stock + material_cantidad
    WHERE almacen = (SELECT almacen FROM Compra WHERE Codigo = NEW.compra)
      AND material = NEW.material;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `calcularTotalesCompra` AFTER INSERT ON `material_compra` FOR EACH ROW BEGIN
    DECLARE cantidad_total INT;
    DECLARE subtotal DECIMAL(10, 2);
    DECLARE iva DECIMAL(5, 2);
    DECLARE total DECIMAL(10, 2);

    
    SELECT SUM(cantidad) INTO cantidad_total
    FROM material_compra
    WHERE compra = NEW.compra;

    
    SELECT SUM(importe) INTO subtotal
    FROM material_compra
    WHERE compra = NEW.compra;

    
    SET iva = subtotal * 0.16;

    
    SET total = subtotal + iva;

    
    UPDATE Compra
    SET prodTotal = cantidad_total,
        Subtotal = subtotal,
        Iva = iva,
        Total = total
    WHERE Codigo = NEW.compra;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `calcular_importe_en_compra` BEFORE INSERT ON `material_compra` FOR EACH ROW BEGIN
    DECLARE precio_compra DECIMAL(10, 2);
    
    
    SELECT precioCompra INTO precio_compra
    FROM Material
    WHERE Codigo = NEW.material;
    
    
    SET NEW.importe = NEW.cantidad * precio_compra;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mat_alma`
--

CREATE TABLE `mat_alma` (
  `almacen` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mat_alma`
--

INSERT INTO `mat_alma` (`almacen`, `material`, `stock`) VALUES
(1, 1, 110),
(2, 2, 55),
(3, 3, 200),
(4, 4, 150),
(5, 5, 80),
(1, 2, 85);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `Codigo` int(11) NOT NULL,
  `num_pago` varchar(20) DEFAULT NULL,
  `montoPago` decimal(10,2) DEFAULT NULL,
  `concepto` varchar(100) DEFAULT NULL,
  `saldo` decimal(10,2) DEFAULT NULL,
  `pedido` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Disparadores `pago`
--
DELIMITER $$
CREATE TRIGGER `calcularPagoSaldo` BEFORE INSERT ON `pago` FOR EACH ROW BEGIN
    DECLARE total_pedido DECIMAL(10, 2);
    DECLARE total_pagado DECIMAL(10, 2);
    DECLARE nuevo_saldo DECIMAL(10, 2);
    DECLARE nuevo_numero_pago INT;

    
    SELECT totalInt INTO total_pedido
    FROM pedido
    WHERE codigo = NEW.pedido;

    
    SELECT COALESCE(SUM(montoPago), 0) INTO total_pagado
    FROM pago
    WHERE pedido = NEW.pedido;

    
    SET nuevo_saldo = total_pedido - (total_pagado + NEW.montoPago);
    SET nuevo_numero_pago = (SELECT COUNT(*) + 1 FROM pago WHERE pedido = NEW.pedido);

    
    SET NEW.saldo = nuevo_saldo;
    SET NEW.num_pago = nuevo_numero_pago;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cantProduct` int(11) DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `iva` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `totalInt` decimal(10,2) DEFAULT NULL,
  `almacen` int(11) DEFAULT NULL,
  `lugar` int(11) DEFAULT NULL,
  `cliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`codigo`, `descripcion`, `fecha`, `cantProduct`, `subtotal`, `iva`, `total`, `totalInt`, `almacen`, `lugar`, `cliente`) VALUES
(1, 'Pedido de repuestos', '2023-11-20', 5, 2500.00, 250.00, 2750.00, 3025.00, 1, 1, 1),
(2, 'Pedido de mobiliario', '2023-11-18', 5, 1200.00, 180.00, 1380.00, NULL, 2, 2, 2),
(3, 'Pedido de ropa', '2023-11-15', 20, 450.00, 67.50, 517.50, NULL, 3, 3, 3),
(4, 'Pedido de libros', '2023-11-12', 15, 300.00, 45.00, 345.00, NULL, 4, 4, 4),
(5, 'Pedido de herramientas', '2023-11-10', 7, 350.00, 35.00, 385.00, 423.50, 5, 5, 5),
(6, 'Pedido de herramientas', '2023-11-10', 9, 4320.00, 432.00, 4752.00, 5227.20, 2, 5, 5);

--
-- Disparadores `pedido`
--
DELIMITER $$
CREATE TRIGGER `initialize_pedido_fields` BEFORE INSERT ON `pedido` FOR EACH ROW BEGIN
    SET NEW.fecha = current_timestamp; 
    SET NEW.cantProduct = 0;
    SET NEW.subtotal = 0.00;
    SET NEW.iva = 0.00;
    SET NEW.total = 0.00;
    
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ped_mat`
--

CREATE TABLE `ped_mat` (
  `pedido` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `importe` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `ped_mat`
--

INSERT INTO `ped_mat` (`pedido`, `material`, `cantidad`, `importe`) VALUES
(2, 2, 2, 180.00),
(3, 3, 10, 450.00),
(4, 4, 8, 240.00),
(5, 5, 4, 320.00),
(2, 3, 1, 150.00),
(2, 4, 5, 150.00),
(5, 1, 3, 30.00),
(6, 1, 3, 1500.00),
(6, 2, 3, 2700.00),
(6, 3, 3, 120.00),
(1, 1, 5, 2500.00);

--
-- Disparadores `ped_mat`
--
DELIMITER $$
CREATE TRIGGER `actualizar_stock_en_venta_almacen` AFTER INSERT ON `ped_mat` FOR EACH ROW BEGIN
    DECLARE material_stock INT;
    DECLARE material_cantidad INT;
    DECLARE material_almacen INT;

    
    SELECT stock INTO material_stock
    FROM mat_alma
    WHERE almacen = (SELECT almacen FROM pedido WHERE codigo = NEW.pedido)
      AND material = NEW.material;

    
    SELECT cantidad INTO material_cantidad
    FROM ped_mat
    WHERE pedido = NEW.pedido AND material = NEW.material;

    
    UPDATE mat_alma
    SET stock = material_stock - material_cantidad
    WHERE almacen = (SELECT almacen FROM pedido WHERE codigo = NEW.pedido)
      AND material = NEW.material;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `calcularImporteMaterialPedido` BEFORE INSERT ON `ped_mat` FOR EACH ROW BEGIN
    DECLARE precio_material DECIMAL(10, 2);

    
    SELECT precioVenta INTO precio_material
    FROM Material
    WHERE Codigo = NEW.material;

    
    SET NEW.importe = precio_material * NEW.cantidad;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `calcular_totales_pedido` AFTER INSERT ON `ped_mat` FOR EACH ROW BEGIN
    DECLARE cantidad_total INT;
    DECLARE subtotal_pedido DECIMAL(10, 2);
    DECLARE iva_pedido DECIMAL(10, 2);
    DECLARE total_pedido DECIMAL(10, 2);
    DECLARE total_con_intereses DECIMAL(10, 2);

    
    SELECT SUM(cantidad) INTO cantidad_total
    FROM ped_mat
    WHERE pedido = NEW.pedido;

    
    SELECT SUM(importe) INTO subtotal_pedido
    FROM ped_mat
    WHERE pedido = NEW.pedido;

    
    SET iva_pedido = subtotal_pedido * 0.10;

    
    SET total_pedido = subtotal_pedido + iva_pedido;

    
    SET total_con_intereses = total_pedido * 1.10;

    
    UPDATE pedido
    SET cantProduct = cantidad_total,
        subtotal = subtotal_pedido,
        iva = iva_pedido,
        total = total_pedido,
        totalInt = total_con_intereses
    WHERE codigo = NEW.pedido;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `verificarStock` BEFORE INSERT ON `ped_mat` FOR EACH ROW BEGIN
    DECLARE stock_actual INT;

    
    SELECT stock INTO stock_actual
    FROM mat_alma
    WHERE material = NEW.material AND almacen = (select almacen from mat_alma where material = new.material);

    
    IF stock_actual IS NULL OR stock_actual < NEW.cantidad THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'No hay suficiente stock para el producto en el almacén.';
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`codigo`, `nombre`, `correo`) VALUES
(1, 'TechSupplier Inc.', 'info@techsupplier.com'),
(2, 'Furniture World', 'sales@furnitureworld.com'),
(3, 'Fashion Distributors', 'info@fashiondistributors.com'),
(4, 'material Haven', 'orders@materialhaven.com'),
(5, 'Tool Masters', 'support@toolmasters.com'),
(6, 'TechSupplier Inc.', 'info@techsupplier.com'),
(7, 'Furniture World', 'sales@furnitureworld.com'),
(8, 'Fashion Distributors', 'info@fashiondistributors.com'),
(9, 'material Haven', 'orders@materialhaven.com'),
(10, 'Tool Masters', 'support@toolmasters.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resenas`
--

CREATE TABLE `resenas` (
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(20) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `resenas`
--

INSERT INTO `resenas` (`codigo`, `descripcion`, `fecha`, `cliente`) VALUES
(1, 'Buena experiencia de', '2023-11-20', 1),
(2, 'Envío rápido y efici', '2023-11-18', 2),
(3, 'Productos de alta ca', '2023-11-15', 3),
(4, 'Atención al cliente ', '2023-11-12', 4),
(5, 'Satisfecho con los s', '2023-11-10', 5);

--
-- Disparadores `resenas`
--
DELIMITER $$
CREATE TRIGGER `before_insert_resena` BEFORE INSERT ON `resenas` FOR EACH ROW BEGIN
    
    SET NEW.fecha = CURDATE();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`codigo`, `nombre`, `descripcion`) VALUES
(1, 'Electrónicos', 'Dispositivos y componentes electrónicos'),
(2, 'Muebles', 'Varios tipos de muebles'),
(3, 'Piezas', 'piezas y complementos'),
(4, 'Material', 'material de construccion'),
(5, 'Herramientas', 'Herramientas manuales y eléctricas'),
(6, 'Electrónicos', 'Dispositivos y componentes electrónicos'),
(7, 'Muebles', 'Varios tipos de muebles'),
(8, 'Piezas', 'piezas y complementos'),
(9, 'Material', 'material de construccion'),
(10, 'Herramientas', 'Herramientas manuales y eléctricas');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `cambios`
--
ALTER TABLE `cambios`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `pedido` (`pedido`),
  ADD KEY `almacen` (`almacen`);

--
-- Indices de la tabla `camb_material`
--
ALTER TABLE `camb_material`
  ADD KEY `cambio` (`cambio`),
  ADD KEY `material` (`material`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Proveedor` (`Proveedor`),
  ADD KEY `almacen` (`almacen`);

--
-- Indices de la tabla `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Tipo` (`Tipo`);

--
-- Indices de la tabla `material_compra`
--
ALTER TABLE `material_compra`
  ADD KEY `material` (`material`),
  ADD KEY `compra` (`compra`);

--
-- Indices de la tabla `mat_alma`
--
ALTER TABLE `mat_alma`
  ADD KEY `almacen` (`almacen`),
  ADD KEY `material` (`material`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `pedido` (`pedido`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `almacen` (`almacen`),
  ADD KEY `lugar` (`lugar`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `ped_mat`
--
ALTER TABLE `ped_mat`
  ADD KEY `pedido` (`pedido`),
  ADD KEY `material` (`material`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `resenas`
--
ALTER TABLE `resenas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`codigo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacen`
--
ALTER TABLE `almacen`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `cambios`
--
ALTER TABLE `cambios`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `lugar`
--
ALTER TABLE `lugar`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `material`
--
ALTER TABLE `material`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `resenas`
--
ALTER TABLE `resenas`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cambios`
--
ALTER TABLE `cambios`
  ADD CONSTRAINT `cambios_ibfk_1` FOREIGN KEY (`pedido`) REFERENCES `pedido` (`codigo`),
  ADD CONSTRAINT `cambios_ibfk_2` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`);

--
-- Filtros para la tabla `camb_material`
--
ALTER TABLE `camb_material`
  ADD CONSTRAINT `camb_material_ibfk_1` FOREIGN KEY (`cambio`) REFERENCES `cambios` (`codigo`),
  ADD CONSTRAINT `camb_material_ibfk_2` FOREIGN KEY (`material`) REFERENCES `material` (`Codigo`);

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `compra_ibfk_1` FOREIGN KEY (`Proveedor`) REFERENCES `proveedor` (`codigo`),
  ADD CONSTRAINT `compra_ibfk_2` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`);

--
-- Filtros para la tabla `material`
--
ALTER TABLE `material`
  ADD CONSTRAINT `material_ibfk_1` FOREIGN KEY (`Tipo`) REFERENCES `tipos` (`codigo`);

--
-- Filtros para la tabla `material_compra`
--
ALTER TABLE `material_compra`
  ADD CONSTRAINT `material_compra_ibfk_1` FOREIGN KEY (`material`) REFERENCES `material` (`Codigo`),
  ADD CONSTRAINT `material_compra_ibfk_2` FOREIGN KEY (`compra`) REFERENCES `compra` (`Codigo`);

--
-- Filtros para la tabla `mat_alma`
--
ALTER TABLE `mat_alma`
  ADD CONSTRAINT `mat_alma_ibfk_1` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`),
  ADD CONSTRAINT `mat_alma_ibfk_2` FOREIGN KEY (`material`) REFERENCES `material` (`Codigo`);

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`pedido`) REFERENCES `pedido` (`codigo`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`),
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`lugar`) REFERENCES `lugar` (`codigo`),
  ADD CONSTRAINT `pedido_ibfk_3` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`codigo`);

--
-- Filtros para la tabla `ped_mat`
--
ALTER TABLE `ped_mat`
  ADD CONSTRAINT `ped_mat_ibfk_1` FOREIGN KEY (`pedido`) REFERENCES `pedido` (`codigo`),
  ADD CONSTRAINT `ped_mat_ibfk_2` FOREIGN KEY (`material`) REFERENCES `material` (`Codigo`);

--
-- Filtros para la tabla `resenas`
--
ALTER TABLE `resenas`
  ADD CONSTRAINT `resenas_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
