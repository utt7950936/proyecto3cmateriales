from django.urls import path

from core import views

from .models import *
from .views import *

app_name = "core"
urlpatterns = [
    
    # URL para el listado general
    path('administracion/', listado_general, name='administracion'),
    
    
##Clientes
path('list/clientes/', views.ListClientes.as_view(), name="List_Clientes"),
path('create/cliente/', views.createCliente.as_view(), name="createCliente"),
path('detail/cliente/<int:pk>/', views.DetailClientes.as_view(), name="detailCliente"),
path('update/cliente/<int:pk>/', views.UpdateCliente.as_view(), name="updateCliente"),
path('delete/cliente/<int:pk>/', views.DeleteCliente.as_view(), name="deleteCliente"),

##Tipos
path('list/tipos/', views.ListTipo.as_view(), name="list_tipos"),
path('create/tipo/', views.CreateTipo.as_view(), name="create_tipo"),
path('detail/tipo/<int:pk>/', views.DetailTipo.as_view(), name="detail_tipo"),
path('update/tipo/<int:pk>/', views.UpdateTipo.as_view(), name="update_tipo"),
path('delete/tipo/<int:pk>/', views.DeleteTipo.as_view(), name="delete_tipo"),

##Lugar
path('list/lugar/', views.ListLugar.as_view(), name="listLugar"),
path('create/lugar/', views.CreateLugar.as_view(), name="create_lugar"),
path('detail/lugar/<int:pk>/', views.DetailLugar.as_view(), name="detail_lugar"),
path('update/lugar/<int:pk>/', views.UpdateLugar.as_view(), name="update_lugar"),
path('delete/lugar/<int:pk>/', views.DeleteLugar.as_view(), name="delete_lugar"),

##Catalogo
path('catalogo/', views.Catalogo.as_view(), name="Catalogo"),
    path('agregar_material/<int:pk>/', views.agregar_material.as_view(), name='agregar_material'),
    path('eliminar_material/<int:pk>/', views.eliminar_material.as_view(), name='eliminar_material'),
    path('restar_material/<int:pk>/', views.restar_material.as_view(), name='restar_material'),
    path('limpiar_carrito/', views.limpiar_carrito.as_view(), name='limpiar_carrito'),
    path('confirmar/pedido/', views.CreatePedidoCarro.as_view(), name="pedido_creo"),


    

##Material
path('list/Material/', views.ListMaterial.as_view(), name="list_Mat"),
path('create/Material/', views.CreateMaterial.as_view(), name="create_Mat"),
path('detail/Material/<int:pk>/', views.DetailMaterial.as_view(), name="detail_Mat"),
path('update/Material/<int:pk>/', views.UpdateMaterial.as_view(), name="update_Mat"),
path('delete/Material/<int:pk>/', views.DeleteMaterial.as_view(), name="delete_Mat"),

##Pedido

path('list/pedido/', views.ListPedido.as_view(), name="list_pedido"),
# path('create/pedido/', views.CreatePedido.as_view(), name="create_pedido"),
path('detail/pedido/<int:pk>/', views.DetailPedido.as_view(), name="detail_pedido"),
path('update/pedido/<int:pk>/', views.UpdatePedido.as_view(), name="update_pedido"),
path('delete/pedido/<int:pk>/', views.DeletePedido.as_view(), name="delete_pedido"),


##Almacen

path('list/almacen/', views.ListAlmacenes.as_view(), name="list_almacen"),
path('create/almacen/', views.CreateAlmacen.as_view(), name="create_almacen"),
path('detail/almacen/<int:pk>/', views.DetailAlmacen.as_view(), name="detail_almacen"),
path('update/almacen/<int:pk>/', views.UpdateAlmacen.as_view(), name="update_almacen"),
path('delete/almacen/<int:pk>/', views.DeleteAlmacen.as_view(), name="delete_almacen"),


##Resenas

path('list/resenas/', views.Listresenas.as_view(), name="list_resena"),
path('create/resenas/', views.Createresenas.as_view(), name="create_resena"),
path('detail/resenas/<int:pk>/', views.Detailresenas.as_view(), name="detail_resena"),
path('update/resenas/<int:pk>/', views.Updateresenas.as_view(), name="update_resena"),
path('delete/resenas/<int:pk>/', views.Deleteresenas.as_view(), name="delete_resena"),

##Pagos

path('list/pagos/', views.ListPago.as_view(), name="list_Pago"),
path('create/pagos/', views.CreatePago.as_view(), name="create_Pago"),
path('detail/pagos/<int:pk>/', views.DetailPago.as_view(), name="detail_Pago"),
path('update/pagos/<int:pk>/', views.UpdatePago.as_view(), name="update_Pago"),
path('delete/pagos/<int:pk>/', views.DeletePago.as_view(), name="delete_Pago"),

##Cambios

# path('list/cambios/', views.ListCambios.as_view(), name="list_cambio"),
# path('create/cambios/', views.CreateCambios.as_view(), name="create_cambio"),
# path('detail/cambios/<int:pk>/', views.DetailCambios.as_view(), name="detail_cambio"),
# path('update/cambios/<int:pk>/', views.UpdateCambios.as_view(), name="update_cambio"),
# path('delete/cambios/<int:pk>/', views.DeleteCambios.as_view(), name="delete_cambio"),


##Proveedor
path('list/Proveedor/', views.ListProveedor.as_view(), name="List_Proveedor"),
path('create/Proveedor/', views.createProveedor.as_view(), name="createProveedor"),
path('detail/Proveedor/<int:pk>/', views.DetailProveedor.as_view(), name="detailProveedor"),
path('update/Proveedor/<int:pk>/', views.UpdateProveedor.as_view(), name="updateProveedor"),
path('delete/Proveedor/<int:pk>/', views.DeleteProveedor.as_view(), name="deleteProveedor"),



##Compra

path('list/compra/', views.ListCompra.as_view(), name="list_compra"),
path('create/compra/', views.CreateCompra.as_view(), name="create_compra"),
path('detail/compra/<int:pk>/', views.DetailCompra.as_view(), name="detail_compra"),
path('update/compra/<int:pk>/', views.UpdateCompra.as_view(), name="update_compra"),
path('delete/compra/<int:pk>/', views.DeleteCompra.as_view(), name="delete_compra"),





]
