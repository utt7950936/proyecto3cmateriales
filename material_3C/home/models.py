from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.


# class UserProfile(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     timestamp = models.DateField(auto_now_add=True, auto_now=False)
#     updated = models.DateField(auto_now_add=False, auto_now=True)
#     bio = models.CharField(max_length=256, default="i Love This APP")
#     status = models.BooleanField(default=True)

#     def __str__(self):
#         return self.user.first_name
        
#     @receiver(post_save, sender=User)
#     def create_user_profile(sender, instance, created, *args, **kwargs):
#         if created:
#             profile, created = UserProfile.objects.get_or_create(user=instance)
#             instance.userprofile = profile
#             instance.save()


# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, *args, **kwargs):
#         if created:
#             UserProfile.objects.create(user=instance)
#         instance.userprofile.save()


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    updated = models.DateField(auto_now_add=False, auto_now=True)
    bio = models.CharField(max_length=256, default="i Love This APP")
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.user.first_name

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, *args, **kwargs):
    if created:
        profile, created = UserProfile.objects.get_or_create(user=instance)
        instance.userprofile = profile
        instance.save()