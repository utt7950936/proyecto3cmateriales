from django.contrib import admin

from .models import *

# Register your models here.

# se le pone el punto(.) para indicar que esta en el mismo folfer


@admin.register(Cliente)
class ClientesAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "telefono",
        "correo"
    ]

@admin.register(Lugar)
class LugarAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "num_calle",
        "colonia",
        "codigo_postal"
    ]

@admin.register(Tipos)
class TiposAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "descripcion"
    ]


@admin.register(Compra)
class CompraAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "descripcion",
        "fecha",
        "subtotal",
        "iva",
        "total",
        "proveedor",
        "almacen"
    ]

@admin.register(Material)
class MaterialAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "descripcion",
        "preciocompra",
        "precioventa",
        "tipo",
        "imagen"
    ]

@admin.register(Almacen)
class AlmacenAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nomcalle",
        "num_calle",
        "colonia",
        "codigo_postal"
    ]

# @admin.register(CambMaterial)
# class CambMaterialAdmin(admin.ModelAdmin):
#     list_display = [
#         "cambio",
#         "material",
#         "cantidad_devuelta",
#         "cantidad_cambiada"
#     ]

# @admin.register(Cambios)
# class CambiosAdmin(admin.ModelAdmin):
#     list_display = [
#         "codigo",
#         "fecha",
#         "descripcion",
#         "pedido",
#         "almacen"
#     ]

@admin.register(MatAlma)
class MatAlmaAdmin(admin.ModelAdmin):
    list_display = [
        "almacen",
        "material",
        "stock"
    ]

@admin.register(MaterialCompra)
class MaterialCompraAdmin(admin.ModelAdmin):
    list_display = [
        "material",
        "compra"
    ]

@admin.register(Pago)
class PagoAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "num_pago",
        "montopago",
        "concepto",
        "pedido"
    ]

@admin.register(PedMat)
class PedMatAdmin(admin.ModelAdmin):
    list_display = [
        "pedido",
        "material",
        "cantidad",
        "importe"
    ]

@admin.register(Pedido)
class PedidoAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "descripcion",
        "fecha",
        "cantproduct",
        "subtotal",
        "iva",
        "total",
        "almacen",
        "lugar",
        "cliente"
    ]

@admin.register(Proveedor)
class ProveedorAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "correo"
    ]

@admin.register(Resenas)
class ResenasAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "descripcion",
        "fecha",
        "cliente"
    ]

