-- Active: 1700758310449@@127.0.0.1@3307@materiales3c

-- CONSULTAS

-- 1.Informacion de un pedido
    -- - Numero del pedido
    -- - Nombre del almacen
    -- - Fecha del pedido
    -- - Nombre del cliente
    -- - Numero de telefono del cliente
    -- - Direccion del lugar de entrega, en una columna
    -- - Subtotal
    -- - IVA
    -- - Total
    -- - Total con intereses
    -- - Cantidad total de productos


SELECT
    ped.codigo AS 'Numero del pedido',
    a.nombre AS 'Nombre del almacen',
    DATE_FORMAT(ped.fecha, '%d-%m-%Y') as 'Fecha del pedido',
    c.Nombre AS 'Nombre del cliente',
    c.telefono AS 'Numero de telefono del cliente',
    CONCAT(l.nomCalle, ', ', l.colonia, ', ', l.codigo_postal) AS 'Direccion del lugar de entrega',
    ped.subtotal AS 'Subtotal',
    ped.iva AS 'IVA',
    ped.total AS 'Total',
    ped.totalInt AS 'Total con intereses',
    ped.cantProduct AS 'Cantidad total de productos'
FROM
    pedido ped
inner join almacen as a ON ped.almacen = a.codigo
inner join lugar as l ON ped.lugar = l.codigo
inner join Cliente as c ON ped.cliente = c.codigo
WHERE
    ped.codigo = 2;






-- 2. Materiales incluidos en un pedido
-- - Numero del pedido
-- - Nombre del material
-- - Nombre del tipo de material
-- - Precio unitario
-- - Cantidad
-- - Importe

--Por comprobar

SELECT
    pm.pedido AS 'Numero del pedido',
    m.Nombre AS 'Nombre del material',
    t.nombre AS 'Nombre del tipo de material',
    m.PrecioVenta AS 'Precio unitario',
    pm.cantidad AS 'Cantidad',
    pm.importe AS 'Importe'
FROM ped_mat as pm
inner join Material as m ON pm.material = m.Codigo
inner join tipos as t ON m.Tipo = t.codigo
WHERE
    pm.pedido = 1;

--Subconsulta

    SELECT
        pm.pedido AS 'Numero del pedido',
        m.Nombre AS 'Nombre del material',
        t.nombre AS 'Nombre del tipo de material',
        m.PrecioVenta AS 'Precio unitario',
        pm.cantidad AS 'Cantidad',
        pm.importe AS 'Importe'
    FROM ped_mat AS pm
    INNER JOIN Material AS m ON pm.material = m.Codigo
    INNER JOIN tipos AS t ON m.Tipo = t.codigo
    WHERE pm.pedido IN (
        SELECT codigo
        FROM pedido
        WHERE codigo = 1
    );





-- 3. Pagos realizados por un pedido
-- - Numero del pedido
-- - Monto total con intereses
-- - Fecha del pago
-- - Numero del pago
-- - Monto del pago
-- - Concepto del pago
-- - Saldo

SELECT
    ped.codigo AS 'Numero del pedido',
    ped.totalInt AS 'Monto total con intereses',
    DATE_FORMAT(pg.fecha, '%d-%m-%Y') as 'Fecha del pago',
    pg.num_pago AS 'Numero del pago',
    pg.montoPago AS 'Monto del pago',
    pg.concepto AS 'Concepto del pago',
    (ped.totalInt - SUM(pg.montoPago)) AS 'Saldo'
FROM pedido as ped
left join pago as pg ON ped.codigo = pg.pedido
WHERE
    ped.codigo = 2
GROUP BY
    ped.codigo



-- 4. Reseñas realizadas por un cliente
-- - Nombre del cliente
-- - Fecha de la reseña
-- - Descripcion de la reseña

SELECT
    c.Nombre AS 'Nombre del cliente',
    DATE_FORMAT(r.fecha, '%d-%m-%Y') as 'Fecha de la reseña',
    r.descripcion AS 'Descripcion de la reseña'
FROM resenas as r
inner join Cliente c ON r.cliente = c.codigo
WHERE
    c.codigo = 5;


    select material, cantidad, importe from ped_mat where pedido=2;


-- 5. Cambios realizados de un pedido
-- - Número del pedido
-- - Fecha del pedido
-- - Fecha del cambio
-- - Nombre del material
-- - Cantidad de productos
SELECT
    p.codigo AS 'Número del pedido',
    m.`Codigo`,
    DATE_FORMAT(p.fecha, '%d-%m-%Y')'Fecha del pedido',
    c.fecha AS 'Fecha del cambio',
    m.Nombre AS 'Nombre del material',
    cm.cantidad_cambiada AS 'Cantidad de productos cambiados'
FROM pedido as p
inner join cambios as c ON p.codigo = c.pedido
inner join camb_material as cm ON c.codigo = cm.cambio
inner join Material as m ON cm.material = m.Codigo
WHERE p.codigo = 1;



--Subconsulta

    SELECT
        p.codigo AS 'Número del pedido',
        DATE_FORMAT(p.fecha, '%d-%m-%Y') 'Fecha del pedido',
        c.fecha AS 'Fecha del cambio',
        m.Nombre AS 'Nombre del material',
        cm.cantidad_cambiada AS 'Cantidad de productos cambiados'
    FROM pedido as p
    INNER JOIN cambios as c ON p.codigo = c.pedido
    INNER JOIN camb_material as cm ON c.codigo = cm.cambio
    INNER JOIN Material as m ON cm.material = m.Codigo
    WHERE p.codigo = (SELECT codigo 
                        FROM pedido 
                        WHERE codigo = 2);

-- 6.Materiales del mismo tipo en un almacén
-- - Nombre del almacén
-- - Nombre del tipo de material
-- - Nombre del material
-- - Stock
SELECT * FROM almacen;

SELECT
    a.nombre AS 'Nombre del almacén',
    t.nombre AS 'Nombre del tipo de material',
    m.Nombre AS 'Nombre del material',
    ma.stock AS 'Stock'
FROM mat_alma AS ma
INNER JOIN almacen AS a ON ma.almacen = a.codigo
INNER JOIN Material AS m ON ma.material = m.Codigo
INNER JOIN tipos AS t ON m.Tipo = t.codigo
WHERE a.codigo = 3 AND t.codigo = 5;






-- 7. Información de una compra en un almacén
-- - Nombre del almacén
-- - Número de la compra
-- - Fecha de la compra
-- - Nombre del proveedor
-- - Subtotal
-- - IVA
-- - Total

SELECT
    a.nombre AS 'Nombre del almacén',
    c.Codigo AS 'Número de la compra',
    DATE_FORMAT(c.Fecha, '%d-%m-%Y') 'Fecha de la compra',
    p.nombre AS 'Nombre del proveedor',
    c.Subtotal AS 'Subtotal',
    c.Iva AS 'IVA',
    c.Total AS 'Total'
FROM Compra as c
inner join proveedor as p ON c.Proveedor = p.codigo
inner join almacen as a ON c.almacen = a.codigo
WHERE c.codigo = 2;


-- 8. Materiales incluidos en una compra
-- - Número de la compra
-- - Nombre del material
-- - Nombre del tipo de material
-- - Precio de compra
-- - Cantidad
-- - Importe

SELECT
    -- mc.compra AS 'Número de la compra', 
    m.Nombre AS 'Nombre del material',
    t.nombre AS 'Nombre del tipo de material',
    m.precioCompra AS 'Precio de compra',
    mc.cantidad AS 'Cantidad',
    mc.importe AS 'Importe'
FROM material_compra as mc
inner join Material as m ON mc.material = m.Codigo
inner join tipos as t ON m.Tipo = t.codigo
WHERE mc.compra = 4;


--Subconsulta

    SELECT
        mc.compra AS 'Número de la compra',
        m.Nombre AS 'Nombre del material',
        t.nombre AS 'Nombre del tipo de material',
        m.precioCompra AS 'Precio de compra',
        mc.cantidad AS 'Cantidad',
        mc.importe AS 'Importe'
    FROM material_compra as mc
    INNER JOIN Material as m ON mc.material = m.Codigo
    INNER JOIN tipos as t ON m.Tipo = t.codigo
    WHERE mc.compra IN (SELECT Codigo 
                            FROM Compra 
                            WHERE Codigo = 1);

-- 9. Compras realizadas a un proveedor en un almacén
-- - Nombre del almacén
-- - Nombre del proveedor
-- - Fecha de la compra
-- - Número de la compra
-- - Monto total de la compra
-- - Cantidad total de productos adquiridos

    SELECT
        a.nomCalle AS 'Nombre del almacén',
        p.nombre AS 'Nombre del proveedor',
        DATE_FORMAT(c.fecha, '%d-%m-%Y') 'Fecha de la compra',
        c.Codigo AS 'Número de la compra',
        c.Total AS 'Monto total de la compra',
        c.prodTotal AS 'Cantidad total de productos adquiridos'
    FROM Compra as c
    INNER JOIN proveedor as p ON c.Proveedor = p.codigo
    INNER JOIN almacen as a ON c.almacen = a.codigo
    WHERE a.codigo = 1;



--Subconsulta
    SELECT
        a.nomCalle AS 'Nombre del almacén',
        p.nombre AS 'Nombre del proveedor',
        DATE_FORMAT(c.fecha, '%d-%m-%Y') 'Fecha de la compra',
        c.Codigo AS 'Número de la compra',
        c.Total AS 'Monto total de la compra',
        c.prodTotal AS 'Cantidad total de productos adquiridos'
    FROM Compra as c
    INNER JOIN proveedor as p ON c.Proveedor = p.codigo
    INNER JOIN almacen as a ON c.almacen = a.codigo
    WHERE a.codigo IN (SELECT codigo FROM almacen WHERE codigo = 1);


-- 10. Almacenes que surten al mismo proveedor
-- - Nombre del proveedor
-- - Nombre del almacén  

SELECT
    p.nombre AS 'Nombre del proveedor',
    a.nombre AS 'Nombre del almacén'
FROM Compra as c
inner join proveedor as p ON c.Proveedor = p.codigo
inner join almacen as a ON c.almacen = a.codigo
WHERE  p.codigo = 2
GROUP BY
    p.nombre, a.nombre;




