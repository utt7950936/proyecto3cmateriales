-- Active: 1700758310449@@127.0.0.1@3307@Football
-- Para MySQL00000000000000
CREATE DATABASE materiales3c;

use materiales3c;

CREATE TABLE Cliente (
    codigo INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(80) not null,
    telefono VARCHAR(15) not null,
    correo VARCHAR(80)
);

CREATE TABLE lugar (
    codigo INT AUTO_INCREMENT PRIMARY KEY,
    nomCalle VARCHAR(30) not null,
    num_calle VARCHAR(30) not null,
    colonia VARCHAR(30) not null,
    codigo_postal VARCHAR(30) not null
);


CREATE TABLE tipos (
    codigo INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(30) not null,
    descripcion VARCHAR(50)
);

CREATE table almacen(
    codigo INT auto_increment PRIMARY key,
    nombre VARCHAR(50),
    nomCalle VARCHAR(50) not null,
    num_calle VARCHAR(30) not null,
    colonia VARCHAR(30) not null,
    codigo_postal VARCHAR(30) not null
);

CREATE TABLE proveedor (
    codigo int auto_increment PRIMARY KEY,
    nombre VARCHAR(50) not null,
    correo VARCHAR(50) not null
);

CREATE TABLE resenas (
    codigo INT AUTO_INCREMENT PRIMARY KEY,
    descripcion VARCHAR(4000),
    fecha date,
    cliente INT not null,
    FOREIGN KEY (cliente) REFERENCES Cliente(codigo)
);


CREATE TABLE Compra (
    Codigo INT AUTO_INCREMENT PRIMARY KEY,
    Descripcion VARCHAR(200),
    Fecha DATE,
    prodTotal int,
    Subtotal DECIMAL(10, 2),
    Iva DECIMAL(10, 2),
    Total DECIMAL(10, 2),
    Proveedor INT not null,
    almacen INT not null, 
    FOREIGN KEY (Proveedor) REFERENCES proveedor(codigo),
    FOREIGN KEY (almacen) REFERENCES almacen(codigo)
);

CREATE TABLE Material (
    Codigo INT AUTO_INCREMENT PRIMARY KEY,
    Nombre VARCHAR(80),
    Descripcion TEXT,
    precioCompra DECIMAL(10,2),
    PrecioVenta DECIMAL(10, 2),
    Tipo int,
    imagen VARCHAR(500) DEFAULT 'nop.png',
    FOREIGN KEY (Tipo) REFERENCES tipos(codigo)
);


CREATE table pedido (
    codigo int auto_increment PRIMARY KEY,
    descripcion varchar(500),
    fecha date,
    cantProduct int,
    subtotal decimal(10,2),
    iva decimal(10,2),
    total decimal(10,2),
    totalInt DECIMAL(10,2),
    almacen int,
    lugar int,
    cliente int,
    FOREIGN KEY (almacen) REFERENCES almacen(codigo),
    FOREIGN KEY (lugar) REFERENCES lugar(codigo),
    FOREIGN KEY (cliente) REFERENCES Cliente(codigo)
);



CREATE TABLE pago (
    Codigo INT AUTO_INCREMENT PRIMARY KEY,
    fecha date,
    num_pago VARCHAR(20),
    montoPago DECIMAL(10, 2),
    concepto VARCHAR(100),
    saldo DECIMAL(10,2),
    pedido INT not null,
    FOREIGN KEY (pedido) REFERENCES pedido(codigo)
);



    create table cambios (
        codigo int auto_increment PRIMARY KEY,
        fecha DATE , 
        descripcion VARCHAR(100),
        material_devuelto int,
        material_cambiado int,
        pedido int not null,
        almacen int not null,
         FOREIGN KEY (material_devuelto) REFERENCES material(codigo),
        FOREIGN KEY (material_cambiado) REFERENCES material(codigo),
            FOREIGN KEY (pedido) REFERENCES pedido(codigo),
        FOREIGN KEY (almacen) REFERENCES almacen(codigo)
    );





create table ped_mat (
    pedido int NOT NULL,
    material int NOT NULL,
    cantidad int NOT NULL,
    importe DECIMAL(10,2),
    PRIMARY KEY (pedido, material),
    FOREIGN KEY (pedido) REFERENCES pedido(codigo),
    FOREIGN KEY (material) REFERENCES Material(codigo) 
);

    create table mat_alma (
        almacen int NOT NULL,
        material int NOT NULL,
        stock int NOT NULL,
        PRIMARY KEY (almacen, material),
        FOREIGN KEY (almacen) REFERENCES almacen(codigo),
        FOREIGN KEY (material) REFERENCES Material(codigo)
    );
 
    create table material_compra (
        material int NOT NULL,
        compra int NOT NULL,
        cantidad int,
        importe DECIMAL(10,2),
        PRIMARY KEY (material, compra),
        FOREIGN KEY (material) REFERENCES Material(codigo),
        FOREIGN KEY (compra) REFERENCES Compra(codigo) 
    );



CREATE table camb_material (
    cambio int NOT NULL,
    material_devuelta int,
    material_cambiada int,
    cantidad_cambiado int,
    cantidad_devuelto int,
    importe DECIMAL(10,2),
    PRIMARY KEY (cambio),
    FOREIGN KEY (cambio) REFERENCES cambios(codigo),
    FOREIGN KEY ( material_devuelta) REFERENCES Material(codigo),
       FOREIGN KEY ( material_cambiada) REFERENCES Material(codigo) 
);


