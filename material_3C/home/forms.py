from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import UserProfile


class LoginForm(forms.Form):
    username = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu username"}))
    password = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu password"}))


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu username"}))
    password1 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu password"}))
    password2 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Repite tu password"}))
    email = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu email"}))
    first_name = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu first_name"}))
    last_name = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu last_name"}))
    class Meta:
        model = User
        fields = [
            "username",
            "password1",
            "password2",
            "email",
            "first_name",
            "last_name"
        
        ]
        # widgets = {
        #     "username": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu username"}),
        #     "password": forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu password"}),
        #     "email": forms.EmailInput(attrs={"type":"email", "class":"form-control", "placeholder":"Escribe tu email"})
        # }



class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = [
            "user",
            "bio",
            "status"
        ]
        widgets = {
            "user": forms.Select(attrs={"type":"select", "class":"form-select"}),
            "bio": forms.Textarea(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe acerca de ti", "row":3}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
        }