# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Cliente(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(db_column='Nombre', max_length=80)  # Field name made lowercase.
    telefono = models.CharField(max_length=15)
    correo = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Cliente'


class Compra(models.Model):
    codigo = models.AutoField(db_column='Codigo', primary_key=True)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=200, blank=True, null=True)  # Field name made lowercase.
    fecha = models.DateField(db_column='Fecha', blank=True, null=True)  # Field name made lowercase.
    prodtotal = models.IntegerField(db_column='prodTotal', blank=True, null=True)  # Field name made lowercase.
    subtotal = models.DecimalField(db_column='Subtotal', max_digits=10, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    iva = models.DecimalField(db_column='Iva', max_digits=5, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    total = models.DecimalField(db_column='Total', max_digits=10, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    proveedor = models.ForeignKey('Proveedor', models.DO_NOTHING, db_column='Proveedor')  # Field name made lowercase.
    almacen = models.ForeignKey('Almacen', models.DO_NOTHING, db_column='almacen')

    class Meta:
        managed = False
        db_table = 'Compra'


class Material(models.Model):
    codigo = models.AutoField(db_column='Codigo', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=80, blank=True, null=True)  # Field name made lowercase.
    descripcion = models.TextField(db_column='Descripcion', blank=True, null=True)  # Field name made lowercase.
    preciocompra = models.DecimalField(db_column='precioCompra', max_digits=10, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    precioventa = models.DecimalField(db_column='PrecioVenta', max_digits=10, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    tipo = models.ForeignKey('Tipos', models.DO_NOTHING, db_column='Tipo', blank=True, null=True)  # Field name made lowercase.
    imagen = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Material'


class Almacen(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, blank=True, null=True)
    nomcalle = models.CharField(db_column='nomCalle', max_length=50)  # Field name made lowercase.
    num_calle = models.CharField(max_length=30)
    colonia = models.CharField(max_length=30)
    codigo_postal = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'almacen'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class CambMaterial(models.Model):
    cambio = models.OneToOneField('Cambios', models.DO_NOTHING, db_column='cambio', primary_key=True)  # The composite primary key (cambio, material) found, that is not supported. The first column is selected.
    material = models.ForeignKey(Material, models.DO_NOTHING, db_column='material')
    cantidad_devuelta = models.IntegerField(blank=True, null=True)
    cantidad_cambiada = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'camb_material'
        unique_together = (('cambio', 'material'),)


class Cambios(models.Model):
    codigo = models.AutoField(primary_key=True)
    fecha = models.DateField(blank=True, null=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    pedido = models.ForeignKey('Pedido', models.DO_NOTHING, db_column='pedido')
    almacen = models.ForeignKey(Almacen, models.DO_NOTHING, db_column='almacen')

    class Meta:
        managed = False
        db_table = 'cambios'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class HomeUserprofile(models.Model):
    id = models.BigAutoField(primary_key=True)
    timestamp = models.DateField()
    updated = models.DateField()
    bio = models.CharField(max_length=256)
    status = models.IntegerField()
    user = models.OneToOneField(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'home_userprofile'


class Lugar(models.Model):
    codigo = models.AutoField(primary_key=True)
    nomcalle = models.CharField(db_column='nomCalle', max_length=30)  # Field name made lowercase.
    num_calle = models.CharField(max_length=30)
    colonia = models.CharField(max_length=30)
    codigo_postal = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'lugar'


class MatAlma(models.Model):
    almacen = models.OneToOneField(Almacen, models.DO_NOTHING, db_column='almacen', primary_key=True)  # The composite primary key (almacen, material) found, that is not supported. The first column is selected.
    material = models.ForeignKey(Material, models.DO_NOTHING, db_column='material')
    stock = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'mat_alma'
        unique_together = (('almacen', 'material'),)


class MaterialCompra(models.Model):
    material = models.OneToOneField(Material, models.DO_NOTHING, db_column='material', primary_key=True)  # The composite primary key (material, compra) found, that is not supported. The first column is selected.
    compra = models.ForeignKey(Compra, models.DO_NOTHING, db_column='compra')
    cantidad = models.IntegerField(blank=True, null=True)
    importe = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'material_compra'
        unique_together = (('material', 'compra'),)


class Pago(models.Model):
    codigo = models.AutoField(db_column='Codigo', primary_key=True)  # Field name made lowercase.
    fecha = models.DateField(blank=True, null=True)
    num_pago = models.CharField(max_length=20, blank=True, null=True)
    montopago = models.DecimalField(db_column='montoPago', max_digits=10, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    concepto = models.CharField(max_length=100, blank=True, null=True)
    saldo = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    pedido = models.ForeignKey('Pedido', models.DO_NOTHING, db_column='pedido')

    class Meta:
        managed = False
        db_table = 'pago'


class PedMat(models.Model):
    pedido = models.OneToOneField('Pedido', models.DO_NOTHING, db_column='pedido', primary_key=True)  # The composite primary key (pedido, material) found, that is not supported. The first column is selected.
    material = models.ForeignKey(Material, models.DO_NOTHING, db_column='material')
    cantidad = models.IntegerField()
    importe = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ped_mat'
        unique_together = (('pedido', 'material'),)


class Pedido(models.Model):
    codigo = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=500, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    cantproduct = models.IntegerField(db_column='cantProduct', blank=True, null=True)  # Field name made lowercase.
    subtotal = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    iva = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    total = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    totalint = models.DecimalField(db_column='totalInt', max_digits=10, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    almacen = models.ForeignKey(Almacen, models.DO_NOTHING, db_column='almacen', blank=True, null=True)
    lugar = models.ForeignKey(Lugar, models.DO_NOTHING, db_column='lugar', blank=True, null=True)
    cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='cliente', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pedido'


class Proveedor(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    correo = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'proveedor'


class Resenas(models.Model):
    codigo = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=4000, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='cliente')

    class Meta:
        managed = False
        db_table = 'resenas'


class Tipos(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tipos'
