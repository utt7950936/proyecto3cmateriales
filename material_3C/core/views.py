from django.contrib import messages
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views import generic

from .forms import *
from .models import *

# Create your views here.

#Administracion
def listado_general(request):
    return render(request, 'core/bd/administracion.html')




class ListClientes(generic.View):
    template_name = "core/bd/cliente/list_Clientes.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Cliente.objects.all()
        self.context = {
            "Clientes": queryset  
        }
        return render(request, self.template_name, self.context)



class DetailClientes(generic.DetailView):
    template_name = "core/bd/cliente/detailCliente.html"
    model = Cliente


class createCliente(generic.CreateView):
    template_name = "core/bd/cliente/createCliente.html"
    model = Cliente
    form_class = ClientesForm
    success_url = reverse_lazy("core:List_Clientes")


class UpdateCliente(generic.UpdateView):
    template_name = "core/bd/cliente/UpdateCliente.html"
    model = Cliente
    form_class = UpdateClientesForm
    success_url = reverse_lazy("core:List_Clientes")

class DeleteCliente(generic.DeleteView):
    template_name = "core/bd/cliente/deleteCliente.html"
    model = Cliente
    success_url = reverse_lazy("core:List_Clientes")


##Equipos

class ListTipo(generic.View):
    template_name = "core/bd/tipos/listTipo.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Tipos.objects.all()
        self.context = {
            "Tipos": queryset
        }
        return render(request, self.template_name,self.context)
    
class CreateTipo(generic.CreateView):
    template_name = "core/bd/tipos/create_tipo.html"
    model = Tipos
    form_class = TiposForm
    success_url = reverse_lazy("core:list_tipos")

    def form_valid(self, form):
        # Aquí puedes realizar operaciones adicionales si es necesario
        return super().form_valid(form)




class DetailTipo(generic.DetailView):
    template_name = "core/bd/tipos/detailTipo.html"
    model = Tipos

class UpdateTipo(generic.UpdateView):
    template_name = "core/bd/tipos/UpdateTipo.html"
    model = Tipos
    form_class = UpdateTiposForm
    success_url = reverse_lazy("core:list_tipos")

class DeleteTipo(generic.DeleteView):
    template_name = "core/bd/tipos/deleteTipo.html"
    model = Tipos
    success_url = reverse_lazy("core:list_tipos")

##Lugares

class ListLugar(generic.View):
    template_name = "core/bd/lugar/listLugar.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Lugar.objects.all()
        self.context = {
            "Lugar": queryset
        }
        return render(request, self.template_name,self.context)

class CreateLugar(generic.CreateView):
    template_name = "core/bd/lugar/createLugar.html"
    model = Lugar
    form_class = LugarForm
    success_url = reverse_lazy("core:listLugar")

class DetailLugar(generic.DetailView):
    template_name = "core/bd/lugar/detailLugar.html"
    model = Lugar

class UpdateLugar(generic.UpdateView):
    template_name = "core/bd/lugar/UpdateLugar.html"
    model = Lugar
    form_class = UpdateLugarForm
    success_url = reverse_lazy("core:listLugar")

class DeleteLugar(generic.DeleteView):
    template_name = "core/bd/lugar/deleteLugar.html"
    model = Lugar
    success_url = reverse_lazy("core:listLugar")
    
    
# class Catalogo(generic.View):
#     template_name = "core/listado.html"
#     context = {}

#     def get(self, request, *args, **kwargs):
#         self.context = {
#             "name": "Prueba catalogo"
#         }
#         return render(request, self.template_name, self.context)
    
    
#     def listado_productos(request):
#         Materiales = Material.objects.all()
#         return render(request, "core/listado.html", {
#             "Materiales": Materiales
#         })
    

class Catalogo(generic.View):
    template_name = "core/listado.html"

    def get(self, request, *args, **kwargs):
        Materiales = Material.objects.all()
        carrito = Carrito(request)
        # Calculate the total from the carrito
        total_carrito = sum(item["acumulado"] for item in carrito.carrito.values())

        context = {
            "Materiales": Materiales,
            "carrito": carrito.carrito,
            "total_carrito": total_carrito,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        carrito = Carrito(request)
        Material_codigo = request.POST.get('Material_codigo') 
        material = Material.objects.get(pk=Material_codigo)

        carrito.agregar(material)

        return redirect('core:Catalogo')  
    
class tienda(generic.View):
    template_name = "core/listado.html"

    def get(self, request, *args, **kwargs):
        materiales = Material.objects.all()
        return render(request, self.template_name, {'materiales': materiales})

    def post(self, request, *args, **kwargs):
        carrito = Carrito(request)
        Material_codigo = request.POST.get('Material_codigo') 
        material = Material.objects.get(pk=Material_codigo)

        carrito.agregar(material)

        return redirect('core:Catalogo')  

class agregar_material(generic.View):

    def get(self, request, pk, *args, **kwargs):
        carrito = Carrito(request)
        material = Material.objects.get(pk=pk)
        carrito.agregar(material)
        return redirect('core:Catalogo')

class eliminar_material(generic.View):

    def get(self, request, pk):
        carrito = Carrito(request)
        material = Material.objects.get(codigo=pk)
        carrito.eliminar(material)
        return redirect("core:Catalogo")

class restar_material(generic.View):

    def get(self, request, pk):
        carrito = Carrito(request)
        material = Material.objects.get(pk=pk)
        carrito.restar(material)
        return redirect("core:Catalogo")

class limpiar_carrito(generic.View):

    def get(self, request):
        carrito = Carrito(request)
        carrito.limpiar()
        return redirect("core:Catalogo")
from django.db import transaction


class CreatePedidoCarro(generic.CreateView):
    template_name = "core/confirmarpedido.html"
    model = Pedido
    form_class = PedidoFormcarrito
    success_url = reverse_lazy("core:Catalogo")
    success_message = "Pedido creado exitosamente. Código del pedido: %(codigo)s"

    @transaction.atomic
    def form_valid(self, form):
        # Crea un nuevo pedido y obtén su id (pk)
        pedido = form.save()

        # Obtiene el carrito de la sesión
        carrito = Carrito(self.request).carrito

        # Lista para almacenar los objetos PedMat a crear
        pedmat_list = []

        # Asocia los materiales con el pedido
        for key, value in carrito.items():
            material_codigo = value["material_codigo"]
            cantidad = value["cantidad"]
            importe = value["acumulado"]

            # Crea un objeto PedMat para asociar el material con el pedido
            material = Material.objects.get(pk=material_codigo)
            pedmat_list.append(PedMat(pedido=pedido, material=material, cantidad=cantidad, importe=importe))

        # Crea los objetos PedMat en una sola transacción
        PedMat.objects.bulk_create(pedmat_list)

        # Limpia el carrito después de confirmar el pedido
        Carrito(self.request).limpiar()

        # Mensajes de depuración
        print(f"Pedido creado: {pedido.codigo}")
        print(f"Subtotal en el pedido: {pedido.subtotal}")
        print(f"IVA en el pedido: {pedido.iva}")
        print(f"Total en el pedido: {pedido.total}")
        print(f"TotalInt en el pedido: {pedido.totalint}")
        print(f"Cantidad total en el pedido: {pedido.cantproduct}")
        # ... Imprime otros detalles según sea necesario

        messages.success(self.request, self.success_message % {'codigo': pedido.codigo})
        return redirect(self.success_url)
    

# Aqui empiezan los CRUDS



#Material
class ListMaterial(generic.View):
    template_name = "core/bd/material/listmat.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Material.objects.all()
        self.context = {
            "Material": queryset
        }
        return render(request, self.template_name,self.context)
    

class CreateMaterial(generic.CreateView):
    template_name = "core/bd/material/create_mat.html"
    model = Material
    form_class = MaterialForm
    success_url = reverse_lazy("core:list_Mat")
    def form_valid(self, form):
        print(self.request.FILES)  # Verifica si los archivos están llegando a la vista
        print(form.cleaned_data['imagen'])  # Verifica si el formulario recibe la imagen correctamente
        response = super().form_valid(form)
        # Procesar la imagen aquí si es necesario
        return response
    

    

class DetailMaterial(generic.DetailView):
    template_name = "core/bd/material/detailmat.html"
    model = Material

class UpdateMaterial(generic.UpdateView):
    template_name = "core/bd/material/Updatemat.html"
    model = Material
    form_class = UpdateMaterialForm
    success_url = reverse_lazy("core:list_Mat")
    def form_valid(self, form):
        print(self.request.FILES)  # Verifica si los archivos están llegando a la vista
        print(form.cleaned_data['imagen'])  # Verifica si el formulario recibe la imagen correctamente
        response = super().form_valid(form)
        # Procesar la imagen aquí si es necesario
        return response

class DeleteMaterial(generic.DeleteView):
    template_name = "core/bd/material/deletemat.html"
    model = Material
    success_url = reverse_lazy("core:list_Mat")
    
    
    
    
#pedido
class ListPedido(generic.View):
    template_name = "core/bd/pedido/listpedido.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Pedido.objects.all()
        self.context = {
            "Pedido": queryset
        }
        return render(request, self.template_name,self.context)
    


# class CreatePedido(generic.CreateView):
#     template_name = "core/bd/pedido/create_pedido.html"
#     model = Pedido
#     form_class = PedidoForm
#     success_url = reverse_lazy("core:list_pedido")

class DetailPedido(generic.DetailView):
    template_name = "core/bd/pedido/detailpedido.html"
    model = Pedido

class UpdatePedido(generic.UpdateView):
    template_name = "core/bd/pedido/Updatepedido.html"
    model = Pedido
    form_class = UpdatePedidoForm
    success_url = reverse_lazy("core:list_pedido")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pedido = self.get_object()
        pedmat_entries = PedMat.objects.filter(pedido=pedido)
        context["pedmat_entries"] = pedmat_entries
        return context

    def form_valid(self, form):
        response = super().form_valid(form)

        pedido = self.object
        materiales = form.cleaned_data.get("materiales", [])
        cantidad_materiales = form.cleaned_data.get("cantidad_materiales")

        for material in materiales:
            pedmat_entry, created = PedMat.objects.get_or_create(pedido=pedido, material=material)
            pedmat_entry.cantidad = cantidad_materiales
            pedmat_entry.save()

        nuevo_material = form.cleaned_data.get("nuevo_material")
        cantidad_nuevo_material = form.cleaned_data.get("cantidad_nuevo_material")

        if nuevo_material and cantidad_nuevo_material:
            PedMat.objects.create(pedido=pedido, material=nuevo_material, cantidad=cantidad_nuevo_material)

        return response


    
    
class DeletePedido(generic.DeleteView):
    template_name = "core/bd/pedido/deletepedido.html"
    model = Pedido
    success_url = reverse_lazy("core:list_pedido")

    def delete(self, request, *args, **kwargs):
        # Obtener el objeto Pedido que se va a eliminar
        pedido = self.get_object()

        # Obtener y eliminar manualmente todos los registros en la tabla PedMat que tienen el código de pedido
        pedmat_entries = PedMat.objects.filter(pedido=pedido)
        pedmat_entries.delete()

        # Luego, llamar al método delete de la clase base para eliminar el pedido
        return super().delete(request, *args, **kwargs)



##Almacenes

class ListAlmacenes(generic.View):
    template_name = "core/bd/almacen/list_Almacen.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Almacen.objects.all()
        self.context = {
            "Almacen": queryset
        }
        return render(request, self.template_name,self.context)

class CreateAlmacen(generic.CreateView):
    template_name = "core/bd/almacen/createAlmacen.html"
    model = Almacen
    form_class = almacenForm
    success_url = reverse_lazy("core:listAlmacen")

class DetailAlmacen(generic.DetailView):
    template_name = "core/bd/almacen/detailAlmacen.html"
    model = Almacen

class UpdateAlmacen(generic.UpdateView):
    template_name = "core/bd/almacen/UpdateAlmacen.html"
    model = Almacen
    form_class = UpdateAlmacenForm
    success_url = reverse_lazy("core:listAlmacen")

class DeleteAlmacen(generic.DeleteView):
    template_name = "core/bd/almacen/deleteAlmacen.html"
    model = Almacen
    success_url = reverse_lazy("core:listAlmacen")
    
    

##Resenas

class Listresenas(generic.View):
    template_name = "core/bd/resenas/listresenas.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Resenas.objects.all()
        self.context = {
            "Resenas": queryset
        }
        return render(request, self.template_name,self.context)

class Createresenas(generic.CreateView):
    template_name = "core/bd/resenas/createresenas.html"
    model = Resenas
    form_class = ResenasForm
    success_url = reverse_lazy("core:list_resena")

class Detailresenas(generic.DetailView):
    template_name = "core/bd/resenas/detailresenas.html"
    model = Resenas

class Updateresenas(generic.UpdateView):
    template_name = "core/bd/resenas/Updateresenas.html"
    model = Resenas
    form_class = UpdateresenasForm
    success_url = reverse_lazy("core:list_resena")

class Deleteresenas(generic.DeleteView):
    template_name = "core/bd/resenas/deleteresenas.html"
    model = Resenas
    success_url = reverse_lazy("core:list_resena")
    

## Pagos

class ListPago(generic.View):
    template_name = "core/bd/Pago/listPago.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Pago.objects.all()
        self.context = {
            "Pago": queryset
        }
        return render(request, self.template_name,self.context)

class CreatePago(generic.CreateView):
    template_name = "core/bd/Pago/createPago.html"
    model = Pago
    form_class = PagoForm
    success_url = reverse_lazy("core:list_Pago")

class DetailPago(generic.DetailView):
    template_name = "core/bd/Pago/detailPago.html"
    model = Pago

class UpdatePago(generic.UpdateView):
    template_name = "core/bd/Pago/UpdatePago.html"
    model = Pago
    form_class = UpdatepagoForm
    success_url = reverse_lazy("core:list_Pago")

class DeletePago(generic.DeleteView):
    template_name = "core/bd/Pago/deletePago.html"
    model = Pago
    success_url = reverse_lazy("core:list_Pago")
    

## Cambios

# class ListCambios(generic.View):
#     template_name = "core/bd/Cambios/listCambios.html"
#     context = {}

#     def get(self, request, *args, **kwargs):
#         queryset = Cambios.objects.all()
#         self.context = {
#             "Cambios": queryset
#         }
#         return render(request, self.template_name,self.context)

# class CreateCambios(generic.CreateView):
#     template_name = "core/bd/Cambios/createCambios.html"
#     model = Cambios
#     form_class = CambiosForm
#     success_url = reverse_lazy("core:listCambios")

# class DetailCambios(generic.DetailView):
#     template_name = "core/bd/Cambios/detailCambios.html"
#     model = Cambios

# class UpdateCambios(generic.UpdateView):
#     template_name = "core/bd/Cambios/UpdateCambios.html"
#     model = Cambios
#     form_class = UpdateCambiosForm
#     success_url = reverse_lazy("core:listCambios")

# class DeleteCambios(generic.DeleteView):
#     template_name = "core/bd/Cambios/deleteCambios.html"
#     model = Cambios
#     success_url = reverse_lazy("core:listCambios")


class ListProveedor(generic.View):
    template_name = "core/bd/Proveedor/list_Proveedor.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Proveedor.objects.all()
        self.context = {
            "Proveedo": queryset  
        }
        return render(request, self.template_name, self.context)



class DetailProveedor(generic.DetailView):
    template_name = "core/bd/Proveedor/detailProveedor.html"
    model = Proveedor


class createProveedor(generic.CreateView):
    template_name = "core/bd/Proveedor/createProveedor.html"
    model = Proveedor
    form_class = proveedorForm
    success_url = reverse_lazy("core:List_Proveedor")


class UpdateProveedor(generic.UpdateView):
    template_name = "core/bd/Proveedor/UpdateProveedor.html"
    model = Proveedor
    form_class = UpdateproveedorForm
    success_url = reverse_lazy("core:List_Proveedor")

class DeleteProveedor(generic.DeleteView):
    template_name = "core/bd/Proveedor/deleteProveedor.html"
    model = Proveedor
    success_url = reverse_lazy("core:List_Proveedor")




#compra
class ListCompra(generic.View):
    template_name = "core/bd/compra/listcompra.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Compra.objects.all()
        self.context = {
            "Compra": queryset
        }
        return render(request, self.template_name,self.context)
    

from decimal import Decimal  # Asegúrate de importar Decimal

from django.http import HttpResponseRedirect


class CreateCompra(generic.CreateView):
    template_name = "core/bd/compra/create_compra.html"
    model = Compra
    form_class = CompraForm
    success_url = reverse_lazy("core:list_compra")

    def form_valid(self, form):
        response = super().form_valid(form)

        for material in form.cleaned_data['materiales']:
            cantidad_key = f'cantidad_{material.codigo}'
            cantidad = form.cleaned_data.get(cantidad_key, 0)

            if cantidad is not None and cantidad != "":
                importe = float(material.preciocompra) * float(cantidad)

                MaterialCompra.objects.create(
                    material=material,
                    compra=self.object,
                    cantidad=cantidad,
                    importe=Decimal(importe),
                )

        return response







class DetailCompra(generic.DetailView):
    template_name = "core/bd/compra/detailcompra.html"
    model = Compra

class UpdateCompra(generic.UpdateView):
    template_name = "core/bd/compra/Updatecompra.html"
    model = Compra
    form_class = UpdateCompraForm
    success_url = reverse_lazy("core:list_compra")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        compra = self.get_object()
        pedmat_entries = MaterialCompra.objects.filter(compra=compra)
        context["pedmat_entries"] = pedmat_entries
        return context

    def form_valid(self, form):
        response = super().form_valid(form)

        compra = self.object
        materiales = form.cleaned_data.get("materiales", [])

        for material in materiales:
            material_compra, created = MaterialCompra.objects.get_or_create(compra=compra, material=material)
            # Si quieres mantener la lógica de cantidad, ajusta esta línea según tus necesidades
            material_compra.cantidad = 1
            material_compra.save()

        return response



    
    
class DeleteCompra(generic.DeleteView):
    template_name = "core/bd/compra/deletecompra.html"
    model = Compra
    success_url = reverse_lazy("core:list_compra")

    def delete(self, request, *args, **kwargs):
        # Obtener el objeto Compra que se va a eliminar
        compra = self.get_object()

        # Obtener y eliminar manualmente todos los registros en la tabla PedMat que tienen el código de compra
        pedmat_entries = PedMat.objects.filter(compra=compra)
        pedmat_entries.delete()

        # Luego, llamar al método delete de la clase base para eliminar el compra
        return super().delete(request, *args, **kwargs)

