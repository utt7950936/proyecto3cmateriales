-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-12-2023 a las 17:28:15
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `materiales.sql`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen`
--

CREATE TABLE `almacen` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `nomCalle` varchar(50) NOT NULL,
  `num_calle` varchar(30) NOT NULL,
  `colonia` varchar(30) NOT NULL,
  `codigo_postal` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `almacen`
--

INSERT INTO `almacen` (`codigo`, `nombre`, `nomCalle`, `num_calle`, `colonia`, `codigo_postal`) VALUES
(1, 'RevoConstruccion', 'Avenida Revolucion', '123', 'Zona Centro', '22000'),
(2, 'PrimeraConstruccion', 'Calle Primera', '546', 'Playas de Tijuana', '22320'),
(3, 'SanConstruccion', 'Boulevard Sanchez', '789', 'Otay', '22440'),
(4, 'Heroes de la Construccion', 'Paseo de los Heroes', '951', 'Zona Este', '22600');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambios`
--

CREATE TABLE `cambios` (
  `codigo` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `material_devuelto` int(11) DEFAULT NULL,
  `material_cambiado` int(11) DEFAULT NULL,
  `pedido` int(11) NOT NULL,
  `almacen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cambios`
--

INSERT INTO `cambios` (`codigo`, `fecha`, `descripcion`, `material_devuelto`, `material_cambiado`, `pedido`, `almacen`) VALUES
(1, '2023-01-01', 'Cambio de materiales para pedido 1', 1, 27, 1, 1),
(2, '2023-01-02', 'Cambio de materiales para pedido 2', 4, 30, 2, 2),
(3, '2023-01-03', 'Cambio de materiales para pedido 3', 7, 33, 3, 3),
(4, '2023-01-04', 'Cambio de materiales para pedido 4', 10, 36, 4, 4),
(5, '2023-01-05', 'Cambio de materiales para pedido 5', 13, 29, 5, 1),
(6, '2023-01-06', 'Cambio de materiales para pedido 6', 16, 32, 6, 2),
(7, '2023-01-07', 'Cambio de materiales para pedido 7', 19, 35, 7, 3),
(8, '2023-01-08', 'Cambio de materiales para pedido 8', 22, 28, 8, 4),
(9, '2023-01-09', 'Cambio de materiales para pedido 9', 25, 31, 9, 1),
(10, '2023-01-10', 'Cambio de materiales para pedido 10', 28, 34, 10, 2),
(11, '2023-01-01', 'Cambio de materiales para pedido 1', 1, 27, 1, 1),
(12, '2023-01-02', 'Cambio de materiales para pedido 2', 4, 30, 2, 2),
(13, '2023-01-03', 'Cambio de materiales para pedido 3', 7, 33, 3, 3),
(14, '2023-01-04', 'Cambio de materiales para pedido 4', 10, 36, 4, 4),
(15, '2023-01-05', 'Cambio de materiales para pedido 5', 13, 29, 5, 1),
(16, '2023-01-06', 'Cambio de materiales para pedido 6', 16, 32, 6, 2),
(17, '2023-01-07', 'Cambio de materiales para pedido 7', 19, 35, 7, 3),
(18, '2023-01-08', 'Cambio de materiales para pedido 8', 22, 28, 8, 4),
(19, '2023-01-09', 'Cambio de materiales para pedido 9', 25, 31, 9, 1),
(20, '2023-01-10', 'Cambio de materiales para pedido 10', 28, 34, 10, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `camb_material`
--

CREATE TABLE `camb_material` (
  `cambio` int(11) NOT NULL,
  `material_devuelta` int(11) DEFAULT NULL,
  `material_cambiada` int(11) DEFAULT NULL,
  `cantidad_cambiado` int(11) DEFAULT NULL,
  `cantidad_devuelto` int(11) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `camb_material`
--

INSERT INTO `camb_material` (`cambio`, `material_devuelta`, `material_cambiada`, `cantidad_cambiado`, `cantidad_devuelto`, `importe`) VALUES
(1, 1, 27, 2, 1, 80.00),
(2, 2, 28, 1, 1, 30.00),
(3, 3, 29, 3, 2, 150.00),
(4, 4, 30, 4, 1, 200.00),
(5, 5, 31, 2, 1, 60.00),
(6, 6, 32, 1, 1, 25.00),
(7, 7, 33, 5, 2, 100.00),
(8, 8, 34, 2, 1, 20.00),
(9, 9, 35, 3, 2, 15.00);

--
-- Disparadores `camb_material`
--
DELIMITER $$
CREATE TRIGGER `actualizar_mat_alma` AFTER INSERT ON `camb_material` FOR EACH ROW BEGIN
    
    DECLARE importe_cambio DECIMAL(10, 2);
    SET importe_cambio = NEW.cantidad_cambiado * (SELECT PrecioVenta FROM material WHERE Codigo = NEW.material_cambiada);

    
    IF (
        (SELECT Importe FROM ped_mat WHERE Pedido = (SELECT pedido FROM cambios WHERE codigo = NEW.cambio) AND Material = NEW.material_devuelta) >= importe_cambio
    ) THEN
        
        UPDATE mat_alma
        SET cantidad = cantidad - NEW.cantidad_devuelto
        WHERE codigo = NEW.material_devuelta;

        
        UPDATE mat_alma
        SET cantidad = cantidad + NEW.cantidad_cambiado
        WHERE codigo = NEW.material_cambiada;
    ELSE
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'Error: El importe del material devuelto es menor al importe del cambio.';
    END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `actualizar_mat_alma_despues_de_un_cambio` AFTER INSERT ON `camb_material` FOR EACH ROW BEGIN
    
    DECLARE importe_cambio DECIMAL(10, 2);
    SET importe_cambio = NEW.cantidad_cambiado * (SELECT PrecioVenta FROM material WHERE Codigo = NEW.material_cambiada);

    
IF new.cantidad_cambiado > new.importe THEN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'La cantidad cambiada no puede ser mayor que el importe del material en el pedido.';
END IF;
        
        UPDATE mat_alma
        SET cantidad = cantidad - NEW.cantidad_devuelto
        WHERE codigo = NEW.material_devuelta;

        
        UPDATE mat_alma
        SET cantidad = cantidad + NEW.cantidad_cambiado
        WHERE codigo = NEW.material_cambiada;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `codigo` int(11) NOT NULL,
  `Nombre` varchar(80) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `correo` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`codigo`, `Nombre`, `telefono`, `correo`) VALUES
(1, 'Construcciones Velázquez S.A.', '664-123-4567', 'info@construccionesvelazquez.com'),
(2, 'Tecnología Innovadora MX', '664-987-6543', 'contacto@tecnoinnovamx.com'),
(3, 'Servicios Eléctricos Torres', '664-345-6789', 'contacto@torres-electricos.com'),
(4, 'Materiales de Calidad SA', '664-876-5432', 'ventas@materialesdecalidad.com'),
(5, 'Constructora Progresiva', '664-234-5678', 'info@constructoraprogresiva.com'),
(6, 'Soluciones Hidráulicas Gómez', '664-765-4321', 'contacto@solucionesgomez.com'),
(7, 'Energía Renovable y Asociados', '664-321-8765', 'info@energiarenovable.com'),
(8, 'Logística y Distribución MX', '664-654-3210', 'ventas@logisticaydistribucion.com'),
(9, 'Ingeniería Estructural Torres', '664-210-9876', 'contacto@ingenieriatorres.com'),
(10, 'Innovación en Construcción', '664-789-0123', 'info@innovacionconstruccion.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `Codigo` int(11) NOT NULL,
  `Descripcion` varchar(200) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `prodTotal` int(11) DEFAULT NULL,
  `Subtotal` decimal(10,2) DEFAULT NULL,
  `Iva` decimal(10,2) DEFAULT NULL,
  `Total` decimal(10,2) DEFAULT NULL,
  `Proveedor` int(11) NOT NULL,
  `almacen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`Codigo`, `Descripcion`, `Fecha`, `prodTotal`, `Subtotal`, `Iva`, `Total`, `Proveedor`, `almacen`) VALUES
(1, 'COMPRA DE CEMENTO Y HERRAMIENTAS', '2023-01-05', 73, 8629.31, 1380.69, 10010.00, 1, 2),
(2, 'LUCES Y MATERIAL ELECTRICO', '2023-02-10', 96, 7206.90, 1153.10, 8360.00, 2, 4),
(3, 'PINTURAS Y ACCESORIOS', '2023-03-15', 63, 3913.79, 626.21, 4540.00, 3, 1),
(4, 'COMPRA DE ESTRUCTURAS', '2023-04-20', 45, 2741.38, 438.62, 3180.00, 4, 3),
(5, 'HERRAMIENTAS DE CONTRUCCION', '2023-05-25', 54, 1862.07, 297.93, 2160.00, 1, 4),
(6, 'CABLEADO ELECTRICO', '2023-06-30', 33, 2810.34, 449.65, 3260.00, 2, 1),
(7, 'AZULEJOS Y MATERIAL PARA PISOS', '2023-08-05', 55, 5431.03, 868.96, 6300.00, 3, 3),
(8, 'COMPRA DE TUERCAS Y TORMILLOS', '2023-09-10', 35, 5603.45, 896.55, 6500.00, 4, 2),
(9, 'MATERIAL PARA FONTANERIA', '2023-10-15', 35, 3086.21, 493.79, 3580.00, 1, 1),
(10, 'LAMPARAS Y ACCESORIOS DE ILUMINACION', '2023-11-20', 35, 1443.97, 231.04, 1675.00, 2, 3),
(26, NULL, '2023-11-29', 23, 1336.21, 213.79, 1550.00, 5, 1),
(27, NULL, '2023-11-29', 34, 1034.48, 165.52, 1200.00, 4, 3),
(28, NULL, '2023-11-29', 33, 2724.14, 435.86, 3160.00, 2, 3),
(29, NULL, '2023-11-29', 34, 783.62, 125.38, 909.00, 5, 4),
(30, NULL, '2023-11-29', 54, 1491.38, 238.62, 1730.00, 4, 1),
(31, 'Compra de prueba', '2023-11-21', 16, 375.86, 60.14, 436.00, 1, 1);

--
-- Disparadores `compra`
--
DELIMITER $$
CREATE TRIGGER `before_insert_compra` BEFORE INSERT ON `compra` FOR EACH ROW BEGIN
    SET NEW.prodTotal = 0;
    SET NEW.Subtotal = 0.00;
    SET NEW.Iva = 0.00;
    SET NEW.Total = 0.00;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar`
--

CREATE TABLE `lugar` (
  `codigo` int(11) NOT NULL,
  `nomCalle` varchar(30) NOT NULL,
  `num_calle` varchar(30) NOT NULL,
  `colonia` varchar(30) NOT NULL,
  `codigo_postal` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `lugar`
--

INSERT INTO `lugar` (`codigo`, `nomCalle`, `num_calle`, `colonia`, `codigo_postal`) VALUES
(1, 'Avenida Revolución', '123', 'Zonaeste', '22000'),
(2, 'Calle Primera', '456', 'Playas de Tijuana', '22320'),
(3, 'Boulevard Sanchez', '789', 'Otay', '22440'),
(4, 'Calle Cuarta', '210', 'Hipódromo', '22110'),
(5, 'Paseo de los Héroes', '567', 'Zona Centro', '22600'),
(6, 'Avenida Constitución', '890', 'Guaycura', '22045'),
(7, 'Calle Octava', '342', 'Cacho', '22310'),
(8, 'Calle Quinta', '765', 'Libertad', '22020'),
(9, 'Boulevard Fundadores', '432', 'Moderna', '22410'),
(10, 'Calle Novena', '654', 'Chapultepec', '22610');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE `material` (
  `Codigo` int(11) NOT NULL,
  `Nombre` varchar(80) DEFAULT NULL,
  `Descripcion` text DEFAULT NULL,
  `precioCompra` decimal(10,2) DEFAULT NULL,
  `PrecioVenta` decimal(10,2) DEFAULT NULL,
  `Tipo` int(11) DEFAULT NULL,
  `imagen` varchar(500) DEFAULT 'nop.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`Codigo`, `Nombre`, `Descripcion`, `precioCompra`, `PrecioVenta`, `Tipo`, `imagen`) VALUES
(1, 'BOLSA DE CEMENTO', 'Bolsa de cemento Portland para construcción', 120.00, 132.00, 1, NULL),
(2, 'VARILLA DE ACERO', 'Varilla de acero corrugado de 3/8 pulgadas', 50.00, 55.00, 2, NULL),
(3, 'PINTURA INTERIOR', 'Pintura acrílica para interiores en color blanco', 200.00, 220.00, 3, NULL),
(4, 'TUBO DE PVC', 'Tubo de PVC para instalaciones de fontanería', 25.00, 27.50, 4, NULL),
(5, 'INTERRUPTOR ELECTRICO', 'Interruptor de luz para instalaciones eléctricas ', 40.00, 44.00, 5, NULL),
(6, 'ADHESIVO PARA AZULEJOS', 'Adhesivo especial para la instalación de azulejos', 60.00, 66.00, 3, NULL),
(7, 'GRIFO DE LAVABO', 'Grifo monomando para lavabo con acabado cromado', 240.00, 264.00, 4, NULL),
(8, 'CABLE ELECTRICO', 'Cable eléctrico THW calibre 12 para instalaciones', 15.00, 16.50, 5, NULL),
(9, 'LLAVE AJUSTABLE', 'Llave ajustable de 10 pulgadas para trabajos generales', 100.00, 110.00, 6, NULL),
(10, 'TUBERIA DE HIERRRO GALVANIZADO', 'Tubería de hierro galvanizado de 1 pulgada', 30.00, 33.00, 2, NULL),
(11, 'AZUELJO PARA BANO', 'Azulejo cerámico para revestimiento de baños', 45.00, 49.50, 3, NULL),
(12, 'CINTA TEFLON', 'Cinta de teflón para sellar roscas en instalaciones', 4.00, 4.40, 4, NULL),
(13, 'FOCO LED', 'Foco LED de 9W para iluminación eficiente', 25.00, 27.50, 5, NULL),
(14, 'BROCHA DE PINTURA', 'Brocha de cerdas naturales para aplicar pintura ', 20.00, 22.00, 3, NULL),
(15, 'LLAVE INGLESA', 'Llave inglesa de 8 pulgadas para trabajos mecánicos', 60.00, 66.00, 6, NULL),
(16, 'GRANO DE ARENA', 'Saco de arena de grano fino para trabajos de construcción', 40.00, 44.00, 1, NULL),
(17, 'TUBERIA DE CPVC', 'Tubería de CPVC para sistemas de fontanería', 30.00, 33.00, 4, NULL),
(18, 'INTERRUPTOR DIFERENCIAL', 'Interruptor diferencial para protección eléctrica', 100.00, 110.00, 5, NULL),
(19, 'ESMERILADORA ANGULAR', 'Esmeriladora angular de 4.5 pulgadas para cortes', 240.00, 264.00, 6, NULL),
(20, 'CLAVO DE ACERO', 'Clavo de acero para construcción de 3 pulgadas', 8.00, 8.80, 2, NULL),
(21, 'SACO DE MORTERO', 'Saco de mortero premezclado para albañilería', 150.00, 165.00, 1, NULL),
(22, 'TUVO GALVANIZADO', 'Tubo de hierro galvanizado de 2 pulgadas', 50.00, 55.00, 2, NULL),
(23, 'PISO CERAMICO', 'Piso cerámico para espacios interiores', 100.00, 110.00, 3, NULL),
(24, 'SIFON PARA FREGADERO', 'Sifón de PVC para desagüe de fregadero', 20.00, 22.00, 4, NULL),
(25, 'CABLE ELECTRICO THHN', 'Cable eléctrico THHN calibre 10 para instalaciones', 20.00, 22.00, 5, NULL),
(27, 'CERÁMICA PARA BAÑO', 'Azulejos cerámicos para revestimiento de baños', 30.00, 33.00, 3, 'nop.png'),
(28, 'TUBERÍA DE PVC PARA FONTANERÍA', 'Tubería de PVC resistente para instalaciones de fontanería', 10.00, 11.00, 4, 'nop.png'),
(29, 'FIBRA DE VIDRIO', 'Material ligero y resistente para reforzar estructuras', 45.00, 49.50, 2, 'nop.png'),
(30, 'CEMENTO RÁPIDO', 'Cemento de fraguado rápido para trabajos rápidos', 180.00, 198.00, 1, 'nop.png'),
(31, 'TELA ASFÁLTICA', 'Material impermeabilizante para techos', 25.00, 27.50, 3, 'nop.png'),
(32, 'MORTERO PREMEZCLADO', 'Mezcla de cemento y arena para albañilería', 40.00, 44.00, 1, 'nop.png'),
(33, 'GRANOS DE GRAVA', 'Grava para mezclas de construcción', 15.00, 16.50, 1, 'nop.png'),
(34, 'TUBOS DE COBRE', 'Tubos de cobre para sistemas de fontanería', 8.00, 8.80, 4, 'nop.png'),
(35, 'BLOQUES DE CONCRETO', 'Bloques prefabricados para construcción', 2.00, 2.20, 1, 'nop.png'),
(36, 'AISLANTE TÉRMICO', 'Material para aislar térmicamente estructuras', 50.00, 55.00, 2, 'nop.png');

--
-- Disparadores `material`
--
DELIMITER $$
CREATE TRIGGER `before_insert_material` BEFORE INSERT ON `material` FOR EACH ROW BEGIN
    
    SET NEW.PrecioVenta = NEW.precioCompra * (1 + 0.10); 
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material_compra`
--

CREATE TABLE `material_compra` (
  `material` int(11) NOT NULL,
  `compra` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `material_compra`
--

INSERT INTO `material_compra` (`material`, `compra`, `cantidad`, `importe`) VALUES
(1, 1, 15, 2250.00),
(1, 26, 10, 1200.00),
(2, 1, 20, 1800.00),
(3, 1, 10, 2500.00),
(3, 8, 10, 2500.00),
(4, 1, 5, 500.00),
(5, 2, 12, 960.00),
(5, 3, 20, 1200.00),
(6, 2, 8, 640.00),
(6, 31, 2, 120.00),
(7, 2, 15, 1500.00),
(7, 9, 15, 1500.00),
(8, 2, 8, 960.00),
(9, 2, 10, 1200.00),
(9, 9, 12, 1440.00),
(10, 2, 16, 640.00),
(10, 26, 5, 150.00),
(10, 29, 25, 750.00),
(10, 31, 10, 300.00),
(11, 3, 20, 1200.00),
(11, 29, 3, 135.00),
(12, 4, 12, 120.00),
(12, 10, 10, 100.00),
(12, 29, 6, 24.00),
(12, 31, 4, 16.00),
(13, 5, 10, 300.00),
(13, 26, 8, 200.00),
(13, 30, 18, 450.00),
(14, 5, 8, 320.00),
(14, 27, 15, 300.00),
(14, 30, 22, 440.00),
(15, 5, 10, 400.00),
(15, 27, 7, 420.00),
(15, 30, 14, 840.00),
(16, 5, 10, 500.00),
(16, 27, 12, 480.00),
(17, 6, 10, 800.00),
(17, 28, 20, 600.00),
(18, 6, 8, 960.00),
(18, 28, 4, 400.00),
(19, 6, 10, 1000.00),
(19, 28, 9, 2160.00),
(20, 7, 20, 2000.00),
(20, 8, 10, 2500.00),
(21, 7, 15, 2700.00),
(22, 7, 8, 640.00),
(23, 4, 15, 900.00),
(25, 10, 15, 375.00),
(27, 1, 8, 960.00),
(27, 6, 5, 500.00),
(28, 1, 5, 500.00),
(28, 7, 12, 960.00),
(29, 1, 10, 1500.00),
(29, 8, 15, 1500.00),
(30, 2, 12, 960.00),
(30, 9, 8, 640.00),
(31, 2, 15, 1500.00),
(31, 10, 10, 1200.00),
(32, 3, 8, 640.00),
(33, 3, 15, 1500.00),
(34, 4, 8, 960.00),
(35, 4, 10, 1200.00),
(36, 5, 16, 640.00);

--
-- Disparadores `material_compra`
--
DELIMITER $$
CREATE TRIGGER `actualizar_stock_en_compra_almacen` AFTER INSERT ON `material_compra` FOR EACH ROW BEGIN
    DECLARE material_stock INT;
    DECLARE material_cantidad INT;

    
    SELECT stock INTO material_stock
    FROM mat_alma
    WHERE almacen = (SELECT almacen FROM Compra WHERE Codigo = NEW.compra)
      AND material = NEW.material;

    
    SELECT cantidad INTO material_cantidad
    FROM material_compra
    WHERE compra = NEW.compra AND material = NEW.material;

    
    UPDATE mat_alma
    SET stock = material_stock + material_cantidad
    WHERE almacen = (SELECT almacen FROM Compra WHERE Codigo = NEW.compra)
      AND material = NEW.material;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `calcularTotalesCompra` AFTER INSERT ON `material_compra` FOR EACH ROW BEGIN
   DECLARE cantidad_total INT;
    DECLARE subtotal_pedido DECIMAL(10, 2);
    DECLARE iva_pedido DECIMAL(10, 2);
    DECLARE total_pedido DECIMAL(10, 2);
    DECLARE sub_total DECIMAL(10, 2); 

    
    SELECT SUM(cantidad) INTO cantidad_total
    FROM material_compra
    WHERE compra = NEW.compra;

    
    SELECT SUM(importe) INTO subtotal_pedido
    FROM material_compra
    WHERE compra = NEW.compra;


SET sub_total = subtotal_pedido / 1.16;
    SET iva_pedido = sub_total * 0.16;

    
    SET total_pedido = subtotal_pedido;

    
    UPDATE Compra
    SET prodTotal = cantidad_total,
        Subtotal = sub_total,
        Iva = iva_pedido,
        Total = total_pedido
    WHERE Codigo = NEW.compra;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `calcular_importe_en_compra` BEFORE INSERT ON `material_compra` FOR EACH ROW BEGIN
    DECLARE precio_compra DECIMAL(10, 2);
    
    
    SELECT precioCompra INTO precio_compra
    FROM Material
    WHERE Codigo = NEW.material;
    
    
    SET NEW.importe = NEW.cantidad * precio_compra;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mat_alma`
--

CREATE TABLE `mat_alma` (
  `almacen` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mat_alma`
--

INSERT INTO `mat_alma` (`almacen`, `material`, `stock`) VALUES
(1, 1, 200),
(1, 2, 6),
(1, 3, 108),
(1, 5, 249),
(1, 6, 79),
(1, 7, 256),
(1, 8, 119),
(1, 10, 83),
(1, 11, 233),
(1, 12, 32),
(1, 13, 101),
(1, 14, 199),
(1, 15, 217),
(1, 16, 163),
(1, 17, 171),
(1, 18, 154),
(1, 19, 162),
(1, 20, 205),
(1, 21, 200),
(1, 22, 147),
(1, 23, 107),
(1, 24, 13),
(1, 25, 83),
(1, 26, 0),
(2, 1, 230),
(2, 2, 253),
(2, 3, 31),
(2, 4, 143),
(2, 5, 59),
(2, 6, -5),
(2, 7, 30),
(2, 8, 73),
(2, 9, 56),
(2, 10, 159),
(2, 11, 133),
(2, 12, 166),
(2, 13, 12),
(2, 14, 236),
(2, 15, 71),
(2, 16, 184),
(2, 17, 166),
(2, 18, 131),
(2, 19, 95),
(2, 20, 259),
(2, 21, 204),
(2, 22, 28),
(2, 23, 112),
(2, 24, 11),
(2, 25, 202),
(3, 1, 245),
(3, 2, 150),
(3, 3, 147),
(3, 4, 220),
(3, 5, 43),
(3, 6, 230),
(3, 7, 98),
(3, 8, 39),
(3, 9, 222),
(3, 10, 146),
(3, 11, 196),
(3, 12, 65),
(3, 13, 162),
(3, 14, 164),
(3, 15, 117),
(3, 16, 232),
(3, 17, 50),
(3, 18, 37),
(3, 19, 109),
(3, 20, 254),
(3, 21, 135),
(3, 22, 198),
(3, 23, 199),
(3, 24, 12),
(3, 25, 164),
(4, 1, 238),
(4, 2, 146),
(4, 3, 235),
(4, 4, 30),
(4, 5, 40),
(4, 6, 54),
(4, 7, 262),
(4, 8, 22),
(4, 9, 227),
(4, 10, 38),
(4, 11, 112),
(4, 12, 156),
(4, 13, 43),
(4, 14, 61),
(4, 15, 216),
(4, 16, 39),
(4, 17, 181),
(4, 18, 37),
(4, 19, 54),
(4, 20, 17),
(4, 21, 73),
(4, 22, 223),
(4, 23, 159),
(4, 24, 86),
(4, 25, 193);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `Codigo` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `num_pago` varchar(20) DEFAULT NULL,
  `montoPago` decimal(10,2) DEFAULT NULL,
  `concepto` varchar(100) DEFAULT NULL,
  `saldo` decimal(10,2) DEFAULT NULL,
  `pedido` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`Codigo`, `fecha`, `num_pago`, `montoPago`, `concepto`, `saldo`, `pedido`) VALUES
(1, '2023-01-15', '1', 1500.00, 'Pago inicial', 1041.00, 1),
(2, '2023-02-01', '2', 300.00, 'Pago parcial', 741.00, 1),
(3, '2023-02-20', '1', 500.00, 'Pago inicial', 564.80, 2),
(4, '2023-03-10', '2', 250.00, 'Pago parcial', 314.80, 2),
(5, '2023-01-02', '1', 1200.00, 'Pago inicial', 1673.75, 3),
(6, '2023-01-01', '1', 300.00, 'Pago inicial', 3378.40, 4),
(7, '2023-01-02', '2', 500.00, 'Pago parcial', 2878.40, 4),
(8, '2023-01-20', '1', 500.00, 'Pago inicial', 661.60, 5),
(9, '2023-01-30', '2', 200.00, 'Pago parcial', 461.60, 5),
(10, '2023-01-01', '1', 800.00, 'Pago inicial', 1378.00, 6),
(103, '2023-03-01', '3', 120.00, 'Pago parcial', 621.00, 1),
(104, '2023-05-15', '4', 70.00, 'Pago parcial', 551.00, 1),
(105, '2023-09-01', '5', 87.00, 'Pago parcial', 464.00, 1),
(106, '2023-07-05', '3', 150.00, 'Pago parcial', 164.80, 2),
(109, '2023-01-10', '2', 67.00, 'Pago parcial', 1606.75, 3),
(110, '2023-01-25', '3', 89.00, 'Pago parcial', 1517.75, 3),
(111, '2023-02-10', '4', 123.00, 'Pago parcial', 1394.75, 3),
(112, '2023-01-15', '3', 10.00, 'Pago parcial', 2868.40, 4),
(113, '2023-01-30', '4', 20.00, 'Pago parcial', 2848.40, 4),
(114, '2023-02-15', '5', 15.00, 'Pago parcial', 2833.40, 4),
(115, '2023-02-03', '3', 25.00, 'Pago parcial', 436.60, 5),
(116, '2023-02-04', '4', 120.00, 'Pago parcial', 316.60, 5),
(117, '2023-02-19', '5', 15.00, 'Pago parcial', 301.60, 5),
(118, '2023-01-25', '2', 30.00, 'Pago parcial', 1348.00, 6),
(119, '2023-02-09', '3', 10.00, 'Pago parcial', 1338.00, 6),
(120, '2023-02-24', '4', 5.00, 'Pago parcial', 1333.00, 6),
(121, '2023-02-01', '1', 15.00, 'Pago parcial', 1558.00, 7),
(122, '2023-02-16', '2', 25.00, 'Pago parcial', 1533.00, 7),
(123, '2023-03-02', '3', 200.00, 'Pago parcial', 1333.00, 7),
(124, '2023-02-06', '1', 30.00, 'Pago parcial', 2898.20, 8),
(125, '2023-02-21', '2', 10.00, 'Pago parcial', 2888.20, 8),
(126, '2023-03-07', '3', 100.00, 'Pago parcial', 2788.20, 8),
(127, '2023-02-16', '1', 20.00, 'Pago parcial', 2545.20, 9),
(128, '2023-02-26', '2', 20.00, 'Pago parcial', 2525.20, 9),
(129, '2023-03-12', '3', 147.00, 'Pago parcial', 2378.20, 9),
(130, '2023-02-16', '1', 90.00, 'Pago parcial', 1144.20, 10),
(131, '2023-03-02', '2', 200.00, 'Pago parcial', 944.20, 10),
(132, '2023-03-03', '3', 100.00, 'Pago parcial', 844.20, 10);

--
-- Disparadores `pago`
--
DELIMITER $$
CREATE TRIGGER `calcularPagoSaldo` BEFORE INSERT ON `pago` FOR EACH ROW BEGIN
    DECLARE total_pedido DECIMAL(10, 2);
    DECLARE total_pagado DECIMAL(10, 2);
    DECLARE nuevo_saldo DECIMAL(10, 2);
    DECLARE nuevo_numero_pago INT;


    
    SELECT totalInt INTO total_pedido
    FROM pedido
    WHERE codigo = NEW.pedido;

    
    SELECT COALESCE(SUM(montoPago), 0) INTO total_pagado
    FROM pago
    WHERE pedido = NEW.pedido;

    
    SET nuevo_saldo = total_pedido - (total_pagado + NEW.montoPago);
    SET nuevo_numero_pago = (SELECT COUNT(*) + 1 FROM pago WHERE pedido = NEW.pedido);

    
    SET NEW.saldo = nuevo_saldo;
    SET NEW.num_pago = nuevo_numero_pago;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `establecer_fecha_pago` BEFORE INSERT ON `pago` FOR EACH ROW BEGIN
    
    SET NEW.fecha = CURRENT_DATE;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cantProduct` int(11) DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `iva` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `totalInt` decimal(10,2) DEFAULT NULL,
  `almacen` int(11) DEFAULT NULL,
  `lugar` int(11) DEFAULT NULL,
  `cliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`codigo`, `descripcion`, `fecha`, `cantProduct`, `subtotal`, `iva`, `total`, `totalInt`, `almacen`, `lugar`, `cliente`) VALUES
(1, 'Pedido de Construcción', '2023-01-15', 30, 1991.38, 318.62, 2310.00, 2541.00, 2, 1, 1),
(2, 'Pedido de Material Eléctrico', '2023-02-20', 24, 834.48, 133.52, 968.00, 1064.80, 2, 2, 2),
(3, 'Pedido de Acabados para Remodelación', '2023-01-01', 55, 2252.16, 360.35, 2612.50, 2873.75, 3, 3, 3),
(4, 'Pedido de Herramientas de Construcción', '2023-01-01', 27, 2882.76, 461.24, 3344.00, 3678.40, 4, 4, 4),
(5, 'Pedido de Plomería', '2023-01-15', 35, 910.34, 145.65, 1056.00, 1161.60, 1, 5, 5),
(6, 'Pedido de Material Eléctrico', '2023-01-01', 26, 1706.90, 273.10, 1980.00, 2178.00, 2, 6, 6),
(7, 'Pedido de Ferretería', '2023-02-01', 35, 1232.76, 197.24, 1430.00, 1573.00, 3, 7, 7),
(8, 'Pedido de Acabados para Baño', '2023-02-06', 33, 2294.83, 367.17, 2662.00, 2928.20, 4, 8, 8),
(9, 'Pedido de Material de Construcción', '2023-02-15', 40, 2010.34, 321.65, 2332.00, 2565.20, 1, 9, 9),
(10, 'Pedido de Ferretería', '2023-02-15', 36, 967.24, 154.76, 1122.00, 1234.20, 2, 10, 10);

--
-- Disparadores `pedido`
--
DELIMITER $$
CREATE TRIGGER `initialize_pedido_fields` BEFORE INSERT ON `pedido` FOR EACH ROW BEGIN
    SET NEW.fecha = current_timestamp; 
    SET NEW.cantProduct = 0;
    SET NEW.subtotal = 0.00;
    SET NEW.iva = 0.00;
    SET NEW.total = 0.00;
    
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ped_mat`
--

CREATE TABLE `ped_mat` (
  `pedido` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `importe` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `ped_mat`
--

INSERT INTO `ped_mat` (`pedido`, `material`, `cantidad`, `importe`) VALUES
(1, 1, 10, 1320.00),
(1, 2, 15, 825.00),
(1, 27, 5, 165.00),
(2, 5, 8, 352.00),
(2, 6, 8, 528.00),
(2, 28, 8, 88.00),
(3, 5, 20, 880.00),
(3, 11, 20, 990.00),
(3, 29, 15, 742.50),
(4, 20, 5, 44.00),
(4, 23, 12, 1320.00),
(4, 30, 10, 1980.00),
(5, 11, 8, 396.00),
(5, 25, 15, 330.00),
(5, 31, 12, 330.00),
(6, 6, 8, 528.00),
(6, 9, 10, 1100.00),
(6, 32, 8, 352.00),
(7, 2, 15, 825.00),
(7, 16, 10, 440.00),
(7, 33, 10, 165.00),
(8, 18, 8, 880.00),
(8, 21, 10, 1650.00),
(8, 34, 15, 132.00),
(9, 3, 10, 2200.00),
(9, 20, 10, 88.00),
(9, 35, 20, 44.00),
(10, 10, 16, 528.00),
(10, 12, 10, 44.00),
(10, 36, 10, 550.00),
(13, 3, 2, 440.00),
(13, 14, 4, 88.00);

--
-- Disparadores `ped_mat`
--
DELIMITER $$
CREATE TRIGGER `calcular_totales_pedido` AFTER INSERT ON `ped_mat` FOR EACH ROW BEGIN
    DECLARE cantidad_total INT;
    DECLARE subtotal_pedido DECIMAL(10, 2);
    DECLARE iva_pedido DECIMAL(10, 2);
    DECLARE total_pedido DECIMAL(10, 2);
    DECLARE total_con_intereses DECIMAL(10, 2);
    DECLARE sub_total DECIMAL(10, 2);

    
    SELECT SUM(cantidad) INTO cantidad_total
    FROM ped_mat
    WHERE pedido = NEW.pedido;

    
    SELECT SUM(importe) INTO subtotal_pedido
    FROM ped_mat
    WHERE pedido = NEW.pedido;

    
    SET sub_total = subtotal_pedido / 1.16;
    SET iva_pedido = sub_total * 0.16;

    
    SET total_pedido = subtotal_pedido;

    
    SET total_con_intereses = total_pedido * 1.10;

    
    UPDATE pedido
    SET cantProduct = cantidad_total,
        subtotal = sub_total,
        iva = iva_pedido,
        total = total_pedido,
        totalInt = total_con_intereses
    WHERE codigo = NEW.pedido;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`codigo`, `nombre`, `correo`) VALUES
(1, 'Construrama Materiales Tijuana S.A. de C.V.', 'compraenlinea@construrama.com'),
(2, 'Productos de importacion de Tijuana SA de CV', 'petsacv@grupopetsa.com'),
(3, 'Los Velazquez', 'ferreteria50@hotmail.com'),
(4, 'FARA Industrial', 'operaciones@ferreteriafara.com'),
(5, 'Sumainer Santa Cruz', 'sumainersantacruz@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resenas`
--

CREATE TABLE `resenas` (
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(4000) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `resenas`
--

INSERT INTO `resenas` (`codigo`, `descripcion`, `fecha`, `cliente`) VALUES
(1, 'Excelente servicio, entregaron los materiales a tiempo.', '2023-05-10', 1),
(2, 'El personal fue muy amable y atento en todo momento.', '2023-08-21', 3),
(3, 'Hubo un pequeño problema con la entrega, pero se solucionó rápido.', '2023-09-12', 7),
(4, 'Muy satisfecho con la calidad de los materiales, definitivamente recomendaré.', '2023-11-24', 9),
(45, 'Los materiales de construcción que adquirimos, como los ladrillos y la madera, fueron de alta calidad. Sin duda, recomendamos esta tienda.', '2023-11-15', 1),
(46, 'La atención al cliente fue excepcional, nos ayudaron a encontrar los productos exactos que necesitábamos para nuestro proyecto de construcción.', '2023-11-16', 1),
(47, 'Encontramos todo el material eléctrico que necesitábamos a precios competitivos. ¡Muy satisfechos con la compra de cables y conexiones!', '2023-11-17', 2),
(48, 'La entrega fue rápida y los productos eran exactamente lo que esperábamos. Sin duda, volveremos a comprar aquí para nuestras necesidades eléctricas.', '2023-11-18', 2),
(49, 'Los azulejos y materiales para pisos que adquirimos superaron nuestras expectativas. ¡Gracias por el excelente servicio en la compra de materiales de construcción!', '2023-11-19', 3),
(50, 'Hubo un pequeño problema con la cantidad en la entrega, pero se resolvió de manera eficiente. Estamos contentos con la compra de azulejos y materiales para pisos.', '2023-11-20', 3),
(51, 'Las herramientas de construcción que compramos cumplen con los estándares de calidad que buscábamos. ¡Excelente servicio en la compra de herramientas!', '2023-11-21', 4),
(52, 'Nos ayudaron a elegir las herramientas adecuadas para nuestra obra. Estamos muy agradecidos por la asesoría profesional en la compra de herramientas de construcción.', '2023-11-22', 4),
(53, 'Los materiales de fontanería que adquirimos cumplen con nuestras expectativas. El proceso de compra fue fácil y la entrega fue rápida. Recomendamos esta tienda.', '2023-11-23', 5),
(54, 'El personal fue amable y nos proporcionaron toda la información que necesitábamos sobre los materiales de fontanería. Definitivamente volveremos a comprar aquí.', '2023-11-24', 5),
(55, 'Estamos satisfechos con la calidad de los materiales de construcción que compramos. La entrega fue puntual y el servicio al cliente fue excelente.', '2023-11-25', 6),
(56, 'La variedad de herramientas de construcción disponibles nos facilitó encontrar lo que necesitábamos. Muy contentos con la compra.', '2023-11-26', 6),
(57, 'Los materiales eléctricos que adquirimos cumplen con nuestras expectativas. La atención al cliente fue excepcional y nos guiaron en la selección de productos.', '2023-11-27', 7),
(58, 'La entrega fue rápida y los materiales eléctricos estaban bien empaquetados. Recomendamos esta tienda para comprar materiales eléctricos.', '2023-11-28', 7),
(59, 'Adquirimos azulejos y materiales para pisos para nuestra renovación. La calidad de los productos fue excelente, y el personal nos brindó un buen servicio.', '2023-11-29', 8),
(60, 'Hubo un pequeño problema con el pedido, pero se solucionó de manera eficiente. Apreciamos la atención al cliente en la compra de azulejos y materiales para pisos.', '2023-11-30', 8),
(61, 'Los materiales de construcción que compramos son de alta calidad. La entrega fue rápida y el servicio al cliente fue excelente. Recomendamos esta tienda.', '2023-12-01', 9),
(62, 'Nos ayudaron a encontrar los materiales de construcción adecuados para nuestro proyecto. Estamos satisfechos con la compra y el servicio recibido.', '2023-12-02', 9),
(63, 'Estamos contentos con la variedad de herramientas de construcción disponibles. El personal nos brindó asesoramiento útil en la selección de herramientas.', '2023-12-03', 10),
(64, 'La entrega de las herramientas de construcción fue rápida y eficiente. Recomendamos esta tienda para la compra de herramientas de construcción.', '2023-12-04', 10),
(65, 'La página web ofrece una amplia variedad de productos y es fácil de navegar. El proceso de compra en línea fue eficiente y sin complicaciones. Recomendamos la plataforma.', '2023-12-05', 1),
(66, 'La experiencia en la página fue positiva. Encontramos fácilmente los productos que necesitábamos y el proceso de compra fue rápido. Estamos satisfechos con el servicio en línea.', '2023-12-06', 2),
(67, 'La interfaz de la página es intuitiva y facilita la búsqueda de productos. El proceso de compra en línea fue sencillo y la entrega se realizó según lo programado. Recomendamos la plataforma.', '2023-12-07', 3),
(68, 'Utilizamos la página para comprar herramientas y quedamos satisfechos con la experiencia en línea. El proceso fue rápido y la información sobre los productos fue detallada y útil.', '2023-12-08', 4),
(69, 'La página web facilita la búsqueda de materiales de fontanería. La compra en línea fue segura y rápida. Estamos contentos con la experiencia general en la plataforma.', '2023-12-09', 5),
(70, 'Encontramos fácilmente los materiales de construcción que necesitábamos en la página. El proceso de compra en línea fue eficiente y la entrega se realizó sin problemas. Recomendamos la plataforma.', '2023-12-10', 6),
(71, 'La página ofrece información detallada sobre los productos eléctricos. La experiencia de compra en línea fue positiva y la entrega fue puntual. Estamos satisfechos con el servicio.', '2023-12-11', 7),
(72, 'La navegación en la página para elegir azulejos y materiales para pisos fue sencilla. El proceso de compra en línea fue rápido y seguro. Recomendamos la página para estos productos.', '2023-12-12', 8),
(73, 'La página web brinda opciones claras de filtrado, facilitando la búsqueda de materiales específicos. La experiencia de compra en línea fue satisfactoria y sin complicaciones.', '2023-12-13', 9),
(74, 'La variedad de herramientas de construcción en la página es impresionante. El proceso de compra en línea fue sencillo y la entrega fue puntual. Recomendamos la plataforma.', '2023-12-14', 10);

--
-- Disparadores `resenas`
--
DELIMITER $$
CREATE TRIGGER `before_insert_resena` BEFORE INSERT ON `resenas` FOR EACH ROW BEGIN
    
    SET NEW.fecha = CURDATE();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`codigo`, `nombre`, `descripcion`) VALUES
(1, 'CEMENTO', 'Material de construcción para unir elementos, brin'),
(2, 'ACEROS', 'Material resistente utilizado en construcción y fa'),
(3, 'ACABADOS', 'Materiales decorativos como pinturas, azulejos y c'),
(4, 'PLOMERIA', 'Materiales relacionados con instalaciones de agua '),
(5, 'METERIAL ELECTRICO', 'Componentes y dispositivos utilizados en instalaci'),
(6, 'HERRAMIENTAS CONTRUCCION', 'Instrumentos y utensilios utilizados en proyectos '),
(7, 'FERRETERIA', 'Materiales y herramientas para construcción y repa');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `cambios`
--
ALTER TABLE `cambios`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `material_devuelto` (`material_devuelto`),
  ADD KEY `material_cambiado` (`material_cambiado`),
  ADD KEY `pedido` (`pedido`),
  ADD KEY `almacen` (`almacen`);

--
-- Indices de la tabla `camb_material`
--
ALTER TABLE `camb_material`
  ADD PRIMARY KEY (`cambio`),
  ADD KEY `material_devuelta` (`material_devuelta`),
  ADD KEY `material_cambiada` (`material_cambiada`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Proveedor` (`Proveedor`),
  ADD KEY `almacen` (`almacen`);

--
-- Indices de la tabla `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Tipo` (`Tipo`);

--
-- Indices de la tabla `material_compra`
--
ALTER TABLE `material_compra`
  ADD PRIMARY KEY (`material`,`compra`),
  ADD KEY `compra` (`compra`);

--
-- Indices de la tabla `mat_alma`
--
ALTER TABLE `mat_alma`
  ADD PRIMARY KEY (`almacen`,`material`),
  ADD KEY `material` (`material`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `pedido` (`pedido`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `almacen` (`almacen`),
  ADD KEY `lugar` (`lugar`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `ped_mat`
--
ALTER TABLE `ped_mat`
  ADD PRIMARY KEY (`pedido`,`material`),
  ADD KEY `material` (`material`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `resenas`
--
ALTER TABLE `resenas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`codigo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacen`
--
ALTER TABLE `almacen`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cambios`
--
ALTER TABLE `cambios`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `lugar`
--
ALTER TABLE `lugar`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `material`
--
ALTER TABLE `material`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `resenas`
--
ALTER TABLE `resenas`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cambios`
--
ALTER TABLE `cambios`
  ADD CONSTRAINT `cambios_ibfk_1` FOREIGN KEY (`material_devuelto`) REFERENCES `material` (`Codigo`),
  ADD CONSTRAINT `cambios_ibfk_2` FOREIGN KEY (`material_cambiado`) REFERENCES `material` (`Codigo`),
  ADD CONSTRAINT `cambios_ibfk_3` FOREIGN KEY (`pedido`) REFERENCES `pedido` (`codigo`),
  ADD CONSTRAINT `cambios_ibfk_4` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`);

--
-- Filtros para la tabla `camb_material`
--
ALTER TABLE `camb_material`
  ADD CONSTRAINT `camb_material_ibfk_1` FOREIGN KEY (`cambio`) REFERENCES `cambios` (`codigo`),
  ADD CONSTRAINT `camb_material_ibfk_2` FOREIGN KEY (`material_devuelta`) REFERENCES `material` (`Codigo`),
  ADD CONSTRAINT `camb_material_ibfk_3` FOREIGN KEY (`material_cambiada`) REFERENCES `material` (`Codigo`);

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `compra_ibfk_1` FOREIGN KEY (`Proveedor`) REFERENCES `proveedor` (`codigo`),
  ADD CONSTRAINT `compra_ibfk_2` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`);

--
-- Filtros para la tabla `material`
--
ALTER TABLE `material`
  ADD CONSTRAINT `material_ibfk_1` FOREIGN KEY (`Tipo`) REFERENCES `tipos` (`codigo`);

--
-- Filtros para la tabla `material_compra`
--
ALTER TABLE `material_compra`
  ADD CONSTRAINT `material_compra_ibfk_1` FOREIGN KEY (`material`) REFERENCES `material` (`Codigo`),
  ADD CONSTRAINT `material_compra_ibfk_2` FOREIGN KEY (`compra`) REFERENCES `compra` (`Codigo`);

--
-- Filtros para la tabla `mat_alma`
--
ALTER TABLE `mat_alma`
  ADD CONSTRAINT `mat_alma_ibfk_1` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`),
  ADD CONSTRAINT `mat_alma_ibfk_2` FOREIGN KEY (`material`) REFERENCES `material` (`Codigo`);

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`pedido`) REFERENCES `pedido` (`codigo`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`),
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`lugar`) REFERENCES `lugar` (`codigo`),
  ADD CONSTRAINT `pedido_ibfk_3` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`codigo`);

--
-- Filtros para la tabla `ped_mat`
--
ALTER TABLE `ped_mat`
  ADD CONSTRAINT `ped_mat_ibfk_1` FOREIGN KEY (`pedido`) REFERENCES `pedido` (`codigo`),
  ADD CONSTRAINT `ped_mat_ibfk_2` FOREIGN KEY (`material`) REFERENCES `material` (`Codigo`);

--
-- Filtros para la tabla `resenas`
--
ALTER TABLE `resenas`
  ADD CONSTRAINT `resenas_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
