from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.forms.models import BaseModelForm
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views import generic

from .forms import LoginForm, SignUpForm, UserProfileForm
from .models import UserProfile

# Create your views here.

class Index(generic.View):
    template_name = "home/index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Gerardo Aguilar",
            "lista": [1, 2, 3],
            

        }
        return render(request, self.template_name, self.context)
    

    


    
    
class LogIn(generic.View):
    template_name = "home/Login.html"
    context = {}
    form = LoginForm()

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Gerardo Aguilar Prueba login",
            "lista": [1, 2, 3, 4, 5],
            "form": self.form,
        }
        return render(request, self.template_name, self.context)

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")  # Redirect to a different URL after successful login
        else:
            return redirect("/")

        
        
        
        
        
        
class LogOut(generic.View):
    template_name = "home/logout.html"
    context = {}
    def get(self, request):
        logout(request)
        return redirect("/")
    



# class SignUp(generic.CreateView):
#         template_name = "home/SignUp.html"
#         form_class = SignUpForm
#         success_url = reverse_lazy("home:login")

#         def form_valid(self, form):
#             form.save()
#         username = form.cleaned_data.get("username")
#         password1 = form.cleaned_data.get("password1")
#         user = authenticate(self.request, username=username, password=password1)
#         if user is not None:
#             login(self.request, user)
#             return redirect("home/Login.html")

class SignUp(generic.CreateView):
    template_name = "home/SignUp.html"
    form_class = SignUpForm
    success_url = reverse_lazy("home:login")

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get("username")
        password1 = form.cleaned_data.get("password1")
        user = authenticate(self.request, username=username, password=password1)
        if user is not None:
            login(self.request, user)
            return redirect("home/Login.html")
        return super().form_valid(form)



class UserProfile(LoginRequiredMixin, generic.CreateView):
    template_name = "home/user_profile.html"
    model = UserProfile
    form_class = UserProfileForm
    success_url = reverse_lazy("home:user_profile")
    login_url = "/"



# No le muevas hacia abajo xd

class About(generic.View):
    template_name = "home/about.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Ray Parra"
        }
        return render(request, self.template_name, self.context)


class Identity(generic.View):
    template_name = "home/identity.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Ray Parra"
        }
        return render(request, self.template_name, self.context)


class Contacto(generic.View):
    template_name = "home/contacto.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Ray Parra"
        }
        return render(request, self.template_name, self.context)
    
    
