-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 23-11-2023 a las 18:59:37
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `materiales3c`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen`
--

CREATE TABLE `almacen` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `nomCalle` varchar(50) NOT NULL,
  `num_calle` varchar(30) NOT NULL,
  `colonia` varchar(30) NOT NULL,
  `codigo_postal` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `almacen`
--

INSERT INTO `almacen` (`codigo`, `nombre`, `nomCalle`, `num_calle`, `colonia`, `codigo_postal`) VALUES
(1, 'Almacén Central', 'Calle de la Construcción', '123', 'Industrial', '54321'),
(2, 'Depósito de Materiales', 'Avenida Obras', '456', 'Oeste', '67890'),
(3, 'Almacén de Innovación', 'Calle Principal', '789', 'Centro', '12345'),
(4, 'Suministros Rápidos', 'Avenida Logística', '101', 'Este', '98765'),
(5, 'Depósito de Herramientas', 'Calle del Progreso', '202', 'Norte', '43210'),
(6, 'Almacén Central', 'Calle de la Construcción', '123', 'Industrial', '54321'),
(7, 'Depósito de Materiales', 'Avenida Obras', '456', 'Oeste', '67890'),
(8, 'Almacén de Innovación', 'Calle Principal', '789', 'Centro', '12345'),
(9, 'Suministros Rápidos', 'Avenida Logística', '101', 'Este', '98765'),
(10, 'Depósito de Herramientas', 'Calle del Progreso', '202', 'Norte', '43210'),
(11, 'Almacén Central', 'Calle de la Construcción', '123', 'Industrial', '54321'),
(12, 'Depósito de Materiales', 'Avenida Obras', '456', 'Oeste', '67890'),
(13, 'Almacén de Innovación', 'Calle Principal', '789', 'Centro', '12345'),
(14, 'Suministros Rápidos', 'Avenida Logística', '101', 'Este', '98765'),
(15, 'Depósito de Herramientas', 'Calle del Progreso', '202', 'Norte', '43210'),
(16, 'Almacén Central', 'Calle de la Construcción', '123', 'Industrial', '54321'),
(17, 'Depósito de Materiales', 'Avenida Obras', '456', 'Oeste', '67890'),
(18, 'Almacén de Innovación', 'Calle Principal', '789', 'Centro', '12345'),
(19, 'Suministros Rápidos', 'Avenida Logística', '101', 'Este', '98765'),
(20, 'Depósito de Herramientas', 'Calle del Progreso', '202', 'Norte', '43210'),
(21, 'Almacén Central', 'Calle de la Construcción', '123', 'Industrial', '54321'),
(22, 'Depósito de Materiales', 'Avenida Obras', '456', 'Oeste', '67890'),
(23, 'Almacén de Innovación', 'Calle Principal', '789', 'Centro', '12345'),
(24, 'Suministros Rápidos', 'Avenida Logística', '101', 'Este', '98765'),
(25, 'Depósito de Herramientas', 'Calle del Progreso', '202', 'Norte', '43210');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambios`
--

CREATE TABLE `cambios` (
  `codigo` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `pedido` int(11) NOT NULL,
  `almacen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cambios`
--

INSERT INTO `cambios` (`codigo`, `fecha`, `descripcion`, `pedido`, `almacen`) VALUES
(6, '2023-11-21', 'Cambio de Material', 1, 1),
(7, '2023-11-22', 'Devolución de Productos', 2, 2),
(8, '2023-11-23', 'Cambio de Herramienta', 3, 3),
(9, '2023-11-24', 'Devolución de Materiales', 4, 4),
(10, '2023-11-25', 'Cambio de Tornillos', 5, 5),
(11, '2023-11-21', 'Cambio de Material', 1, 1),
(12, '2023-11-22', 'Devolución de Productos', 2, 2),
(13, '2023-11-23', 'Cambio de Herramienta', 3, 3),
(14, '2023-11-24', 'Devolución de Materiales', 4, 4),
(15, '2023-11-25', 'Cambio de Tornillos', 5, 5),
(16, '2023-11-21', 'Cambio de Material', 1, 1),
(17, '2023-11-22', 'Devolución de Productos', 2, 2),
(18, '2023-11-23', 'Cambio de Herramienta', 3, 3),
(19, '2023-11-24', 'Devolución de Materiales', 4, 4),
(20, '2023-11-25', 'Cambio de Tornillos', 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `camb_material`
--

CREATE TABLE `camb_material` (
  `cambio` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `cantidad_devuelta` int(11) DEFAULT NULL,
  `cantidad_cambiada` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `camb_material`
--

INSERT INTO `camb_material` (`cambio`, `material`, `cantidad_devuelta`, `cantidad_cambiada`) VALUES
(11, 1, 1, 1),
(11, 2, 2, 2),
(11, 3, 1, 1),
(11, 4, 2, 2),
(11, 5, 2, 2),
(12, 1, 2, 2),
(12, 2, 1, 1),
(12, 3, 1, 1),
(12, 4, 1, 1),
(12, 5, 1, 1),
(13, 1, 1, 1),
(13, 2, 1, 1),
(13, 3, 1, 1),
(13, 4, 1, 1),
(13, 5, 1, 1),
(14, 1, 2, 2),
(14, 2, 1, 1),
(14, 3, 2, 2),
(14, 4, 1, 1),
(14, 5, 1, 1),
(15, 1, 1, 1),
(15, 2, 1, 1),
(15, 3, 1, 1),
(15, 4, 2, 2),
(15, 5, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cliente`
--

CREATE TABLE `Cliente` (
  `codigo` int(11) NOT NULL,
  `Nombre` varchar(80) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `correo` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `Cliente`
--

INSERT INTO `Cliente` (`codigo`, `Nombre`, `telefono`, `correo`) VALUES
(1, 'Construcciones del Valle', '123456789', 'info@construccionesdelvalle.com'),
(2, 'Suministros y Materiales S.A.', '987654321', 'contacto@suministrosymateriales.com'),
(3, 'Arquitectura Moderna', '555111222', 'info@arquitecturamoderna.com'),
(4, 'Construcciones Ecológicas', '777888999', 'ventas@construccioneseconomicas.com'),
(5, 'Proyectos Innovadores', '333444555', 'info@proyectosinnovadores.com'),
(6, 'Construcciones del Valle', '123456789', 'info@construccionesdelvalle.com'),
(7, 'Suministros y Materiales S.A.', '987654321', 'contacto@suministrosymateriales.com'),
(8, 'Arquitectura Moderna', '555111222', 'info@arquitecturamoderna.com'),
(9, 'Construcciones Ecológicas', '777888999', 'ventas@construccioneseconomicas.com'),
(10, 'Proyectos Innovadores', '333444555', 'info@proyectosinnovadores.com'),
(11, 'Construcciones del Valle', '123456789', 'info@construccionesdelvalle.com'),
(12, 'Suministros y Materiales S.A.', '987654321', 'contacto@suministrosymateriales.com'),
(13, 'Arquitectura Moderna', '555111222', 'info@arquitecturamoderna.com'),
(14, 'Construcciones Ecológicas', '777888999', 'ventas@construccioneseconomicas.com'),
(15, 'Proyectos Innovadores', '333444555', 'info@proyectosinnovadores.com'),
(16, 'Construcciones del Valle', '123456789', 'info@construccionesdelvalle.com'),
(17, 'Suministros y Materiales S.A.', '987654321', 'contacto@suministrosymateriales.com'),
(18, 'Arquitectura Moderna', '555111222', 'info@arquitecturamoderna.com'),
(19, 'Construcciones Ecológicas', '777888999', 'ventas@construccioneseconomicas.com'),
(20, 'Proyectos Innovadores', '333444555', 'info@proyectosinnovadores.com'),
(21, 'Construcciones del Valle', '123456789', 'info@construccionesdelvalle.com'),
(22, 'Suministros y Materiales S.A.', '987654321', 'contacto@suministrosymateriales.com'),
(23, 'Arquitectura Moderna', '555111222', 'info@arquitecturamoderna.com'),
(24, 'Construcciones Ecológicas', '777888999', 'ventas@construccioneseconomicas.com'),
(25, 'Proyectos Innovadores', '333444555', 'info@proyectosinnovadores.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Compra`
--

CREATE TABLE `Compra` (
  `Codigo` int(11) NOT NULL,
  `Descripcion` varchar(200) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `prodTotal` int(11) DEFAULT NULL,
  `Subtotal` decimal(10,2) DEFAULT NULL,
  `Iva` decimal(5,2) DEFAULT NULL,
  `Total` decimal(10,2) DEFAULT NULL,
  `Proveedor` int(11) NOT NULL,
  `almacen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `Compra`
--

INSERT INTO `Compra` (`Codigo`, `Descripcion`, `Fecha`, `prodTotal`, `Subtotal`, `Iva`, `Total`, `Proveedor`, `almacen`) VALUES
(1, 'Compra de Materiales para Obra', '2023-11-21', 5, 300.50, 45.08, 345.58, 1, 1),
(2, 'Suministros de Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 2, 2),
(3, 'Compra de Herramientas', '2023-11-23', 4, 120.75, 18.11, 138.86, 4, 3),
(4, 'Materiales para Proyecto Innovador', '2023-11-24', 6, 200.00, 30.00, 230.00, 3, 4),
(5, 'Compra de Tornillos', '2023-11-25', 3, 7.50, 1.13, 8.63, 5, 5),
(6, 'Compra de Materiales para Obra', '2023-11-21', 5, 300.50, 45.08, 345.58, 1, 1),
(7, 'Suministros de Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 2, 2),
(8, 'Compra de Herramientas', '2023-11-23', 4, 120.75, 18.11, 138.86, 4, 3),
(9, 'Materiales para Proyecto Innovador', '2023-11-24', 6, 200.00, 30.00, 230.00, 3, 4),
(10, 'Compra de Tornillos', '2023-11-25', 3, 7.50, 1.13, 8.63, 5, 5),
(11, 'Compra de Materiales para Obra', '2023-11-21', 5, 300.50, 45.08, 345.58, 1, 1),
(12, 'Suministros de Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 2, 2),
(13, 'Compra de Herramientas', '2023-11-23', 4, 120.75, 18.11, 138.86, 4, 3),
(14, 'Materiales para Proyecto Innovador', '2023-11-24', 6, 200.00, 30.00, 230.00, 3, 4),
(15, 'Compra de Tornillos', '2023-11-25', 3, 7.50, 1.13, 8.63, 5, 5),
(16, 'Compra de Materiales para Obra', '2023-11-21', 5, 300.50, 45.08, 345.58, 1, 1),
(17, 'Suministros de Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 2, 2),
(18, 'Compra de Herramientas', '2023-11-23', 4, 120.75, 18.11, 138.86, 4, 3),
(19, 'Materiales para Proyecto Innovador', '2023-11-24', 6, 200.00, 30.00, 230.00, 3, 4),
(20, 'Compra de Tornillos', '2023-11-25', 3, 7.50, 1.13, 8.63, 5, 5),
(21, 'Compra de Materiales para Obra', '2023-11-21', 5, 300.50, 45.08, 345.58, 1, 1),
(22, 'Suministros de Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 2, 2),
(23, 'Compra de Herramientas', '2023-11-23', 4, 120.75, 18.11, 138.86, 4, 3),
(24, 'Materiales para Proyecto Innovador', '2023-11-24', 6, 200.00, 30.00, 230.00, 3, 4),
(25, 'Compra de Tornillos', '2023-11-25', 3, 7.50, 1.13, 8.63, 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar`
--

CREATE TABLE `lugar` (
  `codigo` int(11) NOT NULL,
  `num_calle` varchar(30) NOT NULL,
  `colonia` varchar(30) NOT NULL,
  `codigo_postal` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `lugar`
--

INSERT INTO `lugar` (`codigo`, `num_calle`, `colonia`, `codigo_postal`) VALUES
(1, '123', 'Colonia Centro', '54321'),
(2, '456', 'Colonia Oeste', '67890'),
(3, '789', 'Colonia Norte', '12345'),
(4, '101', 'Colonia Este', '98765'),
(5, '202', 'Colonia Sur', '43210'),
(6, '123', 'Colonia Centro', '54321'),
(7, '456', 'Colonia Oeste', '67890'),
(8, '789', 'Colonia Norte', '12345'),
(9, '101', 'Colonia Este', '98765'),
(10, '202', 'Colonia Sur', '43210'),
(11, '123', 'Colonia Centro', '54321'),
(12, '456', 'Colonia Oeste', '67890'),
(13, '789', 'Colonia Norte', '12345'),
(14, '101', 'Colonia Este', '98765'),
(15, '202', 'Colonia Sur', '43210'),
(16, '123', 'Colonia Centro', '54321'),
(17, '456', 'Colonia Oeste', '67890'),
(18, '789', 'Colonia Norte', '12345'),
(19, '101', 'Colonia Este', '98765'),
(20, '202', 'Colonia Sur', '43210'),
(21, '123', 'Colonia Centro', '54321'),
(22, '456', 'Colonia Oeste', '67890'),
(23, '789', 'Colonia Norte', '12345'),
(24, '101', 'Colonia Este', '98765'),
(25, '202', 'Colonia Sur', '43210');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Material`
--

CREATE TABLE `Material` (
  `Codigo` int(11) NOT NULL,
  `Nombre` varchar(80) DEFAULT NULL,
  `Descripcion` text DEFAULT NULL,
  `precioCompra` decimal(10,2) DEFAULT NULL,
  `PrecioVenta` decimal(10,2) DEFAULT NULL,
  `Tipo` int(11) DEFAULT NULL,
  `imagen` varchar(500) DEFAULT 'nop.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `Material`
--

INSERT INTO `Material` (`Codigo`, `Nombre`, `Descripcion`, `precioCompra`, `PrecioVenta`, `Tipo`, `imagen`) VALUES
(1, 'Cemento Reforzado', 'Bolsa de cemento de alta resistencia para proyectos robustos', 10.50, 15.99, 1, 'nop.png'),
(2, 'Vigas de Acero Estructural', 'Vigas de acero para construcción de estructuras sólidas', 50.75, 75.99, 2, 'nop.png'),
(3, 'Pintura Ecológica', 'Lata de pintura ecológica para interiores y exteriores', 20.25, 30.99, 3, 'nop.png'),
(4, 'Herramienta Multiusos', 'Kit de herramientas variadas para trabajos de construcción', 5.99, 9.99, 4, 'nop.png'),
(5, 'Tornillos para Madera', 'Caja de tornillos de alta calidad para proyectos de carpintería', 2.50, 4.99, 5, 'nop.png'),
(6, 'Cemento Reforzado', 'Bolsa de cemento de alta resistencia para proyectos robustos', 10.50, 15.99, 1, 'nop.png'),
(7, 'Vigas de Acero Estructural', 'Vigas de acero para construcción de estructuras sólidas', 50.75, 75.99, 2, 'nop.png'),
(8, 'Pintura Ecológica', 'Lata de pintura ecológica para interiores y exteriores', 20.25, 30.99, 3, 'nop.png'),
(9, 'Herramienta Multiusos', 'Kit de herramientas variadas para trabajos de construcción', 5.99, 9.99, 4, 'nop.png'),
(10, 'Tornillos para Madera', 'Caja de tornillos de alta calidad para proyectos de carpintería', 2.50, 4.99, 5, 'nop.png'),
(11, 'Cemento Reforzado', 'Bolsa de cemento de alta resistencia para proyectos robustos', 10.50, 15.99, 1, 'nop.png'),
(12, 'Vigas de Acero Estructural', 'Vigas de acero para construcción de estructuras sólidas', 50.75, 75.99, 2, 'nop.png'),
(13, 'Pintura Ecológica', 'Lata de pintura ecológica para interiores y exteriores', 20.25, 30.99, 3, 'nop.png'),
(14, 'Herramienta Multiusos', 'Kit de herramientas variadas para trabajos de construcción', 5.99, 9.99, 4, 'nop.png'),
(15, 'Tornillos para Madera', 'Caja de tornillos de alta calidad para proyectos de carpintería', 2.50, 4.99, 5, 'nop.png'),
(16, 'Cemento Reforzado', 'Bolsa de cemento de alta resistencia para proyectos robustos', 10.50, 15.99, 1, 'nop.png'),
(17, 'Vigas de Acero Estructural', 'Vigas de acero para construcción de estructuras sólidas', 50.75, 75.99, 2, 'nop.png'),
(18, 'Pintura Ecológica', 'Lata de pintura ecológica para interiores y exteriores', 20.25, 30.99, 3, 'nop.png'),
(19, 'Herramienta Multiusos', 'Kit de herramientas variadas para trabajos de construcción', 5.99, 9.99, 4, 'nop.png'),
(20, 'Tornillos para Madera', 'Caja de tornillos de alta calidad para proyectos de carpintería', 2.50, 4.99, 5, 'nop.png'),
(21, 'Cemento Reforzado', 'Bolsa de cemento de alta resistencia para proyectos robustos', 10.50, 15.99, 1, 'nop.png'),
(22, 'Vigas de Acero Estructural', 'Vigas de acero para construcción de estructuras sólidas', 50.75, 75.99, 2, 'nop.png'),
(23, 'Pintura Ecológica', 'Lata de pintura ecológica para interiores y exteriores', 20.25, 30.99, 3, 'nop.png'),
(24, 'Herramienta Multiusos', 'Kit de herramientas variadas para trabajos de construcción', 5.99, 9.99, 4, 'nop.png'),
(25, 'Tornillos para Madera', 'Caja de tornillos de alta calidad para proyectos de carpintería', 2.50, 4.99, 5, 'nop.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material_compra`
--

CREATE TABLE `material_compra` (
  `material` int(11) NOT NULL,
  `compra` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `material_compra`
--

INSERT INTO `material_compra` (`material`, `compra`, `cantidad`, `importe`) VALUES
(1, 1, 2, 21.00),
(2, 1, 3, 227.97),
(3, 1, 1, 30.99),
(4, 1, 4, 39.96),
(5, 1, 5, 24.95),
(1, 2, 2, 31.98),
(2, 2, 1, 75.99),
(3, 2, 4, 123.96),
(4, 2, 2, 19.98),
(5, 2, 3, 14.97),
(1, 3, 1, 15.99),
(2, 3, 2, 101.50),
(3, 3, 1, 30.99),
(4, 3, 3, 29.97),
(5, 3, 1, 4.99),
(1, 4, 3, 47.97),
(2, 4, 2, 151.98),
(3, 4, 1, 30.99),
(4, 4, 4, 79.92),
(5, 4, 1, 4.99),
(1, 5, 1, 2.50),
(2, 5, 2, 101.50),
(3, 5, 1, 30.99),
(4, 5, 4, 39.96),
(5, 5, 5, 24.95);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mat_alma`
--

CREATE TABLE `mat_alma` (
  `almacen` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `mat_alma`
--

INSERT INTO `mat_alma` (`almacen`, `material`, `stock`) VALUES
(1, 1, 8),
(1, 2, 7),
(1, 3, 9),
(1, 4, 6),
(1, 5, 5),
(2, 1, 10),
(2, 2, 8),
(2, 3, 12),
(2, 4, 7),
(2, 5, 6),
(3, 1, 5),
(3, 2, 10),
(3, 3, 8),
(3, 4, 9),
(3, 5, 5),
(4, 1, 12),
(4, 2, 7),
(4, 3, 9),
(4, 4, 8),
(4, 5, 4),
(5, 1, 6),
(5, 2, 12),
(5, 3, 7),
(5, 4, 9),
(5, 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `Codigo` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `num_pago` varchar(20) DEFAULT NULL,
  `montoPago` decimal(10,2) DEFAULT NULL,
  `concepto` varchar(100) DEFAULT NULL,
  `saldo` decimal(10,2) DEFAULT NULL,
  `pedido` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`Codigo`, `fecha`, `num_pago`, `montoPago`, `concepto`, `saldo`, `pedido`) VALUES
(6, '2023-11-21', 'PAGO-001', 100.00, 'Pago parcial', 245.58, 1),
(7, '2023-11-22', 'PAGO-002', 200.00, 'Pago parcial', 369.57, 2),
(8, '2023-11-23', 'PAGO-003', 50.00, 'Pago parcial', 88.86, 3),
(9, '2023-11-24', 'PAGO-004', 150.00, 'Pago parcial', 80.00, 4),
(10, '2023-11-25', 'PAGO-005', 8.63, 'Pago final', 0.00, 5),
(11, '2023-11-21', 'PAGO-001', 100.00, 'Pago parcial', 245.58, 1),
(12, '2023-11-22', 'PAGO-002', 200.00, 'Pago parcial', 369.57, 2),
(13, '2023-11-23', 'PAGO-003', 50.00, 'Pago parcial', 88.86, 3),
(14, '2023-11-24', 'PAGO-004', 150.00, 'Pago parcial', 80.00, 4),
(15, '2023-11-25', 'PAGO-005', 8.63, 'Pago final', 0.00, 5),
(16, '2023-11-21', 'PAGO-001', 100.00, 'Pago parcial', 245.58, 1),
(17, '2023-11-22', 'PAGO-002', 200.00, 'Pago parcial', 369.57, 2),
(18, '2023-11-23', 'PAGO-003', 50.00, 'Pago parcial', 88.86, 3),
(19, '2023-11-24', 'PAGO-004', 150.00, 'Pago parcial', 80.00, 4),
(20, '2023-11-25', 'PAGO-005', 8.63, 'Pago final', 0.00, 5),
(21, '2023-11-21', 'PAGO-001', 100.00, 'Pago parcial', 245.58, 1),
(22, '2023-11-22', 'PAGO-002', 200.00, 'Pago parcial', 369.57, 2),
(23, '2023-11-23', 'PAGO-003', 50.00, 'Pago parcial', 88.86, 3),
(24, '2023-11-24', 'PAGO-004', 150.00, 'Pago parcial', 80.00, 4),
(25, '2023-11-25', 'PAGO-005', 8.63, 'Pago final', 0.00, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cantProduct` int(11) DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `iva` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `totalInt` decimal(10,2) DEFAULT NULL,
  `almacen` int(11) DEFAULT NULL,
  `lugar` int(11) DEFAULT NULL,
  `cliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`codigo`, `descripcion`, `fecha`, `cantProduct`, `subtotal`, `iva`, `total`, `totalInt`, `almacen`, `lugar`, `cliente`) VALUES
(1, 'Pedido para Construcción de Edificio', '2023-11-21', 5, 300.50, 45.08, 345.58, 380.14, 1, 1, 1),
(2, 'Suministros para Obras en Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 569.57, 2, 2, 2),
(3, 'Pedido de Herramientas Variadas', '2023-11-23', 4, 120.75, 18.11, 138.86, 152.75, 3, 3, 3),
(4, 'Materiales para Proyecto de Innovación', '2023-11-24', 6, 200.00, 30.00, 230.00, 253.00, 4, 4, 4),
(5, 'Pedido de Tornillos para Construcción', '2023-11-25', 3, 7.50, 1.13, 8.63, 9.49, 5, 5, 5),
(6, 'Pedido para Construcción de Edificio', '2023-11-21', 5, 300.50, 45.08, 345.58, 380.14, 1, 1, 1),
(7, 'Suministros para Obras en Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 569.57, 2, 2, 2),
(8, 'Pedido de Herramientas Variadas', '2023-11-23', 4, 120.75, 18.11, 138.86, 152.75, 3, 3, 3),
(9, 'Materiales para Proyecto de Innovación', '2023-11-24', 6, 200.00, 30.00, 230.00, 253.00, 4, 4, 4),
(10, 'Pedido de Tornillos para Construcción', '2023-11-25', 3, 7.50, 1.13, 8.63, 9.49, 5, 5, 5),
(11, 'Pedido para Construcción de Edificio', '2023-11-21', 5, 300.50, 45.08, 345.58, 380.14, 1, 1, 1),
(12, 'Suministros para Obras en Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 569.57, 2, 2, 2),
(13, 'Pedido de Herramientas Variadas', '2023-11-23', 4, 120.75, 18.11, 138.86, 152.75, 3, 3, 3),
(14, 'Materiales para Proyecto de Innovación', '2023-11-24', 6, 200.00, 30.00, 230.00, 253.00, 4, 4, 4),
(15, 'Pedido de Tornillos para Construcción', '2023-11-25', 3, 7.50, 1.13, 8.63, 9.49, 5, 5, 5),
(16, 'Pedido para Construcción de Edificio', '2023-11-21', 5, 300.50, 45.08, 345.58, 380.14, 1, 1, 1),
(17, 'Suministros para Obras en Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 569.57, 2, 2, 2),
(18, 'Pedido de Herramientas Variadas', '2023-11-23', 4, 120.75, 18.11, 138.86, 152.75, 3, 3, 3),
(19, 'Materiales para Proyecto de Innovación', '2023-11-24', 6, 200.00, 30.00, 230.00, 253.00, 4, 4, 4),
(20, 'Pedido de Tornillos para Construcción', '2023-11-25', 3, 7.50, 1.13, 8.63, 9.49, 5, 5, 5),
(21, 'Pedido para Construcción de Edificio', '2023-11-21', 5, 300.50, 45.08, 345.58, 380.14, 1, 1, 1),
(22, 'Suministros para Obras en Construcción', '2023-11-22', 7, 450.25, 67.54, 517.79, 569.57, 2, 2, 2),
(23, 'Pedido de Herramientas Variadas', '2023-11-23', 4, 120.75, 18.11, 138.86, 152.75, 3, 3, 3),
(24, 'Materiales para Proyecto de Innovación', '2023-11-24', 6, 200.00, 30.00, 230.00, 253.00, 4, 4, 4),
(25, 'Pedido de Tornillos para Construcción', '2023-11-25', 3, 7.50, 1.13, 8.63, 9.49, 5, 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ped_mat`
--

CREATE TABLE `ped_mat` (
  `pedido` int(11) NOT NULL,
  `material` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `importe` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `ped_mat`
--

INSERT INTO `ped_mat` (`pedido`, `material`, `cantidad`, `importe`) VALUES
(1, 1, 2, 31.98),
(1, 2, 3, 227.97),
(1, 3, 1, 30.99),
(1, 4, 4, 39.96),
(1, 5, 5, 24.95),
(2, 1, 2, 31.98),
(2, 2, 1, 75.99),
(2, 3, 4, 123.96),
(2, 4, 2, 19.98),
(2, 5, 3, 14.97),
(3, 1, 1, 15.99),
(3, 2, 2, 101.50),
(3, 3, 1, 30.99),
(3, 4, 3, 29.97),
(3, 5, 1, 4.99),
(4, 1, 3, 47.97),
(4, 2, 2, 151.98),
(4, 3, 1, 30.99),
(4, 4, 4, 79.92),
(4, 5, 1, 4.99),
(5, 1, 1, 2.50),
(5, 2, 2, 101.50),
(5, 3, 1, 30.99),
(5, 4, 4, 39.96),
(5, 5, 5, 24.95),
(1, 1, 2, 31.98),
(1, 2, 3, 227.97),
(1, 3, 1, 30.99),
(1, 4, 4, 39.96),
(1, 5, 5, 24.95),
(2, 1, 2, 31.98),
(2, 2, 1, 75.99),
(2, 3, 4, 123.96),
(2, 4, 2, 19.98),
(2, 5, 3, 14.97),
(3, 1, 1, 15.99),
(3, 2, 2, 101.50),
(3, 3, 1, 30.99),
(3, 4, 3, 29.97),
(3, 5, 1, 4.99),
(4, 1, 3, 47.97),
(4, 2, 2, 151.98),
(4, 3, 1, 30.99),
(4, 4, 4, 79.92),
(4, 5, 1, 4.99),
(5, 1, 1, 2.50),
(5, 2, 2, 101.50),
(5, 3, 1, 30.99),
(5, 4, 4, 39.96),
(5, 5, 5, 24.95);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`codigo`, `nombre`, `correo`) VALUES
(1, 'Suministros Industriales S.A.', 'info@suministrosindustriales.com'),
(2, 'Materiales de Construcción Innovadores', 'info@materialesinnovadores.com'),
(3, 'Proveedor Arquitectónico', 'info@proveedorarquitectonico.com'),
(4, 'Construcción y Más', 'info@construccionymas.com'),
(5, 'Materiales Verdes', 'info@materialesverdes.com'),
(6, 'Suministros Industriales S.A.', 'info@suministrosindustriales.com'),
(7, 'Materiales de Construcción Innovadores', 'info@materialesinnovadores.com'),
(8, 'Proveedor Arquitectónico', 'info@proveedorarquitectonico.com'),
(9, 'Construcción y Más', 'info@construccionymas.com'),
(10, 'Materiales Verdes', 'info@materialesverdes.com'),
(11, 'Suministros Industriales S.A.', 'info@suministrosindustriales.com'),
(12, 'Materiales de Construcción Innovadores', 'info@materialesinnovadores.com'),
(13, 'Proveedor Arquitectónico', 'info@proveedorarquitectonico.com'),
(14, 'Construcción y Más', 'info@construccionymas.com'),
(15, 'Materiales Verdes', 'info@materialesverdes.com'),
(16, 'Suministros Industriales S.A.', 'info@suministrosindustriales.com'),
(17, 'Materiales de Construcción Innovadores', 'info@materialesinnovadores.com'),
(18, 'Proveedor Arquitectónico', 'info@proveedorarquitectonico.com'),
(19, 'Construcción y Más', 'info@construccionymas.com'),
(20, 'Materiales Verdes', 'info@materialesverdes.com'),
(21, 'Suministros Industriales S.A.', 'info@suministrosindustriales.com'),
(22, 'Materiales de Construcción Innovadores', 'info@materialesinnovadores.com'),
(23, 'Proveedor Arquitectónico', 'info@proveedorarquitectonico.com'),
(24, 'Construcción y Más', 'info@construccionymas.com'),
(25, 'Materiales Verdes', 'info@materialesverdes.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resenas`
--

CREATE TABLE `resenas` (
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(4000) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `resenas`
--

INSERT INTO `resenas` (`codigo`, `descripcion`, `fecha`, `cliente`) VALUES
(1, 'Excelente servicio y atención al cliente', '2023-11-21', 1),
(2, 'Materiales de alta calidad, entrega puntual', '2023-11-22', 2),
(3, 'Herramientas duraderas y resistentes', '2023-11-23', 3),
(4, 'Proveedores confiables, cumplen con los plazos', '2023-11-24', 4),
(5, 'Amplia variedad de productos, altamente recomendado', '2023-11-25', 5),
(6, 'Excelente servicio y atención al cliente', '2023-11-21', 1),
(7, 'Materiales de alta calidad, entrega puntual', '2023-11-22', 2),
(8, 'Herramientas duraderas y resistentes', '2023-11-23', 3),
(9, 'Proveedores confiables, cumplen con los plazos', '2023-11-24', 4),
(10, 'Amplia variedad de productos, altamente recomendado', '2023-11-25', 5),
(11, 'Excelente servicio y atención al cliente', '2023-11-21', 1),
(12, 'Materiales de alta calidad, entrega puntual', '2023-11-22', 2),
(13, 'Herramientas duraderas y resistentes', '2023-11-23', 3),
(14, 'Proveedores confiables, cumplen con los plazos', '2023-11-24', 4),
(15, 'Amplia variedad de productos, altamente recomendado', '2023-11-25', 5),
(16, 'Excelente servicio y atención al cliente', '2023-11-21', 1),
(17, 'Materiales de alta calidad, entrega puntual', '2023-11-22', 2),
(18, 'Herramientas duraderas y resistentes', '2023-11-23', 3),
(19, 'Proveedores confiables, cumplen con los plazos', '2023-11-24', 4),
(20, 'Amplia variedad de productos, altamente recomendado', '2023-11-25', 5),
(21, 'Excelente servicio y atención al cliente', '2023-11-21', 1),
(22, 'Materiales de alta calidad, entrega puntual', '2023-11-22', 2),
(23, 'Herramientas duraderas y resistentes', '2023-11-23', 3),
(24, 'Proveedores confiables, cumplen con los plazos', '2023-11-24', 4),
(25, 'Amplia variedad de productos, altamente recomendado', '2023-11-25', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`codigo`, `nombre`, `descripcion`) VALUES
(1, 'Construcción', 'Materiales para proyectos de construcción'),
(2, 'Estructuras', 'Elementos estructurales para edificaciones'),
(3, 'Pintura', 'Productos para pintura y acabados'),
(4, 'Herramientas', 'Herramientas variadas para proyectos'),
(5, 'Ferretería', 'Suministros de ferretería para construcción'),
(6, 'Construcción', 'Materiales para proyectos de construcción'),
(7, 'Estructuras', 'Elementos estructurales para edificaciones'),
(8, 'Pintura', 'Productos para pintura y acabados'),
(9, 'Herramientas', 'Herramientas variadas para proyectos'),
(10, 'Ferretería', 'Suministros de ferretería para construcción'),
(11, 'Construcción', 'Materiales para proyectos de construcción'),
(12, 'Estructuras', 'Elementos estructurales para edificaciones'),
(13, 'Pintura', 'Productos para pintura y acabados'),
(14, 'Herramientas', 'Herramientas variadas para proyectos'),
(15, 'Ferretería', 'Suministros de ferretería para construcción'),
(16, 'Construcción', 'Materiales para proyectos de construcción'),
(17, 'Estructuras', 'Elementos estructurales para edificaciones'),
(18, 'Pintura', 'Productos para pintura y acabados'),
(19, 'Herramientas', 'Herramientas variadas para proyectos'),
(20, 'Ferretería', 'Suministros de ferretería para construcción'),
(21, 'Construcción', 'Materiales para proyectos de construcción'),
(22, 'Estructuras', 'Elementos estructurales para edificaciones'),
(23, 'Pintura', 'Productos para pintura y acabados'),
(24, 'Herramientas', 'Herramientas variadas para proyectos'),
(25, 'Ferretería', 'Suministros de ferretería para construcción');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `cambios`
--
ALTER TABLE `cambios`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `pedido` (`pedido`),
  ADD KEY `almacen` (`almacen`);

--
-- Indices de la tabla `camb_material`
--
ALTER TABLE `camb_material`
  ADD KEY `cambio` (`cambio`),
  ADD KEY `material` (`material`);

--
-- Indices de la tabla `Cliente`
--
ALTER TABLE `Cliente`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `Compra`
--
ALTER TABLE `Compra`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Proveedor` (`Proveedor`),
  ADD KEY `almacen` (`almacen`);

--
-- Indices de la tabla `lugar`
--
ALTER TABLE `lugar`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `Material`
--
ALTER TABLE `Material`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Tipo` (`Tipo`);

--
-- Indices de la tabla `material_compra`
--
ALTER TABLE `material_compra`
  ADD KEY `material` (`material`),
  ADD KEY `compra` (`compra`);

--
-- Indices de la tabla `mat_alma`
--
ALTER TABLE `mat_alma`
  ADD KEY `almacen` (`almacen`),
  ADD KEY `material` (`material`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `pedido` (`pedido`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `almacen` (`almacen`),
  ADD KEY `lugar` (`lugar`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `ped_mat`
--
ALTER TABLE `ped_mat`
  ADD KEY `pedido` (`pedido`),
  ADD KEY `material` (`material`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `resenas`
--
ALTER TABLE `resenas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`codigo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacen`
--
ALTER TABLE `almacen`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `cambios`
--
ALTER TABLE `cambios`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `Cliente`
--
ALTER TABLE `Cliente`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `Compra`
--
ALTER TABLE `Compra`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `lugar`
--
ALTER TABLE `lugar`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `Material`
--
ALTER TABLE `Material`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `resenas`
--
ALTER TABLE `resenas`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cambios`
--
ALTER TABLE `cambios`
  ADD CONSTRAINT `cambios_ibfk_1` FOREIGN KEY (`pedido`) REFERENCES `pedido` (`codigo`),
  ADD CONSTRAINT `cambios_ibfk_2` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`);

--
-- Filtros para la tabla `camb_material`
--
ALTER TABLE `camb_material`
  ADD CONSTRAINT `camb_material_ibfk_1` FOREIGN KEY (`cambio`) REFERENCES `cambios` (`codigo`),
  ADD CONSTRAINT `camb_material_ibfk_2` FOREIGN KEY (`material`) REFERENCES `Material` (`Codigo`);

--
-- Filtros para la tabla `Compra`
--
ALTER TABLE `Compra`
  ADD CONSTRAINT `Compra_ibfk_1` FOREIGN KEY (`Proveedor`) REFERENCES `proveedor` (`codigo`),
  ADD CONSTRAINT `Compra_ibfk_2` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`);

--
-- Filtros para la tabla `Material`
--
ALTER TABLE `Material`
  ADD CONSTRAINT `Material_ibfk_1` FOREIGN KEY (`Tipo`) REFERENCES `tipos` (`codigo`);

--
-- Filtros para la tabla `material_compra`
--
ALTER TABLE `material_compra`
  ADD CONSTRAINT `material_compra_ibfk_1` FOREIGN KEY (`material`) REFERENCES `Material` (`Codigo`),
  ADD CONSTRAINT `material_compra_ibfk_2` FOREIGN KEY (`compra`) REFERENCES `Compra` (`Codigo`);

--
-- Filtros para la tabla `mat_alma`
--
ALTER TABLE `mat_alma`
  ADD CONSTRAINT `mat_alma_ibfk_1` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`),
  ADD CONSTRAINT `mat_alma_ibfk_2` FOREIGN KEY (`material`) REFERENCES `Material` (`Codigo`);

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`pedido`) REFERENCES `pedido` (`codigo`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`almacen`) REFERENCES `almacen` (`codigo`),
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`lugar`) REFERENCES `lugar` (`codigo`),
  ADD CONSTRAINT `pedido_ibfk_3` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`codigo`);

--
-- Filtros para la tabla `ped_mat`
--
ALTER TABLE `ped_mat`
  ADD CONSTRAINT `ped_mat_ibfk_1` FOREIGN KEY (`pedido`) REFERENCES `pedido` (`codigo`),
  ADD CONSTRAINT `ped_mat_ibfk_2` FOREIGN KEY (`material`) REFERENCES `Material` (`Codigo`);

--
-- Filtros para la tabla `resenas`
--
ALTER TABLE `resenas`
  ADD CONSTRAINT `resenas_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
