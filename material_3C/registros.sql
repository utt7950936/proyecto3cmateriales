-- Active: 1700758310449@@127.0.0.1@3307@materiales3c
INSERT INTO Cliente VALUES
(1, "Construcciones Velázquez S.A.", "664-123-4567", "info@construccionesvelazquez.com"),
(2, "Tecnología Innovadora MX", "664-987-6543", "contacto@tecnoinnovamx.com"),
(3, "Servicios Eléctricos Torres", "664-345-6789", "contacto@torres-electricos.com"),
(4, "Materiales de Calidad SA", "664-876-5432", "ventas@materialesdecalidad.com"),
(5, "Constructora Progresiva", "664-234-5678", "info@constructoraprogresiva.com"),
(6, "Soluciones Hidráulicas Gómez", "664-765-4321", "contacto@solucionesgomez.com"),
(7, "Energía Renovable y Asociados", "664-321-8765", "info@energiarenovable.com"),
(8, "Logística y Distribución MX", "664-654-3210", "ventas@logisticaydistribucion.com"),
(9, "Ingeniería Estructural Torres", "664-210-9876", "contacto@ingenieriatorres.com"),
(10, "Innovación en Construcción", "664-789-0123", "info@innovacionconstruccion.com");

INSERT INTO lugar VALUES
(1, "Avenida Revolución", "123", "Zonaeste", "22000"),
(2, "Calle Primera", "456", "Playas de Tijuana", "22320"),
(3, "Boulevard Sanchez", "789", "Otay", "22440"),
(4, "Calle Cuarta", "210", "Hipódromo", "22110"),
(5, "Paseo de los Héroes", "567", "Zona Centro", "22600"),
(6, "Avenida Constitución", "890", "Guaycura", "22045"),
(7, "Calle Octava", "342", "Cacho", "22310"),
(8, "Calle Quinta", "765", "Libertad", "22020"),
(9, "Boulevard Fundadores", "432", "Moderna", "22410"),
(10, "Calle Novena", "654", "Chapultepec", "22610");

INSERT INTO tipos VALUES
(1, 'CEMENTO', 'Material de construcción para unir elementos, brindando resistencia.'),
(2, 'ACEROS', 'Material resistente utilizado en construcción y fabricación de estructuras.'),
(3, 'ACABADOS', 'Materiales decorativos como pinturas, azulejos y cerámicos para mejorar la estética de espacios interiores y exteriores.'),
(4, 'PLOMERIA', 'Materiales relacionados con instalaciones de agua y sistemas de fontanería.'),
(5, 'METERIAL ELECTRICO', 'Componentes y dispositivos utilizados en instalaciones eléctricas para la conducción y control de la electricidad.'),
(6, 'HERRAMIENTAS CONTRUCCION', 'Instrumentos y utensilios utilizados en proyectos de construcción para diversas tareas y aplicaciones.'),
(7, 'FERRETERIA', 'Materiales y herramientas para construcción y reparación.');

INSERT INTO almacen VALUES
(1, 'RevoConstruccion', 'Avenida Revolucion', '123', 'Zona Centro', '22000'),
(2, 'PrimeraConstruccion', 'Calle Primera', '546', 'Playas de Tijuana', '22320'),
(3, 'SanConstruccion', 'Boulevard Sanchez', '789', 'Otay', '22440'),
(4, 'Heroes de la Construccion', 'Paseo de los Heroes', '951', 'Zona Este', '22600');

INSERT INTO proveedor VALUES
(1, 'Construrama Materiales Tijuana S.A. de C.V.', 'compraenlinea@construrama.com'),
(2, 'Productos Eléctricos de Tijuana SA de CV', 'petsacv@grupopetsa.com'),
(3, 'Ferreteria Velazquez', 'ferreteria50@hotmail.com'),
(4, 'FARA Industrial', 'operaciones@ferreteriafara.com');

 INSERT INTO resenas VALUES 
 (1, 'Excelente servicio, entregaron los materiales a tiempo.', '2023-05-10', 1),
(2, 'El personal fue muy amable y atento en todo momento.', '2023-08-21', 3),
(3, 'Hubo un pequeño problema con la entrega, pero se solucionó rápido.', '2023-09-12', 7),
(4, 'Muy satisfecho con la calidad de los materiales, definitivamente recomendaré.', '2023-11-24', 9);

INSERT INTO Compra VAlUES
(1, 'COMPRA DE CEMENTO Y HERRAMIENTAS', '2023-01-05', 50, 5922, 1128, 7050, 1, 2),
(2, 'LUCES Y MATERIAL ELECTRICO', '2023-02-10', 69, 4956, 944, 5900, 2, 4),
(3, 'PINTURAS Y ACCESORIOS', '2023-03-15', 40, 2000, 400, 2400, 3, 1),
(4, 'COMPRA DE ESTRUCTURAS', '2023-04-20', 27, 857.14, 162.86, 1020, 4, 3),
(5, 'HERRAMIENTAS DE CONTRUCCION', '2023-05-25', 38, 1292, 228, 1520, 1, 4),
(6, 'CABLEADO ELECTRICO', '2023-06-30', 28, 2265.6, 494.4, 2760, 2, 1),
(7, 'AZULEJOS Y MATERIAL PARA PISOS', '2023-08-05', 43, 4536, 804, 5340, 3, 3),
(8, 'COMPRA DE TUERCAS Y TORMILLOS', '2023-09-10', 20, 4250, 750, 5000, 4, 2),
(9, 'MATERIAL PARA FONTANERIA', '2023-10-15', 27, 2469.6, 470.4, 2940, 1, 1),
(10, 'LAMPARAS Y ACCESORIOS DE ILUMINACION', '2023-11-20', 25, 399.4, 75.6, 475, 2, 3);

INSERT INTO Material VALUES
(1, 'BOLSA DE CEMENTO', 'Bolsa de cemento Portland para construcción', 120, 150, 1, NULL),
(2, 'VARILLA DE ACERO', 'Varilla de acero corrugado de 3/8 pulgadas', 50, 70, 2, NULL),
(3, 'PINTURA INTERIOR', 'Pintura acrílica para interiores en color blanco', 200, 250, 3, NULL),
(4, 'TUBO DE PVC', 'Tubo de PVC para instalaciones de fontanería', 25, 30, 4, NULL),
(5, 'INTERRUPTOR ELECTRICO', 'Interruptor de luz para instalaciones eléctricas ', 40, 50, 5, NULL),
(6, 'ADHESIVO PARA AZULEJOS', 'Adhesivo especial para la instalación de azulejos', 60, 80, 3, NULL),
(7, 'GRIFO DE LAVABO', 'Grifo monomando para lavabo con acabado cromado', 240, 300, 4, NULL),
(8, 'CABLE ELECTRICO', 'Cable eléctrico THW calibre 12 para instalaciones', 15, 20, 5, NULL),
(9, 'LLAVE AJUSTABLE', 'Llave ajustable de 10 pulgadas para trabajos generales', 100, 120, 6, NULL),
(10, 'TUBERIA DE HIERRRO GALVANIZADO', 'Tubería de hierro galvanizado de 1 pulgada', 30, 40, 2, NULL),
(11, 'AZUELJO PARA BANO', 'Azulejo cerámico para revestimiento de baños', 45, 60, 3, NULL),
(12, 'CINTA TEFLON', 'Cinta de teflón para sellar roscas en instalaciones', 4, 5, 4, NULL),
(13, 'FOCO LED', 'Foco LED de 9W para iluminación eficiente', 25, 30, 5, NULL),
(14, 'BROCHA DE PINTURA', 'Brocha de cerdas naturales para aplicar pintura ', 20, 25, 3, NULL),
(15, 'LLAVE INGLESA', 'Llave inglesa de 8 pulgadas para trabajos mecánicos', 60, 80, 6, NULL),
(16, 'GRANO DE ARENA', 'Saco de arena de grano fino para trabajos de construcción', 40, 50, 1, NULL),
(17, 'TUBERIA DE CPVC', 'Tubería de CPVC para sistemas de fontanería', 30, 35, 4, NULL),
(18, 'INTERRUPTOR DIFERENCIAL', 'Interruptor diferencial para protección eléctrica', 100, 120, 5, NULL),
(19, 'ESMERILADORA ANGULAR', 'Esmeriladora angular de 4.5 pulgadas para cortes', 240, 300, 6, NULL),
(20, 'CLAVO DE ACERO', 'Clavo de acero para construcción de 3 pulgadas', 8, 10, 2, NULL),
(21, 'SACO DE MORTERO', 'Saco de mortero premezclado para albañilería', 150, 180, 1, NULL),
(22, 'TUVO GALVANIZADO', 'Tubo de hierro galvanizado de 2 pulgadas', 50, 60, 2, NULL),
(23, 'PISO CERAMICO', 'Piso cerámico para espacios interiores', 100, 120, 3, NULL),
(24, 'SIFON PARA FREGADERO', 'Sifón de PVC para desagüe de fregadero', 20, 25, 4, NULL),
(25, 'CABLE ELECTRICO THHN', 'Cable eléctrico THHN calibre 10 para instalaciones', 20, 25, 5, NULL);


INSERT INTO pedido VALUES
(1, 'Pedido de Construcción', '2023-01-15', 25, 2198.28, 351.72, 1950, 2145, 1, 1, 1),
(2, 'Pedido de Material Eléctrico', '2023-02-20', 16, 896.55, 143.45, 800, 880, 2, 2, 2),
(3, 'Pedido de Acabados para Remodelación', '2023-03-25', 40, 1896.55, 303.45, 1700, 1870, 3, 3, 3),
(4, 'Pedido de Herramientas de Construcción', '2023-04-30', 17, 1284.48, 205.52, 1240, 1364, 4, 4, 4),
(5, 'Pedido de Plomería', '2023-05-15', 23, 874.14, 140.86, 780, 858, 1, 5, 5),
(6, 'Pedido de Material Eléctrico', '2023-06-20', 18, 1586.21, 253.79, 1480, 1628, 2, 6, 6),
(7, 'Pedido de Ferretería', '2023-07-25', 25, 1336.21, 213.79, 1150, 1265, 3, 7, 7),
(8, 'Pedido de Acabados para Baño', '2023-08-30', 18, 2379.31, 380.69, 2300, 2530, 4, 8, 8),
(9, 'Pedido de Material de Construcción', '2023-09-15', 20, 2241.38, 358.62, 2080, 2288, 1, 9, 9),
(10, 'Pedido de Ferretería', '2023-10-20', 26, 594.83, 95.17, 520, 572, 2, 10, 10);

INSERT INTO pago (codigo, fecha, montoPago, concepto, saldo, pedido)VALUES
(11, '2023/01/15', 1500,  'Pago inicial', 0, 1),
(12, '2023/02/01', 300, 'Pago parcial', 0, 1),
(13, '2023/02/20', 500, 'Pago inicial', 0, 2),
(14, '2023/03/10', 250, 'Pago parcial', 0, 2),
(15, '2023/03/25', 1200,  'Pago inicial', 0, 3),
(16, '2023/04/05', 300, 'Pago inicial', 0, 4),
(17, '2023/04/10', 500, 'Pago parcial', 0, 4),
(18, '2023/04/30', 500, 'Pago inicial', 0, 5),
(19, '2023/05/15', 200, 'Pago parcial', 0, 5),
(20, '2023/06/01', 800, 'Pago inicial', 0, 6);

INSERT INTO cambios VALUES
(1, '2023/07/27', 'Reemplazo de varillas de acero por mala calidad', 7, 3),
(2, '2023/03/28', 'Sustitución de interruptores defectuosos', 3, 3),
(3, '2023/05/17', 'Cambio de azulejos excedentes en el pedido', 5, 1),
(4, '2023/09/15', 'Modificación de la cantidad de clavos excedentes', 9, 1),
(5, '2023/02/20', 'Corrección en la descripción de adhesivo por error', 2, 2);

INSERT INTO ped_mat VALUES
(1, 1, 10, 1500),
(1, 2, 15, 1050),
(2, 5, 8, 400),
(2, 6, 8, 640),
(3, 11, 20, 1200),
(3, 5, 20, 1000),
(4, 20, 5, 50),
(4, 23, 12, 1440),
(5, 11, 8, 480),
(5, 25, 15, 375),
(6, 6, 8, 640),
(6, 9, 10, 1200),
(7, 2, 15, 1050),
(7, 16, 10, 500),
(8, 18, 8, 960),
(8, 21, 10, 1800),
(9, 3, 10, 2500),
(9, 20, 10, 100),
(10, 10, 16, 640),
(10, 12, 10, 50);

INSERT INTO mat_alma VALUES
(1, 1, 30),
(1, 2, 21),
(1, 3, 120),
(1, 4, 124),
(1, 5, 229),
(1, 6, 75),
(1, 7, 241),
(1, 8, 119),
(1, 9, 28),
(1, 10, 63),
(1, 11, 221),
(1, 12, 24),
(1, 13, 101),
(1, 14, 203),
(1, 15, 217),
(1, 16, 163),
(1, 17, 161),
(1, 18, 146),
(1, 19, 152),
(1, 20, 215),
(1, 21, 200),
(1, 22, 147),
(1, 23, 107),
(1, 24, 13),
(1, 25, 98),
(2, 1, 200),
(2, 2, 233),
(2, 3, 11),
(2, 4, 138),
(2, 5, 67),
(2, 6, 11),
(2, 7, 30),
(2, 8, 73),
(2, 9, 66),
(2, 10, 175),
(2, 11, 133),
(2, 12, 176),
(2, 13, 12),
(2, 14, 236),
(2, 15, 71),
(2, 16, 184),
(2, 17, 166),
(2, 18, 131),
(2, 19, 95),
(2, 20, 249),
(2, 21, 204),
(2, 22, 28),
(2, 23, 112),
(2, 24, 11),
(2, 25, 202),
(3, 1, 245),
(3, 2, 165),
(3, 3, 147),
(3, 4, 220),
(3, 5, 63),
(3, 6, 230),
(3, 7, 98),
(3, 8, 39),
(3, 9, 222),
(3, 10, 146),
(3, 11, 216),
(3, 12, 43),
(3, 13, 162),
(3, 14, 164),
(3, 15, 117),
(3, 16, 242),
(3, 17, 50),
(3, 18, 37),
(3, 19, 109),
(3, 20, 234),
(3, 21, 120),
(3, 22, 190),
(3, 23, 184),
(3, 24, 12),
(3, 25, 149),
(4, 1, 238),
(4, 2, 146),
(4, 3, 235),
(4, 4, 30),
(4, 5, 28),
(4, 6, 46),
(4, 7, 247),
(4, 8, 14),
(4, 9, 217),
(4, 10, 22),
(4, 11, 112),
(4, 12, 156),
(4, 13, 33),
(4, 14, 53),
(4, 15, 206),
(4, 16, 29),
(4, 17, 181),
(4, 18, 45),
(4, 19, 54),
(4, 20, 22),
(4, 21, 83),
(4, 22, 223),
(4, 23, 171),
(4, 24, 86),
(4, 25, 193);

INSERT INTO material_compra VALUES
(1, 1, 15, 2250),
(2, 1, 20, 1800),
(3, 1, 10, 2500),
(4, 1, 5, 500),
(5, 2, 12, 960),
(6, 2, 8, 640),
(7, 2, 15, 1500),
(8, 2, 8, 960),
(9, 2, 10, 1200),
(10, 2, 16, 640),
(11, 3, 20, 1200),
(5, 3, 20, 1200),
(12, 4, 12, 120),
(23, 4, 15, 900),
(13, 5, 10, 300),
(14, 5, 8, 320),
(15, 5, 10, 400),
(16, 5, 10, 500),
(17, 6, 10, 800),
(18, 6, 8, 960),
(19, 6, 10, 1000),
(20, 7, 20, 2000),
(21, 7, 15, 2700),
(22, 7, 8, 640),
(3, 8, 10, 2500),
(20, 8, 10, 2500),
(7, 9, 15, 1500),
(9, 9, 12, 1440),
(25, 10, 15, 375),
(12, 10, 10, 100);

INSERT INTO camb_material VALUES
(2, 2, 0,3);
(3, 11, 8),
(4, 20, 2),
(5, 6, 5);





---------------insert nuevos para la tabla pagos 

INSERT INTO pago (fecha, num_pago, montoPago, concepto, pedido) VALUES
('2023-01-01', '14', 120, 'Pago parcial', 1),
('2023-01-15', '15', 70, 'Pago parcial', 1),
('2023-02-01', '16', 87, 'Pago parcial', 1),
('2023-01-05', '17', 150, 'Pago parcial', 2),
('2023-01-20', '18', 200, 'Pago parcial', 2),
('2023-02-05', '19', 36, 'Pago parcial', 2),
('2023-01-10', '20', 67, 'Pago parcial', 3),
('2023-01-25', '21', 89, 'Pago parcial', 3),
('2023-02-10', '22', 123, 'Pago parcial', 3),
('2023-01-15', '23', 10, 'Pago parcial', 4),
('2023-01-30', '24', 20, 'Pago parcial', 4),
('2023-02-15', '25', 15, 'Pago parcial', 4),
('2023-01-20', '26', 25, 'Pago parcial', 5),
('2023-02-04', '27', 120, 'Pago parcial', 5),
('2023-02-19', '28', 15, 'Pago parcial', 5),
('2023-01-25', '29', 30, 'Pago parcial', 6),
('2023-02-09', '30', 10, 'Pago parcial', 6),
('2023-02-24', '31', 5, 'Pago parcial', 6),
('2023-02-01', '32', 15, 'Pago parcial', 7),
('2023-02-16', '33', 25, 'Pago parcial', 7),
('2023-03-02', '34', 200, 'Pago parcial', 7),
('2023-02-06', '35', 30, 'Pago parcial', 8),
('2023-02-21', '36', 10, 'Pago parcial', 8),
('2023-03-07', '37', 100, 'Pago parcial', 8),
('2023-02-11', '38', 20, 'Pago parcial', 9),
('2023-02-26', '39', 20, 'Pago parcial', 9),
('2023-03-12', '40', 147, 'Pago parcial', 9),
('2023-02-16', '41', 90, 'Pago parcial', 10),
('2023-03-02', '42', 200, 'Pago parcial', 10),
('2023-03-02', '42', 100, 'Pago parcial', 10);



---insert nuevo para la tabla `proveedor`

INSERT INTO proveedor (codigo,nombre, correo) VALUES
(6,'Construmax Tijuana', 'ventas@construmax.com');
(7,'Herramientas y Más', 'info@herramientasymas.com');



---insert nuevos a la tabla materiales

INSERT INTO material (codigo, nombre, descripcion, precioCompra, PrecioVenta, Tipo) VALUES
(27, 'CERÁMICA PARA BAÑO', 'Azulejos cerámicos para revestimiento de baños', 30, 40, 3),
(28, 'TUBERÍA DE PVC PARA FONTANERÍA', 'Tubería de PVC resistente para instalaciones de fontanería', 10, 15, 4),
(29, 'FIBRA DE VIDRIO', 'Material ligero y resistente para reforzar estructuras', 45, 55, 2),
(30, 'CEMENTO RÁPIDO', 'Cemento de fraguado rápido para trabajos rápidos', 180, 220, 1),
(31, 'TELA ASFÁLTICA', 'Material impermeabilizante para techos', 25, 35, 3),
(32, 'MORTERO PREMEZCLADO', 'Mezcla de cemento y arena para albañilería', 40, 50, 1),
(33, 'GRANOS DE GRAVA', 'Grava para mezclas de construcción', 15, 20, 1),
(34, 'TUBOS DE COBRE', 'Tubos de cobre para sistemas de fontanería', 8, 12, 4),
(35, 'BLOQUES DE CONCRETO', 'Bloques prefabricados para construcción', 2, 3, 1),
(36, 'AISLANTE TÉRMICO', 'Material para aislar térmicamente estructuras', 50, 60, 2);

--insert nuevos para la tabla mat_ped

INSERT INTO ped_mat (pedido, material, cantidad, importe)
VALUES 
    (1, 27, 5, 200),
    (2, 28, 8, 400),
    (3, 29, 15, 825),
    (4, 30, 10, 200),
    (5, 31, 12, 420),
    (6, 32, 8, 320),
    (7, 33, 10, 500),
    (8, 34, 15, 180),
    (9, 35, 20, 240),
    (10, 36, 10, 600);



---insert nuevos para la tbal resenas 

INSERT INTO resenas (descripcion, fecha, cliente)
VALUES 
    ('Los materiales de construcción que adquirimos, como los ladrillos y la madera, fueron de alta calidad. Sin duda, recomendamos esta tienda.', '2023-11-15', 1),
    ('La atención al cliente fue excepcional, nos ayudaron a encontrar los productos exactos que necesitábamos para nuestro proyecto de construcción.', '2023-11-16', 1),
    ('Encontramos todo el material eléctrico que necesitábamos a precios competitivos. ¡Muy satisfechos con la compra de cables y conexiones!', '2023-11-17', 2),
    ('La entrega fue rápida y los productos eran exactamente lo que esperábamos. Sin duda, volveremos a comprar aquí para nuestras necesidades eléctricas.', '2023-11-18', 2),
    ('Los azulejos y materiales para pisos que adquirimos superaron nuestras expectativas. ¡Gracias por el excelente servicio en la compra de materiales de construcción!', '2023-11-19', 3),
    ('Hubo un pequeño problema con la cantidad en la entrega, pero se resolvió de manera eficiente. Estamos contentos con la compra de azulejos y materiales para pisos.', '2023-11-20', 3),
    ('Las herramientas de construcción que compramos cumplen con los estándares de calidad que buscábamos. ¡Excelente servicio en la compra de herramientas!', '2023-11-21', 4),
    ('Nos ayudaron a elegir las herramientas adecuadas para nuestra obra. Estamos muy agradecidos por la asesoría profesional en la compra de herramientas de construcción.', '2023-11-22', 4),
    ('Los materiales de fontanería que adquirimos cumplen con nuestras expectativas. El proceso de compra fue fácil y la entrega fue rápida. Recomendamos esta tienda.', '2023-11-23', 5),
    ('El personal fue amable y nos proporcionaron toda la información que necesitábamos sobre los materiales de fontanería. Definitivamente volveremos a comprar aquí.', '2023-11-24', 5),
    ('Estamos satisfechos con la calidad de los materiales de construcción que compramos. La entrega fue puntual y el servicio al cliente fue excelente.', '2023-11-25', 6),
    ('La variedad de herramientas de construcción disponibles nos facilitó encontrar lo que necesitábamos. Muy contentos con la compra.', '2023-11-26', 6),
    ('Los materiales eléctricos que adquirimos cumplen con nuestras expectativas. La atención al cliente fue excepcional y nos guiaron en la selección de productos.', '2023-11-27', 7),
    ('La entrega fue rápida y los materiales eléctricos estaban bien empaquetados. Recomendamos esta tienda para comprar materiales eléctricos.', '2023-11-28', 7),
    ('Adquirimos azulejos y materiales para pisos para nuestra renovación. La calidad de los productos fue excelente, y el personal nos brindó un buen servicio.', '2023-11-29', 8),
    ('Hubo un pequeño problema con el pedido, pero se solucionó de manera eficiente. Apreciamos la atención al cliente en la compra de azulejos y materiales para pisos.', '2023-11-30', 8),
    ('Los materiales de construcción que compramos son de alta calidad. La entrega fue rápida y el servicio al cliente fue excelente. Recomendamos esta tienda.', '2023-12-01', 9),
    ('Nos ayudaron a encontrar los materiales de construcción adecuados para nuestro proyecto. Estamos satisfechos con la compra y el servicio recibido.', '2023-12-02', 9),
    ('Estamos contentos con la variedad de herramientas de construcción disponibles. El personal nos brindó asesoramiento útil en la selección de herramientas.', '2023-12-03', 10),
    ('La entrega de las herramientas de construcción fue rápida y eficiente. Recomendamos esta tienda para la compra de herramientas de construcción.', '2023-12-04', 10);


INSERT INTO resenas (descripcion, fecha, cliente)
VALUES 
    ('La página web ofrece una amplia variedad de productos y es fácil de navegar. El proceso de compra en línea fue eficiente y sin complicaciones. Recomendamos la plataforma.', '2023-12-05', 1),
    ('La experiencia en la página fue positiva. Encontramos fácilmente los productos que necesitábamos y el proceso de compra fue rápido. Estamos satisfechos con el servicio en línea.', '2023-12-06', 2),
    ('La interfaz de la página es intuitiva y facilita la búsqueda de productos. El proceso de compra en línea fue sencillo y la entrega se realizó según lo programado. Recomendamos la plataforma.', '2023-12-07', 3),
    ('Utilizamos la página para comprar herramientas y quedamos satisfechos con la experiencia en línea. El proceso fue rápido y la información sobre los productos fue detallada y útil.', '2023-12-08', 4),
    ('La página web facilita la búsqueda de materiales de fontanería. La compra en línea fue segura y rápida. Estamos contentos con la experiencia general en la plataforma.', '2023-12-09', 5),
    ('Encontramos fácilmente los materiales de construcción que necesitábamos en la página. El proceso de compra en línea fue eficiente y la entrega se realizó sin problemas. Recomendamos la plataforma.', '2023-12-10', 6),
    ('La página ofrece información detallada sobre los productos eléctricos. La experiencia de compra en línea fue positiva y la entrega fue puntual. Estamos satisfechos con el servicio.', '2023-12-11', 7),
    ('La navegación en la página para elegir azulejos y materiales para pisos fue sencilla. El proceso de compra en línea fue rápido y seguro. Recomendamos la página para estos productos.', '2023-12-12', 8),
    ('La página web brinda opciones claras de filtrado, facilitando la búsqueda de materiales específicos. La experiencia de compra en línea fue satisfactoria y sin complicaciones.', '2023-12-13', 9),
    ('La variedad de herramientas de construcción en la página es impresionante. El proceso de compra en línea fue sencillo y la entrega fue puntual. Recomendamos la plataforma.', '2023-12-14', 10);



---insert nuevos a las tablas compra material para los almacenes 

INSERT INTO material_compra (material, compra, cantidad, importe)
VALUES 
    (27, 1, 8, 960),
    (28, 1, 5, 500),
    (29, 1, 10, 1500),
    (30, 2, 12, 960),
    (31, 2, 15, 1500),
    (32, 3, 8, 640),
    (33, 3, 15, 1500),
    (34, 4, 8, 960),
    (35, 4, 10, 1200),
    (36, 5, 16, 640),
    (27, 6, 5, 500),
    (28, 7, 12, 960),
    (29, 8, 15, 1500),
    (30, 9, 8, 640),
    (31, 10, 10, 1200);





INSERT INTO COMPRA (proveedor, almacen, fecha)
VALUES
    (5, 1, '2023-11-29'),
    (4, 3, '2023-11-29'),
    (2, 3, '2023-11-29'), 
    (5, 4, '2023-11-29'),
    (4, 1, '2023-11-29');


INSERT INTO material_compra (compra, material, cantidad)
VALUES
    (26, 1, 10),
    (26, 10, 5),
    (26, 13, 8),
    (27, 14, 15),
    (27, 15, 7),
    (27, 16, 12),
    (28, 17, 20),
    (28, 18, 4),
    (28, 19, 9),
    (29, 10, 25),
    (29, 11, 3),
    (29, 12, 6),
    (30, 13, 18),
    (30, 14, 22),
    (30, 15, 14);



INSERT INTO cambios (fecha, descripcion, material_devuelto, material_cambiado, pedido, almacen) VALUES
('2023-01-01', 'Cambio de materiales para pedido 1', 1, 27, 1, 1),
('2023-01-02', 'Cambio de materiales para pedido 2', 4, 30, 2, 2),
('2023-01-03', 'Cambio de materiales para pedido 3', 7, 33, 3, 3),
('2023-01-04', 'Cambio de materiales para pedido 4', 10, 36, 4, 4),
('2023-01-05', 'Cambio de materiales para pedido 5', 13, 29, 5, 1),
('2023-01-06', 'Cambio de materiales para pedido 6', 16, 32, 6, 2),
('2023-01-07', 'Cambio de materiales para pedido 7', 19, 35, 7, 3),
('2023-01-08', 'Cambio de materiales para pedido 8', 22, 28, 8, 4),
('2023-01-09', 'Cambio de materiales para pedido 9', 25, 31, 9, 1),
('2023-01-10', 'Cambio de materiales para pedido 10', 28, 34, 10, 2);



INSERT INTO camb_material VALUES
(1, 1, 27, 2, 1, 80.00),
(2, 2, 28, 1, 1, 30.00),
(3, 3, 29, 3, 2, 150.00),
(4, 4, 30, 4, 1, 200.00),
(5, 5, 31, 2, 1, 60.00),
(6, 6, 32, 1, 0.75, 25.00),
(7, 7, 33, 5, 2, 100.00),
(8, 8, 34, 2, 1, 20.00),
(9, 9, 35, 3, 1.5, 15.00),
(10, 10, 36, 1, 0.5, 30.00);
