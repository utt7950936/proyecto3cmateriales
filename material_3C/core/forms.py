from django import forms

from .models import *


class ClientesForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del cliente"}),
            "telefono": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el número de teléfono"}),
            "correo": forms.EmailInput(attrs={"type": "email", "class": "form-control", "placeholder": "Ingrese el correo electrónico"}),
        }

class UpdateClientesForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del cliente"}),
            "telefono": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el número de teléfono"}),
            "correo": forms.EmailInput(attrs={"type": "email", "class": "form-control", "placeholder": "Ingrese el correo electrónico"}),
        }

class LugarForm(forms.ModelForm):
    class Meta:
        model = Lugar
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "num_calle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el número de la calle"}),
            "nomCalle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre de la calle"}),
            "colonia": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre de la colonia"}),
            "codigo_postal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el código postal"}),
        }

class UpdateLugarForm(forms.ModelForm):
    class Meta:
        model = Lugar
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "num_calle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el número de calle"}),
            "nomCalle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre de la calle"}),
            "colonia": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre de la colonia"}),
            "codigo_postal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el código postal"}),
        }

class TiposForm(forms.ModelForm):
    class Meta:
        model = Tipos
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del tipo"}),
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción del tipo"}),
        }

class UpdateTiposForm(forms.ModelForm):
    class Meta:
        model = Tipos
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del tipo"}),
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción del tipo"}),
        }
        
        # se trabaja por secciones
        #Si la seccion no existe se iguala a la del carrito, y si existe se utiliza

        
class Carrito:
    def __init__(self, request):
        # Inicializa el carrito utilizando la sesión de la solicitud
        self.request = request
        self.session = request.session
        carrito = self.session.get("carrito")

        # Si no existe un carrito en la sesión, se crea un diccionario vacío
        if not carrito:
            self.session["carrito"] = {}
            self.carrito = self.session["carrito"]
        else:
            # Si ya existe un carrito, se utiliza el existente
            self.carrito = carrito

    def agregar(self, Material):
        # Añade un material al carrito
        id = str(Material.codigo)
        if id not in self.carrito.keys():
            self.carrito[id] = {
                "material_codigo": Material.codigo,
                "nombre": Material.nombre,
                "acumulado": float(Material.precioventa),
                "cantidad": 1,
            }
        else:
            # Si el material ya está en el carrito, se incrementa la cantidad y el acumulado
            self.carrito[id]["cantidad"] += 1
            self.carrito[id]["acumulado"] += float(Material.precioventa)  # Cast to float explicitly

        # Guarda el carrito actualizado
        self.guardar_carrito()

    def guardar_carrito(self):
        # Guarda el carrito en la sesión
        self.session["carrito"] = self.carrito
        self.session.modified = True

    def eliminar(self, Material):
        # Elimina un material del carrito
        id = str(Material.codigo)
        if id in self.carrito:
            del self.carrito[id]
            self.guardar_carrito()

    def restar(self, Material):
        # Reduce la cantidad y el acumulado de un material en el carrito
        id = str(Material.codigo)
        if id in self.carrito.keys():
            self.carrito[id]["cantidad"] -= 1
            self.carrito[id]["acumulado"] -= float(Material.precioventa)

            # Si la cantidad llega a cero o menos, se elimina el material del carrito
            if self.carrito[id]["cantidad"] <= 0:
                self.eliminar(Material)

            # Guarda el carrito actualizado
            self.guardar_carrito()

    def limpiar(self):
        # Limpia todo el carrito
        self.session["carrito"] = {}
        self.session.modified = True




class PedidoFormcarrito(forms.ModelForm):
    class Meta:
        model = Pedido
        fields = ['descripcion', 'almacen', 'lugar', 'cliente', 'subtotal', 'iva', 'total', 'totalint']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Oculta o desactiva los campos que se llenan automáticamente
        for field_name in ['subtotal', 'iva', 'total', 'totalint']:
            self.fields[field_name].widget = forms.HiddenInput()
            self.fields[field_name].disabled = True


        

# CRUDS

class MaterialForm(forms.ModelForm):
    class Meta:
        model = Material
        fields = "__all__"
        exclude = ["codigo", "precioventa"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del tipo"}),
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción del tipo"}),
            "preciocompra": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el precio de compra"}),
            "tipo": forms.Select(attrs={"class": "form-select"}),
            "imagen": forms.ClearableFileInput(attrs={"class": "form-control-file", "required": False})
        }


class UpdateMaterialForm(forms.ModelForm):
    class Meta:
        model = Material
        fields = "__all__"
        exclude = ["codigo", "precioventa"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del tipo"}),
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción del tipo"}),
            "preciocompra": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el precio de compra"}),
            "tipo": forms.Select(attrs={"class": "form-select"}),
            "imagen": forms.ClearableFileInput(attrs={"class": "form-control-file", "required": False})
        }


    def __init__(self, *args, **kwargs):
        super(UpdateMaterialForm, self).__init__(*args, **kwargs)
        
        



class almacenForm(forms.ModelForm):
    class Meta:
        model = Almacen
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del alamcen"}),
            "nombreCalle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre de calle del almacen"}),
            "num_calle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el numero de calle del almacen"}),
            "colonia": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la colonia del almacen"}),
            "codigo_postal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el codigo postal del almacen"}),
        }

class UpdateAlmacenForm(forms.ModelForm):
    class Meta:
        model = Almacen
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del alamcen"}),
            "nombreCalle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre de calle del almacen"}),
            "num_calle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el numero de calle del almacen"}),
            "colonia": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la colonia del almacen"}),
            "codigo_postal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el codigo postal del almacen"}),
        }


class proveedorForm(forms.ModelForm):
    class Meta:
        model = Proveedor
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del proveedor"}),
            "correo": forms.EmailInput(attrs={"type": "email", "class": "form-control", "placeholder": "Ingrese el correo electrónico"}),        
            }

class UpdateproveedorForm(forms.ModelForm):
    class Meta:
        model = Proveedor
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del proveedor"}),
            "correo": forms.EmailInput(attrs={"type": "email", "class": "form-control", "placeholder": "Ingrese el correo electrónico"}),
        }


class ResenasForm(forms.ModelForm):
    class Meta:
        model = Resenas
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción"}),
            "fecha": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la fecha"}),
            "cliente": forms.Select(attrs={"class": "form-select"}),
        }

class UpdateresenasForm(forms.ModelForm):
    class Meta:
        model = Resenas
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción"}),
            "fecha": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la fecha"}),
            "cliente": forms.Select(attrs={"class": "form-select"}),
        }
        

# class CompraForm(forms.ModelForm):
#     class Meta:
#         model = Compra
#         fields = "__all__"
#         exclude = ["codigo"]
#         widgets = {
#             "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción"}),
#             "fecha": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la fecha"}),
#             "prodtotal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la cantidad total de productos"}),
#             "subtotal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el subtotal"}),
#             "iva": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el IVA"}),
#             "total": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el total"}),
#             "proveedor": forms.Select(attrs={"class": "form-select"}),
#             "almacen": forms.Select(attrs={"class": "form-select"}),
#         }

# class UpdateCompraForm(forms.ModelForm):
#     class Meta:
#         model = Compra
#         fields = "__all__"
#         exclude = ["codigo"]
#         widgets = {
#             "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción"}),
#             "fecha": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la fecha"}),
#             "prodtotal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la cantidad total de productos"}),
#             "subtotal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el subtotal"}),
#             "iva": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el IVA"}),
#             "total": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el total"}),
#             "proveedor": forms.Select(attrs={"class": "form-select"}),
#             "almacen": forms.Select(attrs={"class": "form-select"}),
#         }




class CompraForm(forms.ModelForm):
    class Meta:
        model = Compra
        fields = ["descripcion", "almacen", "proveedor"]

    materiales = forms.ModelMultipleChoiceField(
        queryset=Material.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
        label="Materiales",
    )

    def __init__(self, *args, **kwargs):
        super(CompraForm, self).__init__(*args, **kwargs)

        for material in Material.objects.all():
            field_name = f"cantidad_{material.codigo}"
            label = f"Cantidad de {material.nombre}"
            widget = forms.NumberInput(attrs={"class": "form-control"})
            field = forms.IntegerField(label=label, widget=widget, required=False)
            self.fields[field_name] = field





class UpdateCompraForm(forms.ModelForm):
    class Meta:
        model = Compra
        fields = "__all__"
        exclude = ["codigo", "cliente", "fecha", "subtotal", "iva", "total"]
        widgets = {
            "materiales": forms.CheckboxSelectMultiple(),
            "descripcion": forms.TextInput(attrs={"class": "form-control"}),
            "prodtotal": forms.NumberInput(attrs={"class": "form-control"}),
            "proveedor": forms.Select(attrs={"class": "form-select"}),
            "almacen": forms.Select(attrs={"class": "form-select"}),
            # Agrega widgets para otros campos según sea necesario
        }
    cantidad_nuevo_material = forms.IntegerField(
        label="Cantidad del nuevo material", required=False
    )

    materiales = forms.ModelMultipleChoiceField(
        queryset=Material.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
        label="Materiales",
    )





class PedidoForm(forms.ModelForm):
    class Meta:
        model = Pedido
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción"}),
            "fecha": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la fecha"}),
            "cantproduct": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la cantidad de productos"}),
            "subtotal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el subtotal"}),
            "iva": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el IVA"}),
            "total": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el total"}),
            "totalint": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el total (int)"}),
            "almacen": forms.Select(attrs={"type":"select", "class": "form-select"}),
            "lugar": forms.Select(attrs={"type":"select", "class": "form-select"}),
            "cliente": forms.Select(attrs={"type":"select", "class": "form-select"}),
        }

class UpdatePedidoForm(forms.ModelForm):
    class Meta:
        model = Pedido
        fields = ["descripcion", "fecha", "almacen", "lugar", "cliente"]

    materiales = forms.ModelMultipleChoiceField(
        queryset=Material.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
        label="Materiales",
    )

    cantidad_materiales = forms.IntegerField(
        required=False,
        widget=forms.TextInput(attrs={"type": "number", "class": "form-control"}),
        label="Cantidad de materiales",
    )

    nuevo_material = forms.ModelChoiceField(
        queryset=Material.objects.all(),
        widget=forms.Select(attrs={"class": "form-control"}),
        required=False,
        label="Seleccionar nuevo material",
    )

    cantidad_nuevo_material = forms.IntegerField(
        required=False,
        widget=forms.TextInput(attrs={"type": "number", "class": "form-control"}),
        label="Cantidad del nuevo material",
    )



class PagoForm(forms.ModelForm):
    class Meta:
        model = Pago
        fields = "__all__"
        exclude = ["fecha", "codigo","num_pago","saldo"]
        widgets = {
            "montopago": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Monto del Pago", "pattern": "[0-9]+(\.[0-9]{1,2})?$"}),
            "concepto": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Concepto"}),
            "pedido": forms.Select(attrs={"class": "form-select"})
        }

class UpdatepagoForm(forms.ModelForm):
    class Meta:
        model = Pago
        fields = "__all__"
        exclude = ["fecha", "codigo","num_pago","saldo"]
        widgets = {
            "montopago": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Monto del Pago", "pattern": "[0-9]+(\.[0-9]{1,2})?$"}),
            "concepto": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Concepto"}),
            "pedido": forms.Select(attrs={"class": "form-select"})
        }
        


# class CambiosForm(forms.ModelForm):
#     class Meta:
#         model = Cambios
#         fields = "__all__"
#         exclude = ["codigo"]
#         widgets = {
#             "fecha": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del tipo"}),
#             "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción del tipo"}),
#             "pedido": forms.Select(attrs={"type":"select", "class":"form-select"}),
#             "almacen": forms.Select(attrs={"type":"select", "class":"form-select"}),
#         }

# class UpdateCambiosForm(forms.ModelForm):
#     class Meta:
#         model = Cambios
#         fields = "__all__"
#         exclude = ["codigo"]
#         widgets = {
#             "fecha": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del tipo"}),
#             "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción del tipo"}),
#             "pedido": forms.Select(attrs={"type":"select", "class":"form-select"}),
#             "almacen": forms.Select(attrs={"type":"select", "class":"form-select"}),
#         }

