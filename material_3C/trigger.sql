-- Active: 1699482820400@@127.0.0.1@3306@materiales.sql

--TRIGGER DEL PROYECTO0000000 979787



-- Crear el trigger
DELIMITER //
CREATE TRIGGER before_insert_compra
BEFORE INSERT ON Compra
FOR EACH ROW
BEGIN
    SET NEW.prodTotal = 0;
    SET NEW.Subtotal = 0.00;
    SET NEW.Iva = 0.00;
    SET NEW.Total = 0.00;
END;
//
DELIMITER ;

DELIMITER //

CREATE TRIGGER establecer_fecha_pago
BEFORE INSERT ON pago
FOR EACH ROW
BEGIN
    -- Establecer la fecha actual antes de la inserción
    SET NEW.fecha = CURRENT_DATE;
END //

DELIMITER ;


-- Crear el trigger holaaaaa
DELIMITER //

CREATE TRIGGER before_insert_material
BEFORE INSERT ON Material
FOR EACH ROW
BEGIN
    -- Establecer el precio de venta automáticamente
    SET NEW.PrecioVenta = NEW.precioCompra * (1 + 0.10); -- Ajusta el 0.10 al porcentaje deseado
END //

DELIMITER ;


insert into material (Nombre, `Descripcion`, `precioCompra`, tipo)value
("Rodillo", "Rodillo para pintura", 50.00, 2);

select *
FROM material
where Codigo= 26;

SELECT *
FROM tipos;



----------------------------------------------------------------------------------
-- Crear el trigger
DELIMITER //

CREATE TRIGGER before_insert_resena
BEFORE INSERT ON resenas
FOR EACH ROW
BEGIN
    -- Establecer la fecha actual antes de la inserción
    SET NEW.fecha = CURDATE();
END //

DELIMITER ;


--------------------------------------------------------------------------------------------------------
-- poner los campos automaticamente el dia que se registre el pedido
DELIMITER //

CREATE TRIGGER initialize_pedido_fields
BEFORE INSERT ON pedido
FOR EACH ROW
BEGIN
    SET NEW.fecha = current_timestamp; 
    SET NEW.cantProduct = 0;
    SET NEW.subtotal = 0.00;
    SET NEW.iva = 0.00;
    SET NEW.total = 0.00;
    
END;
//

DELIMITER ;

INSERT INTO pedido (descripcion, fecha, cantProduct, subtotal, iva, total, almacen, lugar, cliente) VALUES
    ('Pedido de repuestos', '2023-11-20', 10, 500.00, 75.00, 575.00, 1, 1, 1);

INSERT INTO ped_mat (pedido, material, cantidad) VALUES
    (12, 26, 2);
SELECT * FROM pedido
where codigo = 12;

INSERT into mat_alma (almacen, material, stock) VALUES
(1, 26, 100); 

SELECT * FROm mat_alma where almacen = 1 and material = 26;

select * from proveedor;

 insert into compra (Descripcion, Fecha, prodTotal, Proveedor, almacen) values
 ('Compra de 100 rodillos', '2023/11/27', 100, 1, 1);

select * from compra;

insert into material_compra (material, compra, cantidad) values 
(26, 21, 50);

select * from material_compra;

update cantidad = 50 
where material = 26 and compra = 21;



-------------------------------------------------------------------------------------------------------
--1. Calcular el importe de cada material en un pedido. 

DELIMITER //

CREATE TRIGGER calcularImporteMaterialPedido
before INSERT ON ped_mat
FOR EACH ROW
BEGIN
    DECLARE precio_material DECIMAL(10, 2);

    -- Obtener el precio del material
    SELECT precioVenta INTO precio_material
    FROM Material
    WHERE Codigo = NEW.material;

    -- Calcular el importe
    SET NEW.importe = precio_material * NEW.cantidad;
END //

DELIMITER ;



INSERT INTO ped_mat (pedido, material, cantidad) VALUES
    (21, 1, 5);


     DELETE FROM ped_mat
WHERE pedido = 21 and material=1;
-------------------------------------------------------------------------------------------------------
------2. Calcular el número de pago y el saldo de un pago realizado a un pedido 

DELIMITER //

CREATE TRIGGER calcularPagoSaldo
BEFORE INSERT ON pago
FOR EACH ROW
BEGIN
    DECLARE total_pedido DECIMAL(10, 2);
    DECLARE total_pagado DECIMAL(10, 2);
    DECLARE nuevo_saldo DECIMAL(10, 2);
    DECLARE nuevo_numero_pago INT;


    -- Obtener el total del pedido
    SELECT totalInt INTO total_pedido
    FROM pedido
    WHERE codigo = NEW.pedido;

    -- Obtener el total pagado hasta ahora para ese pedido
    SELECT COALESCE(SUM(montoPago), 0) INTO total_pagado
    FROM pago
    WHERE pedido = NEW.pedido;

    -- Calcular el nuevo saldo y número de pago
    SET nuevo_saldo = total_pedido - (total_pagado + NEW.montoPago);
    SET nuevo_numero_pago = (SELECT COUNT(*) + 1 FROM pago WHERE pedido = NEW.pedido);

    -- Actualizar el saldo y número de pago en la fila que se está insertando
    SET NEW.saldo = nuevo_saldo;
    SET NEW.num_pago = nuevo_numero_pago;
END //

DELIMITER ;


drop trigger calcularPagoSaldo


INSERT INTO pago ( montoPago, concepto, pedido,saldo) VALUES
    ( 0, 'Pago 2 de repuestos', 7,10);

   DELETE FROM ped_mat
WHERE pedido = ;

----------------------------------------------------------------------------------


--3. Calcular el stock de material cuando se realiza una venta en una almacen


DELIMITER //
CREATE TRIGGER actualizar_stock_en_venta_almacen
AFTER INSERT ON ped_mat
FOR EACH ROW
BEGIN
    DECLARE material_stock INT;
    DECLARE material_cantidad INT;
    DECLARE material_almacen INT;

    -- Recuperar el stock actual del material en el almacén
    SELECT stock INTO material_stock
    FROM mat_alma
    WHERE almacen = (SELECT almacen FROM pedido WHERE codigo = NEW.pedido)
      AND material = NEW.material;

    -- Recuperar la cantidad vendida en la transacción actual
    SELECT cantidad INTO material_cantidad
    FROM ped_mat
    WHERE pedido = NEW.pedido AND material = NEW.material;

    -- Actualizar el stock en el almacén después de la venta
    UPDATE mat_alma
    SET stock = material_stock - material_cantidad
    WHERE almacen = (SELECT almacen FROM pedido WHERE codigo = NEW.pedido)
      AND material = NEW.material;
END;
//
DELIMITER ;




drop trigger actualizar_stock_en_venta_almacen


-----------------------------------------------------------------
INSERT INTO compra VAlUES
( 20,'COMPRA DE CEMENTO Y HERRAMIENTAS', '2023-01-05', 50, 5922, 1128, 7050, 1, 2);


INSERT INTO material_compra VALUES
(1, 20, 15, 10);
    
    
    DELETE FROM ped_mat

WHERE pedido = 21 and material=2;





-----------------------------------------------------


-----4. Calcular el stock de material cuando se realiza una compra en un almacen

DELIMITER //
CREATE TRIGGER actualizar_stock_en_compra_almacen
AFTER INSERT ON material_compra
FOR EACH ROW
BEGIN
    DECLARE material_stock INT;
    DECLARE material_cantidad INT;

    -- Recuperar el stock actual del material en el almacén
    SELECT stock INTO material_stock
    FROM mat_alma
    WHERE almacen = (SELECT almacen FROM Compra WHERE Codigo = NEW.compra)
      AND material = NEW.material;

    -- Recuperar la cantidad comprada en la transacción actual
    SELECT cantidad INTO material_cantidad
    FROM material_compra
    WHERE compra = NEW.compra AND material = NEW.material;

    -- Actualizar el stock en el almacén después de la compra
    UPDATE mat_alma
    SET stock = material_stock + material_cantidad
    WHERE almacen = (SELECT almacen FROM Compra WHERE Codigo = NEW.compra)
      AND material = NEW.material;
END;
//
DELIMITER ;
drop trigger  actualizar_stock_en_compra_almacen
----------------------------------------------------------------------------
insert into material_compra (material, compra,cantidad,importe)
VALUES
    (1,1,5000,20.0);

     DELETE FROM  material_compra 
WHERE compra = 1 and material =1;
----------------------------------------------------------------------------------
-- 5. Trigger para calcular el stock de material después de un cambio en un pedido
-- Crear el trigger
-- Crear el trigger
-- Crear el trigger
-- Crear el trigger
DELIMITER //

CREATE TRIGGER actualizar_stock_despues_de_cambio
AFTER INSERT ON camb_material
FOR EACH ROW
BEGIN
    DECLARE cantidad_cambiada_material INT;
    DECLARE cantidad_devuelta_material INT;
    DECLARE importe_material DECIMAL(10,2);
    DECLARE num_pedido INT;
     DECLARE nuevo_stock INT;

    -- Obtener la cantidad cambiada del material en el cambio y el importe del material en el pedido
    SELECT cm.cantidad_cambiada,cm.cantidad_devuelta, pm.importe, c.pedido
    INTO cantidad_cambiada_material, cantidad_devuelta_material,  importe_material, num_pedido
    FROM camb_material cm
    JOIN cambios c ON cm.cambio = c.codigo
    JOIN ped_mat pm ON c.pedido = pm.pedido AND cm.material = pm.material
    WHERE cm.cambio = NEW.cambio AND cm.material = NEW.material;

    -- Verificar si la cantidad devuelta es menor o igual al importe
    IF cantidad_cambiada_material >= importe_material THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'La cantidad cambiada no puede ser mayor que el importe del material en el pedido.';
    END IF;

    -- Calcular el nuevo stock después del cambio
   
    SET nuevo_stock = (SELECT stock FROM mat_alma WHERE almacen = (SELECT almacen FROM cambios WHERE codigo = NEW.cambio) AND material = NEW.material) - cantidad_cambiada_material+cantidad_devuelta_material;

    -- Actualizar el stock en la tabla mat_alma
    UPDATE mat_alma
    SET stock = nuevo_stock
    WHERE almacen = (SELECT almacen FROM cambios WHERE codigo = NEW.cambio) AND material = NEW.material;
END //

DELIMITER ;









 DELETE FROM  camb_material
WHERE cambio = 20 and material=1;



drop trigger actualizar_stock_despues_de_cambio
----------------------------------------------------------------
-- Insertar un cambio en la tabla cambios
INSERT INTO cambios (FECHA, descripcion, pedido, almacen)
VALUES ('2023-11-21', 'Cambio de productos', 21, 1);


INSERT INTO camb_material (cambio, material,cantidad_cambiada, cantidad_devuelta) VALUES
(20, 1,1350 ,2);

INSERT INTO cambios VALUES
(20, '2023/07/27', 'Reemplazo de varillas de acero por mala calidad', 1, 1);




-- Insertar un registro en la tabla camb_material para el cambio
INSERT INTO camb_material (cambio, material,cantidad_cambiada, cantidad_devuelta)
VALUES (20, 1, 1,200000);

INSERT INTO camb_material (cambio, material,cantidad_cambiada, cantidad_devuelta)
VALUES (1, 1, 11,0);

delect 



-- Insertar datos en la tabla camb_material para probar el trigger
INSERT INTO camb_material (cambio, material, cantidad_cambiada, cantidad_devuelta) VALUES
    (2, 1, 3, 1);



DELETE FROM camb_material WHERE cambio=1;
-- Insertar datos en la tabla camb_material para probar el trigger


-- Eliminar registros de la tabla camb_material
DELETE FROM camb_material;





----------------------------------------------------------
insert into camb_material(cambio, material,cant_prod)
VALUES
    (2,2,5);

     DELETE FROM camb_material
WHERE cambio= 1;
---------------------------------------------------------
---6.Calcular el importe de cada material en una compra

DELIMITER //
CREATE TRIGGER calcular_importe_en_compra
BEFORE INSERT ON material_compra
FOR EACH ROW
BEGIN
    DECLARE precio_compra DECIMAL(10, 2);
    
    -- Recuperar el precio de compra del material
    SELECT precioCompra INTO precio_compra
    FROM Material
    WHERE Codigo = NEW.material;
    
    -- Calcular el importe y asignarlo al nuevo registro
    SET NEW.importe = NEW.cantidad * precio_compra;
END;
//
DELIMITER ;
-------------------------------

INSERT INTO material_compra (material, compra, cantidad)
VALUES
    (1, 2, 2); 

DELETE FROM material_compra  WHERE compra=2;
------------------------------------------------------------------------------------

--7. Calcular la cantidad total de productos, subtotal, IVA, total y total con intereses de un pedido








DELIMITER //
CREATE TRIGGER calcular_totales_pedido
AFTER INSERT ON ped_mat
FOR EACH ROW
BEGIN
    DECLARE cantidad_total INT;
    DECLARE subtotal_pedido DECIMAL(10, 2);
    DECLARE iva_pedido DECIMAL(10, 2);
    DECLARE total_pedido DECIMAL(10, 2);
    DECLARE total_con_intereses DECIMAL(10, 2);
    DECLARE sub_total DECIMAL(10, 2);

    -- Calcular la cantidad total de productos
    SELECT SUM(cantidad) INTO cantidad_total
    FROM ped_mat
    WHERE pedido = NEW.pedido;

    -- Calcular el subtotal del pedido
    SELECT SUM(importe) INTO subtotal_pedido
    FROM ped_mat
    WHERE pedido = NEW.pedido;

    -- Calcular el IVA del subtotal (asumiendo un 16%)
    SET sub_total = subtotal_pedido / 1.16;
    SET iva_pedido = sub_total * 0.16;

    -- Calcular el total del pedido
    SET total_pedido = subtotal_pedido;

    -- Calcular el total con intereses (asumiendo un 10%)
    SET total_con_intereses = total_pedido * 1.10;

    -- Actualizar los campos en la tabla pedido
    UPDATE pedido
    SET cantProduct = cantidad_total,
        subtotal = sub_total,
        iva = iva_pedido,
        total = total_pedido,
        totalInt = total_con_intereses
    WHERE codigo = NEW.pedido;
END;
//
DELIMITER ;

drop trigger calcular_totales_pedido
---------------------------------------------------------------------------------
INSERT INTO pedido (descripcion, fecha, almacen, lugar, cliente) VALUES
('Pedido de herramientas', '2023-11-10', 1, 1, 1);


INSERT INTO ped_mat (pedido, material, cantidad)
VALUES
    (12, 3, 2),
     (12, 14, 4);
    
    select * from ped_mat where pedido=12;


    select codigo from pedido where codigo=1;
select 
ma.codigo as codigo,
ma.nombre as nombre,
ma.PrecioVenta as precio
from mat_alma maa
inner join  material as ma on maa.material=ma.Codigo 
where almacen=1;

      DELETE FROM ped_mat WHERE pedido=7;

---------------------------------------------------------------------------------------------
------8. Calcular la cantidad total de productos, subtotal, IVA y total de una compra. 
DELIMITER //

CREATE TRIGGER calcularTotalesCompra
AFTER INSERT ON material_compra
FOR EACH ROW
BEGIN
   DECLARE cantidad_total INT;
    DECLARE subtotal_pedido DECIMAL(10, 2);
    DECLARE iva_pedido DECIMAL(10, 2);
    DECLARE total_pedido DECIMAL(10, 2);
    DECLARE sub_total DECIMAL(10, 2); 

    -- Calcula la cantidad total de productos
    SELECT SUM(cantidad) INTO cantidad_total
    FROM material_compra
    WHERE compra = NEW.compra;

    -- Calcula el subtotal
    SELECT SUM(importe) INTO subtotal_pedido
    FROM material_compra
    WHERE compra = NEW.compra;


SET sub_total = subtotal_pedido / 1.16;
    SET iva_pedido = sub_total * 0.16;

    -- Calcular el total del pedido
    SET total_pedido = subtotal_pedido;

    -- Actualiza los campos en la tabla de compras
    UPDATE Compra
    SET prodTotal = cantidad_total,
        Subtotal = sub_total,
        Iva = iva_pedido,
        Total = total_pedido
    WHERE Codigo = NEW.compra;
END //

DELIMITER ;
drop trigger calcularTotalesCompra
-------------------------------

-- Inserta una compra (debes obtener el código de la compra después de la inserción)
INSERT INTO Compra (Descripcion, Fecha,  Proveedor, almacen)
VALUES ('Compra de prueba', '2023-11-21', 1, 1);

-- Inserta materiales asociados a la compra
INSERT INTO material_compra (material, compra, cantidad, importe)
VALUES
    (1, 7, 5, 50.0);


------------------------------------------------------------------------------------------------------
    ----9.Verificar que haya stock suficiente de un producto en un almacén cuando se realiza un pedido.

 DELIMITER //

CREATE TRIGGER verificar_stock_before_insert
BEFORE INSERT ON ped_mat
FOR EACH ROW
BEGIN
    DECLARE stock_disponible INT;

    -- Obtener el stock disponible en el almacén para el material en el pedido
    SELECT mat_alma.stock INTO stock_disponible
    FROM mat_alma
    WHERE mat_alma.almacen = (SELECT almacen FROM pedido WHERE codigo = NEW.pedido)
        AND mat_alma.material = NEW.material;

    -- Verificar si hay suficiente stock
    IF stock_disponible IS NULL OR stock_disponible < NEW.cantidad THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'No hay suficiente stock para el material en el almacén seleccionado';
    END IF;
END //

DELIMITER ;

drop trigger verificar_stock_before_insert
-------------------------------------------------------
INSERT INTO ped_mat (pedido, material, cantidad) VALUES
    (1, 4, 2);

   DELETE FROM ped_mat  WHERE pedido=21 and material=4;



   select material, stock from mat_alma WHERE material=1 and almacen=1;

  -----------------------------------------------------  




